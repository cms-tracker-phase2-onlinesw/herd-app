# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security


## [0.4.25] - 2024-10-13

### Changed

* Load plugins with `RTLD_DEEPBIND` flag, in addition to pre-existing flags (so that plugins use their own provided symbols in place of those from SWATCH & HERD)


## [0.4.24] - 2024-09-25

### Changed

* Docker images: Add entrypoint script to `-devel` image, and set log4cplus config environment variable inside this script

### Fixed

* CI: Add missing `tags` attribute for 'load plugin' test jobs (this missing tag caused 'arm64' jobs to erroneously run on AMD64 runners).


## [0.4.23] - 2024-09-20

### Fixed

* Update to SWATCH v1.8.7, which adds a check on the presence of optical modules to the Board class constructor


## [0.4.22] - 2024-08-15

### Fixed

* Update to SWATCH v1.8.6, which adds support for uint16 and int16 parameter types, and initialises the properties of optical module channels on contruction.


## [0.4.21] - 2024-07-06

### Fixed

* Plugin CI pipeline: Override HERD image entrypoint for smoke test jobs
* CentOS7 images: Patched YUM repo files to point to archived YUM repositories following EOL


## [0.4.20] - 2024-04-06

### Fixed

* Plugin CI pipeline: Use `/entrypoint.sh` wrapper in 'load plugin' job, to correctly set up environment before running HERD.


## [0.4.19] - 2024-04-06

### Added

* Plugin CI pipeline: Add automatic that loads plugin libraries and lints configuration file
* Plugin CI pipeline: Add quick (manually-triggered) smoke test that is run on the board

  * In this job, HERD loads the plugin library, creates the device classes, starts the HTTP server
    and runs at least one GET request against each available GET endpoint (checking for a good
    status code in the response from each).

### Changed

* Add extra information to HTTP response from request with expired/invalid lease.


## [0.4.18] - 2024-03-14

### Fixed

* Add missing encoding/decoding functions for uint64_t, int64_t and double parameters


## [0.4.17] - 2024-02-25

### Changed

* Update to SWATCH v1.8.4 (updates almalinux base images)


## [0.4.16] - 2024-02-24

### Changed

* Update to SWATCH v1.8.3 (empty algorithm interface now registered as fallback)


## [0.4.15] - 2024-01-14

### Changed

* Update to SWATCH v1.8.2 (updates cmake version in CentOS7 image)


## [0.4.14] - 2024-01-07

### Changed

* SWATCH thread pool: Increased the number of threads (previously this simply defaulted to 1, preventing actions for different processors from running in parallel) 


## [0.4.13] - 2024-01-04

### Changed

* Plugin CI pipeline: Image used for format job can now be selected by setting `FORMAT_JOB_OS` variable (e.g. to `centos7`, `alma8` or `alma9`)
  - The format job is omitted if `FORMAT_JOB_OS` is not set, superceding `INCLUDE_FORMAT_JOB`
* Update to SWATCH v1.8.1 (fixes some uninitialised value errors, and updates CI templates)
* When running HERD in valgrind, track origin of uninitialised value errors by default, and support user specifying extra arguments (via `VALGRIND_EXTRA_ARGS` environment variable)


## [0.4.12] - 2023-11-21

### Added

* Log package build & version info before starting server

### Changed

* Updated to SWATCH v1.8.0 (improvements relating to property updates)


## [0.4.11] - 2023-11-03

### Fixed

* Fix 'use object after deletion' error in log message for retiring lease.
* Refactorise file deletion code, so that local ('on board') file parameters are no longer deleted after the command/transition finishes.
* Update to SWATCH v1.7.7
  - Fixes bug in optics monitoring sync in post-action callback (bug affected boards with inter-FPGA links);
  - Improves logging of post-action callbacks; and
  - Adds protection against post-action callbacks throwing exceptions.


## [0.4.10] - 2023-10-29

### Added

- CI (this repo & plugins): Job which verifies that:
  1. the devcontainer config matches schema; and
  2. the image specified therein matches the base container used for image builds.

### Changed

- Update to SWATCH v1.7.5, so that devtoolset-8 is now sourced outside the entrypoint of the CentOS7 amd64 and arm64 images.


## [0.4.9] - 2023-10-03

### Fixed

- Update to SWATCH v1.7.4, to include CI pipeline fixes for new k8s-provisioned CERN runners.


## [0.4.8] - 2023-08-07

### Changed

- Plugin CI: Create JUnit report in test template, and declare it to GitLab
- CI: Run tests on all OSs; create JUnit reports and declare them to GitLab

### Fixed

- Plugin CI: Fix image for docker:prep job to support non-default `BASE_IMAGE_REPO_URL`


## [0.4.7] - 2023-07-31

### Fixed

- Fix logic error in encoding connected port information


## [0.4.6] - 2023-07-30

### Changed

- Update to SWATCH v1.7.3, in which a non-zero CRC mismatch count is relaxed from an error to a warning.


## [0.4.5] - 2023-07-27

### Changed

- Improve performance of callbacks for GET requests, and add log messages which record time taken to handle requests


## [0.4.4] - 2023-07-20

### Changed

- Update plugin test jobs to PySWATCH version 0.4.1

### Fixed

- Protect `Server` class member data against access from multiple threads


## [0.4.3] - 2023-06-08

### Fixed

- Update `herd.yml` copy command in plugin dockerfile, to avoid ambiguity when plugin repository packages multiple YAML files


## [0.4.2] - 2023-06-08

### Fixed

- Update to SWATCH v1.7.2, so that post-action callback is run even if error occurs.


## [0.4.1] - 2023-05-25

### Fixed

- Update to SWATCH v1.7.2, so that CI pipelines use newer docker builder images which will not be deleted by CERN IT.

