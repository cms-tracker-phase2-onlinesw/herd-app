# HERD application

This repository implements the HERD control and monitoring application - an executable that loads SWATCH plugins, and provides a network interface (on port 3000 by default) that allows remote applications to run control/configuration procedures and retrieve monitoring data declared in those plugins.

The HERD application is packaged as RPMs that are automatically uploaded to a YUM repository by the CI pipeline. This repository also includes Dockerfiles and other configuration files for automatically creating docker images that contain the HERD application alongside plugin libraries using GitLab CI.

Note: An extensive guide for developing associated plugins can be found at https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/index.html


## Dependencies

The main dependency is the SWATCH library, which in turn requires the following:

 * Build utilities: make, CMake3 (at least v3.17)
 * [boost](https://boost.org)
 * [log4cplus](https://github.com/log4cplus/log4cplus)

For network communication and configuration files, the HERD application also uses:

 * [cpp-httplib](https://github.com/yhirose/cpp-httplib)
 * [jsoncpp](https://github.com/open-source-parsers/jsoncpp)
 * [yaml-cpp](https://github.com/jbeder/yaml-cpp)


## Build instructions

Firstly, clone this repository:
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/cms-tracker-phase2-onlinesw/herd-app.git
```
Note: The --recurse-submodules flag is needed here to clone the submodules at the same time.

There are two supported routes for building the software:
 1. **Building inside a docker container**

    To simplify development of HERD, this repository includes a VSCode configuration file
    (`.devcontainer/devcontainer.json`) which allows one to build in a development container
    that already contains all dependencies, along with relevant tooling like git. To use
    this setup you just need to open the path for the cloned repository in VSCode, click on
    the green bar in the bottom left of the window and then click "Reopen in container".

 2. **Classic workflow (containerless)**

    If building the code outside of the development container, you'll need to install the
    dependencies listed above. For example, on CentOS7:
    ```
    yum install gcc-c++ make cmake3 boost-devel log4cplus-devel jsoncpp-devel yaml-cpp-devel
    ```
    Then install SWATCH v.1.7.0


After these preparatory steps, to actually build HERD you can simply follow the standard
CMake build workflow:
```
mkdir build
cd build
cmake3 ..
make
```

Then, if you want to install HERD you can do this directly by running:
```
sudo make install
```

Or you can build the RPMs first and then install those:
```
make package
sudo dnf localinstall $(find . -name "*.rpm")
```

*Note:* When developing HERD it's often useful to run it with the dummy plugin. This can be done
by following the HERD build & RPM installation commands above, checking out the
[`herd-dummy`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy)
repository (inside the same container if developing in a container), and then building and
installing the dummy plugin. After installing the dummy plugin, you should remove the HERD
RPMs - `rpm -e $(rpm -qa | grep herd)` - and then you can rebuild HERD after making changes
to the codebase and run the HERD executable from the `build` directory whenever you want
to test your changes, pointing it to `dummy.yml` from the dummy plugin repository - e.g.
`./herd ../herd-dummy/dummy.yml`.


## Running the HERD application

The control and monitoring application, `herd`, runs an HTTP server to allow remote applications to
run SWATCH commands and FSM transitions. It must be run with a YAML configuration file which lists
the plugin libraries to load (`libraries` field) and the devices to instantiate (`service` and
`processors` fields); for an example of this file, see `dummy.yml` in the `herd-dummy` repository.
If run with `dummy.yml` as the configuration file, the application will load the dummy plugin and
create one 'service module' device (of type `dummy::swatch::ServceArea`) and two processor devices
(of type `dummy::swatch::Processor`, named `x0` and `x1`):
```
source env.sh
./build/herd dummy.yml
```
By default the HERD application listens for requests on TCP port 3000, but the port number can be changed using the `-p`/`--port` option. If you are deploying HERD with plugins across multiple boards, we recommend either using the docker images that are automatically produced by the common GitLab CI configuration, or installing the CI-produced RPMs from the CI-produced YUM repository.

While the HERD application is running, you can control the dummy devices instantiated by this server (i.e. run their commands and FSM transitions) using the command-line console.

In this console you can:
 * list device, command and FSM information by typing `info`;
 * run commands or transitions by typing `run DEVICE_ID COMMAND_OR_TRANSITION_ID` (e.g. `run x0 reset`);
 * engage FSMs by typing `engage-fsm DEVICE_ID FSM_ID` (e.g. `engage-fsm x0 myFSM`); and
 * reset FSMs by typing `reset-fsm DEVICE_ID FSM_ID` (e.g. `reset-fsm x0 myFSM`); and
 * disengage FSMs by typing `disengage-fsm DEVICE_ID FSM_ID` (e.g. `disengage-fsm x0 myFSM`); and


## Test suite

The test suite is used to validate the functionality of the application's core classes and functions. It can be run as follows:
```
source env.sh
./build/tests/herd-app-test-runner --log_level=test_suite
```
