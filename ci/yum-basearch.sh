
read -r -d '' BASEARCH_PYTHON_SCRIPT << EOM
try:
    import yum
    print(yum.YumBase().conf.yumvar['basearch'])
except ImportError:
    import dnf
    print(dnf.dnf.Base().conf.substitutions['basearch'])
EOM

export YUM_BASEARCH=$(/usr/libexec/platform-python -c "$BASEARCH_PYTHON_SCRIPT" | tail -n1)
