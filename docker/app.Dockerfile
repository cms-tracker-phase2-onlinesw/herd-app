
ARG BUILD_VARIANT


#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/${BUILD_VARIANT}/swatch-devel:v1.8.7 as builder

# 1) Copy over source code
COPY .  /tmp/______________________________/herd-app


# 2) Build & install
ARG GITLAB_CI=0
ARG CI_COMMIT_SHORT_SHA=XXXXXXXX
ARG CI_COMMIT_TAG=
ARG CI_SERVER_URL=
ARG CI_PROJECT_PATH=
ARG CI_PROJECT_ID=
ARG CI_PIPELINE_ID=
ARG CI_JOB_ID=

RUN \
  cd /tmp/______________________________/herd-app && \
  mkdir build && \
  cd build && \
  cmake .. && \
  make -j$(nproc) && \
  make package && \
  ls .. && ls ../ci && \
  source ../ci/yum-basearch.sh && \
  # If statement to cope with different env variable expansion syntax required in dockerfiles for CentOS 7 & 8 builds
  if [ "${YUM_BASEARCH}" ]; then \
    mkdir -vp /rpms/herd-app/${YUM_BASEARCH}/{devel,debuginfo} && \
    mv -v *-devel-*.rpm /rpms/herd-app/${YUM_BASEARCH}/devel/ && \
    mv -v *-debuginfo-*.rpm /rpms/herd-app/${YUM_BASEARCH}/debuginfo/ && \
    mv -v *.rpm /rpms/herd-app/${YUM_BASEARCH}/; \
  else \
    mkdir -vp /rpms/herd-app/\${YUM_BASEARCH}/{devel,debuginfo} && \
    mv -v *-devel-*.rpm /rpms/herd-app/\${YUM_BASEARCH}/devel/ && \
    mv -v *-debuginfo-*.rpm /rpms/herd-app/\${YUM_BASEARCH}/debuginfo/ && \
    mv -v *.rpm /rpms/herd-app/\${YUM_BASEARCH}/; \
  fi && \
  tree -a /rpms




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM scratch as export
COPY --from=builder /rpms/herd-app /rpms




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/${BUILD_VARIANT}/swatch-devel:v1.8.7 as development

COPY docker/plugin.Dockerfile /tmp/herd-app-with-plugin.Dockerfile
COPY ci/yum-basearch.sh /tmp/yum-basearch.sh
COPY docker/entrypoint.sh /entrypoint.sh
COPY docker/fix-yum-repo-files.sh /tmp/fix-yum-repo-files.sh
COPY --from=builder /rpms /rpms
RUN \
  /tmp/fix-yum-repo-files.sh && \
  sh -c 'yum -y localinstall /rpms/herd-app/*/*.rpm /rpms/herd-app/*/*/*.rpm || rpm -Uvh --nodeps --ignoresize /rpms/herd-app/*/*.rpm /rpms/herd-app/*/*/*.rpm'




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/${BUILD_VARIANT}/swatch:v1.8.7 as production

# 1) Copy over binaries
ARG LIBRARY_DIR=lib64
ARG BUILD_VARIANT
ARG TARGETPLATFORM
COPY docker/entrypoint.sh /entrypoint.sh
COPY docker/fix-yum-repo-files.sh /tmp/fix-yum-repo-files.sh
COPY --from=builder /rpms/herd-app/*/*.rpm /rpms/herd-app/
RUN \
  /tmp/fix-yum-repo-files.sh && \
  echo BUILD_VARIANT=${BUILD_VARIANT}; \
  echo TARGETPLATFORM=${TARGETPLATFORM}; \
  if [ "${BUILD_VARIANT}/${TARGETPLATFORM}" == "centos7/linux/arm64" ]; then \
    curl --fail -L https://github.com/krallin/tini/releases/download/v0.19.0/tini-arm64 -o /usr/bin/tini && \
    chmod a+x /usr/bin/tini; \
  elif [ "${BUILD_VARIANT}/${TARGETPLATFORM}" == "centos7/linux/arm/v7" ]; then \
    curl --fail -L https://github.com/krallin/tini/releases/download/v0.19.0/tini-armhf -o /usr/bin/tini && \
    chmod a+x /usr/bin/tini; \
  else \
    yum -y install tini; \
  fi && \
  yum -y install gdb valgrind && \
  yum -y localinstall /rpms/herd-app/*.rpm

# 2) Expose port
EXPOSE 3000


# 3) Set default user & command
USER root
ENTRYPOINT ["herd"]
CMD []

STOPSIGNAL SIGTERM
