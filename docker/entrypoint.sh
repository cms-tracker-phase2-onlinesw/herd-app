#!/bin/bash

if [ -x /entrypoint_env.sh ]; then
  source /entrypoint_env.sh
fi


# Formulate appropriate command to run, based on whether valgrind and/or gbd requested
if [[ "${RUN_HERD_IN_GDB}" -eq 1 ]]; then
  # Do not catch exceptions inside GDB, so that can get the stacktrace
  HERD_CMD="herd --do-not-catch-exceptions"
else
  HERD_CMD=herd
fi

if [[ "${RUN_HERD_IN_VALGRIND}" -eq 1 ]]; then
  VALGRIND_ARGS="--tool=memcheck --error-exitcode=1 --leak-check=full --num-callers=100 --track-origins=yes"

  if [[ "${RUN_HERD_IN_GDB}" -eq 1 ]]; then
    VALGRIND_ARGS+=" --vgdb=yes --vgdb-error=0"
  fi

  if [ ! -z "${VALGRIND_EXTRA_ARGS}" ]; then
    VALGRIND_ARGS+=" ${VALGRIND_EXTRA_ARGS}"
  fi

  HERD_CMD="valgrind $VALGRIND_ARGS ${HERD_CMD}"
  echo "Running HERD within valgrind, using command: ${HERD_CMD}"

elif [[ "${RUN_HERD_IN_GDB}" -eq 1 ]]; then
  HERD_CMD="gdb --args ${HERD_CMD}"
  echo "Running HERD within GDB, using command: ${HERD_CMD}"
fi


# Set path of log4cplus config
export SWATCH_LOG4CPLUS_CONFIG=/usr/etc/swatch/log4cplus.properties


# Finally, run the appropriate command, storing copy of output if HERD_APP_OUTPUT_FILE is set
if [ -z "${HERD_APP_OUTPUT_FILE}" ]; then
  exec ${HERD_CMD} "$@"

else
  echo "NOTE: Writing copy of output to ${HERD_APP_OUTPUT_FILE}"
  if mkdir -p $(dirname ${HERD_APP_OUTPUT_FILE}); then
    if touch ${HERD_APP_OUTPUT_FILE}; then
      set -o pipefail
      exec ${HERD_CMD} "$@" | tee -a ${HERD_APP_OUTPUT_FILE}

    else
      echo "ERROR: Cannot create file ${HERD_APP_OUTPUT_FILE} (for copy of app output)"
      exec ${HERD_CMD} "$@"
    fi

  else
    echo "ERROR: Cannot create directory $(dirname ${HERD_APP_OUTPUT_FILE}) (for copy of app output)"
    exec ${HERD_CMD} "$@"
  fi
fi

