
ARG BASE_IMAGE_REPO_URL
ARG BUILD_VARIANT
ARG BASE_IMAGE_TAG


#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM ${BASE_IMAGE_REPO_URL}/${BUILD_VARIANT}/herd-devel:${BASE_IMAGE_TAG} as buildenv

# 1) Copy over source code
ARG PLUGIN_NAME
COPY . /tmp/______________________________/${PLUGIN_NAME}

# 2) Install dependencies
RUN \
  cd /tmp/______________________________/${PLUGIN_NAME} && \
  if [ -x ci/install_dependencies.sh ]; then ./ci/install_dependencies.sh dev; fi && \
  yum clean all




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM buildenv as builder

# 1) Build & install
ARG GITLAB_CI=0
ARG CI_COMMIT_SHORT_SHA=XXXXXXXX
ARG CI_COMMIT_TAG=
ARG CI_SERVER_URL=
ARG CI_PROJECT_PATH=
ARG CI_PROJECT_ID=
ARG CI_PIPELINE_ID=
ARG CI_JOB_ID=

RUN \
  cd /tmp/______________________________/${PLUGIN_NAME} && \
  mkdir build && \
  cd build && \
  cmake .. && \
  make -j$(nproc) && \
  make package && \
  source /tmp/yum-basearch.sh && \
  # If statement to cope with different env variable expansion syntax required in dockerfiles for CentOS 7 & 8 builds
  if [ "${YUM_BASEARCH}" ]; then \
    mkdir -vp /rpms/${PLUGIN_NAME}/${YUM_BASEARCH}/{devel,debuginfo} && \
    mv -v *-devel-*.rpm /rpms/${PLUGIN_NAME}/${YUM_BASEARCH}/devel/ && \
    mv -v *-debuginfo-*.rpm /rpms/${PLUGIN_NAME}/${YUM_BASEARCH}/debuginfo/ && \
    mv -v *.rpm /rpms/${PLUGIN_NAME}/${YUM_BASEARCH}/; \
  else \
    mkdir -vp /rpms/\${PLUGIN_NAME}/\${YUM_BASEARCH}/{devel,debuginfo} && \
    mv -v *-devel-*.rpm /rpms/\${PLUGIN_NAME}/\${YUM_BASEARCH}/devel/ && \
    mv -v *-debuginfo-*.rpm /rpms/\${PLUGIN_NAME}/\${YUM_BASEARCH}/debuginfo/ && \
    mv -v *.rpm /rpms/\${PLUGIN_NAME}/\${YUM_BASEARCH}/; \
  fi && \
  tree -a /rpms




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM scratch as export
ARG PLUGIN_NAME
COPY --from=builder /rpms/${PLUGIN_NAME} /rpms




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM buildenv as development

COPY --from=builder /rpms /rpms
ARG PLUGIN_NAME
RUN sh -c 'yum -y localinstall /rpms/${PLUGIN_NAME}/*/*.rpm /rpms/${PLUGIN_NAME}/*/*/*.rpm || rpm -Uvh --nodeps --ignoresize /rpms/${PLUGIN_NAME}/*/*.rpm /rpms/${PLUGIN_NAME}/*/*/*.rpm'




#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
FROM ${BASE_IMAGE_REPO_URL}/${BUILD_VARIANT}/herd:${BASE_IMAGE_TAG} as production

# 1) Copy over source code and binaries
ARG LIBRARY_DIR=lib64
ARG PLUGIN_NAME
COPY ci /tmp/${PLUGIN_NAME}/ci
COPY --from=builder /rpms/${PLUGIN_NAME}/*/*.rpm /rpms/${PLUGIN_NAME}/
# CUSTOM COPY STATEMENTS (optional, auto-inserted by CI template)

RUN \
  cd /tmp/${PLUGIN_NAME} && \
  if [ -x ci/install_dependencies.sh ]; then ./ci/install_dependencies.sh app; fi && \
  sh -c 'yum -y localinstall /rpms/${PLUGIN_NAME}/*.rpm || rpm -Uvh --nodeps --ignoresize /rpms/${PLUGIN_NAME}/*.rpm' && \
  if [ -x ci/entrypoint_env.sh ]; then cp ci/entrypoint_env.sh /entrypoint_env.sh; fi && \
  ln -sf $(rpm -qpl /rpms/${PLUGIN_NAME}/*.rpm | grep herd.yml) /herd.yml && \
  rm -rf /tmp/${PLUGIN_NAME}

# 2) Set entrypoint & command
ENTRYPOINT ["tini", "-v", "--", "/entrypoint.sh"]
CMD ["herd.yml"]

STOPSIGNAL SIGTERM
