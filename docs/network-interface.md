# Control application: network interface

This page summarises the 'network API' of the control application, `herd` - i.e. how to remotely access information about the devices, or run commands and FSM transitions.

`herd` starts an HTTP server on port 3000 (by default). In order to gather information and execute actions (i.e. commands and FSM transitions), other applications send HTTP GET/PUT requests to `herd`, using the URLs listed below; upon receiving each request, `herd` will attempt to carry out the requested actions, and then send back relevant information in the body of the HTTP response, as JSON. [Shep](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep) and the [Python SWATCH](https://gitlab.cern.ch/cms-cactus/phase2/pyswatch) library are the supported implementations of remote clients for `herd`.

The sections below summarise the URLs that `herd` supports, and the format of the relevant request/response data in each case. Note: If the control app receives an invalid request, or if an exception is thrown when enacting any request, it will set the HTTP response error code to an appropriate value - i.e:

 * 4xx - request body is not valid JSON, does not contain mandatory fields, or otherwise has invalid format
 * 500 - exception thrown when invoking relevant callbacks

In this case, the request body will be a JSON object with two entries (both strings): `errorType` and `errorMessage`.


## Getting general information

```
GET /
```

Response body:
```
{
  "hardwareType": HARDWARE_TYPE,
  "startTime": SECONDS_SINCE_EPOCH,
  "uptime": APPLICATION_UPTIME_IN_SECONDS,
  "packages": {
    PACKAGE_NAME : {
      "version" : [MAJOR, MINOR, PATCH],
      "versionControlSystem" : VCS_INFO,
      "build": BUILD_INFO
    }
  },
  "lease": LEASE_INFO
}
```
where:

 * `HARDWARE_TYPE` is the type of board that the application is running on (e.g. Apollo, APx, Serenity, X20)
 * `VCS_INFO` is either `null` or the following map for a git repo:
```
{
  "type": "git",
  "sha": GIT_SHA,
  "clean": GIT_CLEAN,
  "tag": GIT_TAG_NAME, // Only present if building from a tag
  "branch": GIT_BRANCH_NAME // Only present if building from a branch
}
```
   where `GIT_CLEAN` indicates whether there are any local uncommitted changes in the repository (value can be `true` or `false`)
* `BUILD_INFO` is either `{"type": "manual", "time": SECONDS_SINCE_EPOCH}` for a manual build, or the following for a GitLab CI build:
```
{
  "type": "gitlab",
  "time": SECONDS_SINCE_EPOCH,
  "serverURL": GITLAB_SERVER_BASE_URL,
  "projectPath": GITLAB_REPO_PATH, 
  "projectID": GITLAB_PROJECT_ID,
  "pipelineID": GITLAB_PIPELINE_ID,
  "jobID": GITLAB_JOB_ID
}
```


## Getting summary information about devices

```
GET /devices
```

Response body:
```
{
  DEVICE_ID: {
    "role": DEVICE_ROLE,
    "type": DEVICE_CLASS,
    "uri": DEVICE_URI,
    "addressTable": DEVICE_ADDR_TABLE,
    "fsm": FSM_ID,
    "state" : STATE_ID,
    "runningActions" : RUNNING_ACTIONS,
    "isPresent": <boolean>
  }
}
```

where `RUNNING_ACTIONS` is a list containing all nested running actions, starting with the outermost - e.g. if command `cmdA` is currently running as part of FSM transition `transitionX`, it will be `["transitionX", "cmdA"]`.


## Getting information about devices' registered commands

```
GET /commands
```

Response body:
```
{
  DEVICE_ID: {
    COMMAND_ID: {
      "alias": COMMAND_ALIAS,
      "description": COMMAND_DESCRIPTION,
      "parameters": {
        PARAMETER_ID: {
          "type": PARAMETER_TYPE,
          "description": PARAMETER_DESCRIPTION,
          "rule": RULE_INFO,
          "defaultValue": DEFAULT_VALUE
        }
      },
      "constraints": {
        CONSTRAINT_NAME: [[PARAMETER_ID, ...], CONSTRAINT_DESCRIPTION]
      }
    }
  }
}
```

where `RULE_INF0` is `[RULE_DESCRIPTION, RULE_ID, RULE_SETTING1, ..., RULE_SETTINGn]` for a rule
from the `core` package, or simply `[RULE_DESCRIPTION]` for a user-defined rule class.
`PARAMETER_TYPE` can be any one of the following:
 * `bool`
 * `uint16`, `uint32`, `uint64`
 * `int32`, `int64`
 * `float`, `double`
 * `string`
 * `vector:bool`
 * `vector:uint16`, `vector:uint32`, `vector:uint64`
 * `vector:int32`, `vector:int64`
 * `vector:float`
 * `vector:string`
 * `file`
 * `table`


## Getting information about devices' registered FSMs

```
GET /fsms
```

Response body:
```
{
  DEVICE_ID: {
    FSM_ID: {
      "alias": FSM_ALIAS,
      "description": FSM_DESCRIPTION,
      "initialState": INITIAL_STATE,
      "errorState": ERROR_STATE,
      "states": {
        STATE_ID: {
          TRANSITION_ID: {
            "alias": TRANSITION_ALIAS,
            "description": TRANSITION_DESCRIPTION,
            "endState": TRANSITION_END_STATE,
            "commands": [[COMMAND_ID, NAMESPACE], ...]
          }
        }
      }
    }
  }
}
```

## Obtaining a lease

In order to be able to execute the actions (i.e. commands & FSM transitions) of a specific HERD app,
supervisory applications must first request a 'lease'. The details of this mechanism are as follows:

 * When requesting a lease, the supervisory application must supply a human-readable, meaningful
   identifier (e.g. hostname & port); this identifier will be included in error messages when other
   applications request a lease, or attempt to execute a command/transition.
 * The response to a lease request will contain a unique, randomly-generated token - the 'lease ID' -
   which must be included in subsequent requests for executing commands and transitions.
 * Each lease will automatically expire after some time. The desired lease duration must be specified
   when obtaining a lease, and the duration granted by the application (which could be smaller than
   the requested duration) is sent back in the response to the least request.
 * Before a lease automatically expires, the supervisory application can either renew it or
   immediately stop it (using endpoints specified in following sections).

```
PUT /lease/obtain
```

Request body:
```
{
  "supervisor": SUPERVISOR_ID,
  "duration": DURATION_IN_SECONDS
}
```
where `SUPERVISOR_ID` is a unique ID string for the supervisory application (e.g. hostname + port)

Response body:
```
{
  "lease": LEASE_ID,
  "duration": DURATION_IN_SECONDS
}
```

## Checking lease state

```
GET /lease
```

Response body:
```
{
  "supervisor": SUPERVISOR_LABEL,
  "duration": DURATION_IN_SECONDS
}
```

Both `SUPERVISOR_LABEL` and `DURATION_IN_SECONDS` will be `null` if there is no current lease.


## Renewing a lease

```
PUT /lease/renew
```

Request body:
```
{
  "lease": LEASE_ID,
  "duration": DURATION_IN_SECONDS
}
```

Same response body as for obtaining the lease


# Retiring a lease early

```
PUT /lease/retire
```

Request body:
```
{
  "lease": LEASE_ID
}
```

Response body: Empty.


## Running a command

```
PUT /commands/DEVICE_ID/COMMAND_ID
```

Request body:
```
{
  "lease": LEASE_ID,
  "parameters": {PARAMETER_ID: PARAMETER_VALUE, ...},
}
```
where the format of `PARAMETER_VALUE` is outlined in the [Command parameters](#command-parameters) section below.


Response body (sent immediately, while command is running):
```
{
  "path": COMMAND_ID_PATH,
  "state": COMMAND_STATE,
  "startTime": COMMAND_START_TIME,
  "duration": COMMAND_DURATION,
  "progress": COMMAND_PROGRESS,
  "messages": [[SECONDS_SINCE_START, COMMAND_MESSAGE], ...],
  "result": COMMAND_RESULT,
  "errorDetails": COMMAND_ERROR_INFO
}
```

where `COMMAND_ERROR_INFO` is `null` if no exception was thrown from the command, otherwise if the exception contains error info:
```
{
  "type": EXCEPTION_TYPE,
  "message": EXCEPTION_MESSAGE,
  "location": {
    "function": FUNCTION_NAME,
    "file": FILE_PATH,
    "line": LINE_NUMBER
  },
  "nestedError": COMMAND_ERROR_INFO
}
```
The value of `"nestedError"` will be `null` if there wasn't any inner nested exception; similarly
the value of `"location"` will be `null` if the throw location wasn't attached to the exception.


## Checking the status of a command

```
GET /commands/DEVICE_ID/COMMAND_ID
```

Response: Same as for `PUT` request to same endpoint (i.e. running a command)


## Engaging an FSM

```
PUT /fsms/DEVICE_ID/FSM_ID/engage
```

Request body:
```
{
  "lease": LEASE_ID
}
```

Response body: Empty.


## Resetting an FSM

```
PUT /fsms/DEVICE_ID/FSM_ID/reset
```

Request body:
```
{
  "lease": LEASE_ID
}
```

Response body: Empty.


## Disengaging an FSM

```
PUT /fsms/DEVICE_ID/FSM_ID/disengage
```

Request body:
```
{
  "lease": LEASE_ID
}
```

Response body: Empty.


## Running an FSM transition

```
PUT /fsms/DEVICE_ID/FSM_ID/STATE_ID/TRANSITION_ID
```

Request body:
```
{
  "lease": LEASE_ID,
  "parameters": [{PARAMETER_ID: PARAMETER_VALUE}, ...]
}
```

Response body (sent immediately, while transition is running):
```
{
  "path": TRANSITION_ID_PATH,
  "state": TRANSITION_STATE,
  "startTime": TRANSITION_START_TIME,
  "duration": TRANSITION_DURATION,
  "progress": TRANSITION_PROGRESS,
  "numCommandsDone": TRANSITION_NUMBER_COMMANDS_DONE,
  "numCommandsTotal": TRANSITION_TOTAL_NUMBER_COMMANDS,
  "commands": [
    COMMAND_SNAPSHOT,
    ...
  ]
}
```
where `COMMAND_SNAPSHOT` has the same format as the response from the command status endpoint. 


## Checking the status of an FSM transition

```
GET /fsms/DEVICE_ID/FSM_ID/STATE_ID/TRANSITION_ID
```

Response: Same as for `PUT` request to same endpoint (i.e. running an FSM transition).


## Masking input ports

```
PUT /input-masks/PROCESSOR_ID
```

Response body:
```
{
  "lease": LEASE_ID,
  "mask": [INPUT_PORT_INDEX, ...],
  "clear": [INPUT_PORT_INDEX, ...]
}
```


## Masking output ports

```
PUT /output-masks/PROCESSOR_ID
```

Request body:
```
{
  "lease": LEASE_ID,
  "mask": [OUTPUT_PORT_INDEX, ...],
  "clear": [OUTPUT_PORT_INDEX, ...]
}
```


## Changing I/O port operating modes

I.e. setting protocol, key protocol parameters (packet length & periodicity),
```
PUT /io-operating-modes/PROCESSOR_ID
```

Request body:
```
{
  "lease": LEASE_ID,
  "inputs": [[[INPUT_PORT_INDEX, ...], MODE_INFO], ...],
  "outputs": [[[OUTPUT_PORT_INDEX, ...], MODE_INFO], ...],
  "loopback": [IO_PORT_INDEX, ...],
  "standard": [IO_PORT_INDEX, ...]
}
```
where `MODE_INFO` is one of the following:

 * `"PRBS7"`
 * `"PRBS9"`
 * `"PRBS15"`
 * `"PRBS23"`
 * `"PRBS31"`
 * `"CSP", PACKET_LENGTH, PACKET_PERIODICITY, IDLE_METHOD` (with `IDLE_METHOD` either `"idle1"` or `"idle2"`)


## Update monitoring data

```
PUT /monitoring
PUT /monitoring/DEVICE_OR_OPTICSMODULE_ID
```

Response body: Empty.


## Update properties

```
PUT /properties/DEVICE_OR_OPTICSMODULE_ID
```

Response body: Empty.


## Command parameters and results, monitoring/property data

The HERD control application only supports the serialisation & deserialisation of a fixed set of parameter types.
These types are listed in the table below, with the corresponding JSON data types that are used in the network interface:

| C++ type                   | Name            | JSON encoding                                                                        |
| -------------------------- | --------------- | ------------------------------------------------------------------------------------ |
| `bool`                     | `bool`          | Boolean                                                                              |
| `uint16_t`                 | `uint16`        | Number                                                                               |
| `uint32_t`                 | `uint32`        | Number                                                                               |
| `uint64_t`                 | `uint64`        | Number                                                                               |
| `int32_t`                  | `int32`         | Number                                                                               |
| `int64_t`                  | `int64`         | Number                                                                               |
| `float`                    | `float`         | Number                                                                               |
| `double`                   | `double`        | Number                                                                               |
| `std::string`              | `string`        | String                                                                               |
| `std::vector<bool>`        | `vector:bool`   | Array[Boolean]                                                                       |
| `std::vector<uint16_t>`    | `vector:uint16` | array[Number]                                                                        |
| `std::vector<uint32_t>`    | `vector:uint32` | array[Number]                                                                        |
| `std::vector<uint64_t>`    | `vector:uint64` | array[Number]                                                                        |
| `std::vector<int32_t>`     | `vector:int32`  | array[Number]                                                                        |
| `std::vector<int64_t>`     | `vector:int64`  | array[Number]                                                                        |
| `std::vector<float>`       | `vector:float`  | array[Number]                                                                        |
| `std::vector<std::string>` | `vector:string` | array[String]                                                                        |
| `swatch::action::File`     | `file`          | Local file: `{"path": PATH, "type": TYPE, "format": FORMAT}`                         |
|                            |                 | Transferred file: `{"name": NAME, "type": TYPE, "format": FORMAT, "contents": STR}`  |
|                            |                 | where STR is a base64-encoded copy of the file.                                      |
| `swatch::action::Table`    | `table`         |                                                                                      |
