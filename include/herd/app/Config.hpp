
#ifndef __HERD_APP_CONFIG_HPP__
#define __HERD_APP_CONFIG_HPP__


#include <string>
#include <vector>

#include "swatch/phase2/DeviceStub.hpp"
#include "swatch/phase2/OpticalFibreConnectorStub.hpp"


namespace herd {
namespace app {

struct Config {
public:
  Config();

  std::string hardwareType;
  std::vector<std::string> libraries;
  swatch::phase2::DeviceStub serviceModule;
  std::vector<swatch::phase2::DeviceStub> processors;
  std::vector<swatch::phase2::OpticalFibreConnectorStub> opticalFibreConnectors;
};

Config parseConfigFile(const std::string& aPath);

std::vector<swatch::phase2::OpticalFibreConnectorStub> parseFrontPanelConfigFile(const std::string& aPath);

} // namespace app
} // namespace herd


#endif
