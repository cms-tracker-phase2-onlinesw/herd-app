
#ifndef __HERD_APP_REMNANTS_HPP__
#define __HERD_APP_REMNANTS_HPP__


#include <functional>
#include <string>
#include <vector>


namespace log4cplus {
class Logger;
}


namespace herd {
namespace app {

struct CleanupTask {
  CleanupTask();
  CleanupTask(const std::function<void()>& aAction, const std::string& aDescription);

  std::string description;

  void run() const;

  explicit operator bool() const;

private:
  std::function<void()> action;
};

//! Represents the remanants-to-be from extracting parameters (i.e. local copies of files)
class Remnants {
public:
  struct Item {
    std::string action;
    std::string parameter;
    CleanupTask cleanup;
  };

  Remnants() = default;

  void add(const std::string&, const std::string&, const CleanupTask&);

  void add(const Remnants&);

  void cleanup(log4cplus::Logger& aLogger, const std::string& context /* e.g. "Post-action callback for "blah" */) const;

private:
  std::vector<Item> mItems;
};

} // namespace app
} // namespace herd

#endif
