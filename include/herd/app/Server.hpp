
#ifndef __HERD_APP_SERVER_HPP__
#define __HERD_APP_SERVER_HPP__


#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <boost/optional.hpp>

#include <json/value.h>

#include "swatch/core/TimePoint.hpp"
#include "swatch/phase2/Board.hpp"

#include "herd/app/Remnants.hpp"


namespace httplib {
class Request;
class Response;
class Server;
}

namespace herd {
namespace app {

class Server {

public:
  enum class RunMode {
    kStopAfterStartUpTests,
    kNormal,
    kIgnoreStartUpTestFailures
  };

  struct Lease {
    typedef swatch::core::SteadyTimePoint_t::clock SteadyClock_t;
    typedef swatch::core::SystemTimePoint_t::clock SystemClock_t;

    Lease() = delete;
    Lease(const std::string&, float);
    Lease(const std::string&, const std::string&, float);
    std::string supervisor, id;
    swatch::core::TimePoint start;
    swatch::core::TimePoint deadline;

    bool hasExpired() const;
    float remainingTime() const;
    bool renew(float);
  };

  struct ResponseSpec {
    ResponseSpec();
    ResponseSpec(const std::shared_ptr<Json::Value>& aData);
    ResponseSpec(const uint16_t aStatusCode, const std::shared_ptr<Json::Value>& aData);

    uint16_t statusCode;
    std::shared_ptr<Json::Value> data;
  };

  struct HistoricalEvent {

    enum Type {
      kFSMEngage,
      kFSMReset,
      kFSMDisengage,
      kTransition,
      kCommand,
      kLeaseObtain,
      kLeaseRenew,
      kLeaseRetire
    };

    Type type;
    std::string deviceID;
    std::string id;
    swatch::core::TimePoint time;
    std::string stateID;
    std::string transID;
    Json::Value params;
    Json::Value status;
    Json::Value lease;
  };

  Server(swatch::phase2::Board& aBoard, uint16_t aPort, const boost::optional<std::chrono::seconds>& aTimeout, bool aLogMessageData);
  virtual ~Server() {}

  void appendToEventHistory(const HistoricalEvent&, const std::lock_guard<std::mutex>&);

  const swatch::phase2::Board& getBoard() const;

  swatch::phase2::Board& getBoard();

  int run(const RunMode);

  static ResponseSpec getErrorResponse(uint16_t, const std::string&, const std::string&);

  static std::string writeToString(const Json::Value&);

  void postActionCallback(const swatch::action::Functionoid&);

private:
  // Supervise server, after starting it (i.e. wait for it to start up, run initial tests, and then shutdown after timeout if requested)
  int supervise(const RunMode, httplib::Server&);

  void stopAfterTimeout(httplib::Server&);
  std::thread launchSignalHandler(bool&);
  void handleTerminationSignal(bool&);

  typedef std::function<ResponseSpec(Server&, const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&)> GetCallback;
  typedef std::function<ResponseSpec(Server&, const std::vector<std::string>&, const Json::Value&)> PutCallback;

  static std::vector<int> getTerminationSignals();
  static sigset_t convertToSignalSet(const std::vector<int>&);
  static void initialiseThreadPool(size_t aNumberOfActionables);
  static void invokeGetCallback(Server&, const Server::GetCallback&, const httplib::Request&, httplib::Response&, bool);
  static void invokePutCallback(Server&, const Server::PutCallback&, const httplib::Request&, httplib::Response&, bool);

  ResponseSpec executeGetGeneralInfo(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);
  ResponseSpec executeGetDevices(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);

  ResponseSpec executeGetLeaseInfo(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);
  ResponseSpec executeObtainLease(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeRenewLease(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeRetireLease(const std::vector<std::string>&, const Json::Value&);

  ResponseSpec executeGetCommands(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);
  ResponseSpec executeRunCommand(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeGetCommandStatus(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);

  ResponseSpec executeGetFSMs(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);
  ResponseSpec executeEngageFSM(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeResetFSM(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeDisengageFSM(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeRunTransition(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeGetTransitionStatus(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);

  ResponseSpec executeUpdateProperties(const std::vector<std::string>&, const Json::Value&);

  ResponseSpec executeMaskInputPorts(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeMaskOutputPorts(const std::vector<std::string>&, const Json::Value&);
  ResponseSpec executeSetPortOperatingModes(const std::vector<std::string>&, const Json::Value&);

  ResponseSpec executeGetMonitoringData(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&);
  ResponseSpec executeUpdateMonitoringData(const std::vector<std::string>&, const Json::Value&);

  ResponseSpec executeGetHistoryData(
      const std::vector<std::string>&,
      const std::multimap<std::string, std::string>&, const Json::Value&);

  //! Checks whether the supplied lease token matches the current lease; if not, returns appropriate HTTP response code and body.
  boost::optional<ResponseSpec> checkLease(const std::string& aLeaseToken, const std::lock_guard<std::mutex>& aGuard);

  swatch::phase2::Board& mBoard;
  const swatch::core::TimePoint mStartTime;
  const uint16_t mPort;
  const boost::optional<std::chrono::seconds> mTimeout;
  const bool mLogMessageData;

  std::pair<bool, int> mShutdownSignalRcvd;
  std::mutex mShutdownMutex;
  std::condition_variable mShutdownCondVar;
  size_t mPendingResponseCount;
  swatch::core::TimePoint mLastResponseSent;

  std::mutex mMutex; //< Mutex for lease and event history
  boost::optional<Lease> mLease;
  std::vector<HistoricalEvent> mEventHistory;

  std::map<const swatch::action::ActionableObject*, Remnants> mRemnants; //< Remnants from current command/transition

  static log4cplus::Logger sLogger;
  static const std::map<std::string, GetCallback> kGetCallbacks;
  static const std::map<std::string, PutCallback> kPutCallbacks;

  static constexpr size_t kMaxEventHistoryLength = 10;
};


} // namespace app
} // namespace herd

#endif
