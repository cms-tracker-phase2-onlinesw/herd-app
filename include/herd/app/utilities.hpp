
#ifndef __HERD_APP_UTILITIES_HPP__
#define __HERD_APP_UTILITIES_HPP__


#include <chrono>
#include <string>
#include <vector>

#include <boost/optional.hpp>

#include <json/value.h>

#include "herd/app/Server.hpp"
#include "swatch/action/StateMachine.hpp"


namespace log4cplus {
class Logger;
}

namespace boost {
namespace filesystem {
class path;
}
}

namespace swatch {
namespace action {
class Command;
class CommandSnapshot;
class PropertyHolder;
}
namespace core {
class AbstractRule;
class ErrorInfo;
class MetricSnapshot;
class MonitorableObject;
class ParameterSet;
}
namespace phase2 {
class AbstractPort;
class Board;
class Device;
}
}

namespace herd {
namespace app {

class Remnants;

std::string encodeToBase64(const std::string&);

std::string decodeFromBase64(const std::string&);

void encodeProperties(Json::Value&, const swatch::action::PropertyHolder&);

std::shared_ptr<Json::Value> encodeDeviceInfo(const swatch::phase2::Board&);

void encodeOpticsInfo(Json::Value&, const swatch::phase2::Board&);

void encodeFrontPanelInfo(Json::Value&, const swatch::phase2::Board&);

std::shared_ptr<Json::Value> encodeCommandInfo(const swatch::phase2::Board&);

void encodeCommandInfo(Json::Value&, const swatch::action::ActionableObject&);

Json::Value encodeParameterRule(const swatch::core::AbstractRule&);

Json::Value encodeParameterRule(const std::shared_ptr<const swatch::core::AbstractRule>&);

Json::Value encodeFsmInfo(const swatch::action::ActionableObject&);

std::shared_ptr<Json::Value> encodeLeaseInfo(const boost::optional<Server::Lease>&);

std::shared_ptr<Json::Value> encodeLeaseToken(const Server::Lease&);

std::shared_ptr<Json::Value> encodeCommandSnapshot(const swatch::action::CommandSnapshot&);

std::shared_ptr<Json::Value> encodeTransitionSnapshot(const swatch::action::TransitionSnapshot&);

Json::Value encodeErrorInfo(const std::shared_ptr<const swatch::core::ErrorInfo>&);

void encodeMonitorableObject(Json::Value&, const swatch::core::MonitorableObject&, const boost::optional<size_t>&);

void encodeMetric(Json::Value&, const swatch::core::AbstractMetric&);

std::shared_ptr<Json::Value> encodeMonitoringData(const swatch::phase2::Board&, const boost::optional<size_t>&);

std::shared_ptr<Json::Value> encodeHistoryData(const std::vector<Server::HistoricalEvent>&);

std::pair<swatch::core::ParameterSet, Remnants> extractParameterSet(const swatch::action::Command&, const Json::Value&);

std::pair<std::vector<swatch::core::ParameterSet>, Remnants> extractParameterSets(const swatch::action::Transition&, const Json::Value&);

void loadLibraries(const std::vector<std::string>& aPaths, const boost::filesystem::path& aBase);

} // namespace app
} // namespace herd

#endif
