
#include "herd/app/Config.hpp"


#include <fstream>
#include <iostream>

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include "boost/algorithm/string/regex.hpp"
#include "boost/algorithm/string/split.hpp"
#include "boost/algorithm/string/trim.hpp"
#include "boost/filesystem.hpp"


namespace herd {
namespace app {


Config::Config() :
  serviceModule("")
{
}


Config parseConfigFile(const std::string& aPath)
{
  YAML::Node topNode = YAML::LoadFile(aPath);

  if (not topNode.IsMap())
    throw std::runtime_error("Invalid configuration file format (top node is not a map)");

  Config c;

  for (YAML::const_iterator it = topNode.begin(); it != topNode.end(); it++) {
    const std::string key(it->first.as<std::string>());

    if (key == "hardwareType") {
      if (not it->second.IsScalar())
        throw std::runtime_error("Invalid configuration file format ('hardwareType' value is not a string)");
      c.hardwareType = it->second.as<std::string>();
      continue;
    }
    else if (key == "libraries") {
      if (not it->second.IsSequence())
        throw std::runtime_error("Invalid configuration file format (child node '" + key + "' is not a list)");

      for (YAML::const_iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
        if (not it2->IsScalar())
          throw std::runtime_error("Invalid configuration file format (library node is not a string)");
        c.libraries.push_back(it2->as<std::string>());
      }
    }
    else if (key == "service") {
      if (not it->second.IsMap())
        throw std::runtime_error("Invalid configuration file format (child node '" + key + "' is not a map)");

      YAML::Node node(it->second);
      c.serviceModule.id = node["id"].as<std::string>();
      c.serviceModule.creator = node["class"].as<std::string>();
      if (node["uri"])
        c.serviceModule.uri = node["uri"].as<std::string>();
      if (node["addressTable"])
        c.serviceModule.addressTable = node["addressTable"].as<std::string>();

      if (node["role"])
        c.serviceModule.role = node["role"].as<std::string>();
    }
    else if (key == "processors") {
      if (not it->second.IsSequence())
        throw std::runtime_error("Invalid configuration file format (child node '" + key + "' is not a list)");

      for (YAML::const_iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
        if (not it2->IsMap())
          throw std::runtime_error("Invalid configuration file format (processor node is not a map)");

        YAML::Node childNode(*it2);
        swatch::phase2::DeviceStub stub(childNode["id"].as<std::string>());
        stub.creator = childNode["class"].as<std::string>();
        if (childNode["uri"])
          stub.uri = childNode["uri"].as<std::string>();
        if (childNode["addressTable"])
          stub.addressTable = childNode["addressTable"].as<std::string>();

        if (childNode["role"])
          stub.role = childNode["role"].as<std::string>();

        c.processors.push_back(stub);
      }
    }
    else if (key == "frontPanel") {
      if (not it->second.IsScalar())
        throw std::runtime_error("Invalid configuration file format ('frontPanel' value is not a string)");

      boost::filesystem::path lFrontPanelFilePath(boost::filesystem::path(it->second.as<std::string>()));
      if (lFrontPanelFilePath.is_relative())
        lFrontPanelFilePath = boost::filesystem::path(aPath).parent_path() / lFrontPanelFilePath;
      c.opticalFibreConnectors = parseFrontPanelConfigFile(lFrontPanelFilePath.c_str());
    }
    else
      throw std::runtime_error("Invalid top-level key '" + key + "'");
  }

  return c;
}


std::vector<swatch::phase2::OpticalFibreConnectorStub> parseFrontPanelConfigFile(const std::string& aPath)
{
  std::ifstream lFile(aPath);
  if (not lFile.is_open())
    throw std::runtime_error("Could not open front panel config file \"" + aPath + "\"");

  std::vector<swatch::phase2::OpticalFibreConnectorStub> lResult;
  std::string line;
  for (size_t i = 1; std::getline(lFile, line); i++) {
    // Remove leading and trailing spaces
    boost::trim(line);

    // Skip empty lines and comments
    if (line.empty())
      continue;
    if (line.at(0) == '#')
      continue;

    // Split removing whitespace
    std::vector<std::string> tokens;
    boost::split_regex(tokens, line, boost::regex("\\s+"));

    // Validation
    if (tokens.size() < 2)
      throw std::runtime_error(aPath + ", line " + std::to_string(i) + ": Invalid number of words (there should be at least 2 - ID, type, then one per channel)");

    const std::string id(tokens.at(0));
    const std::string type(tokens.at(1));
    const std::map<std::string, size_t> validTypes { { "MPO12", 12 }, { "MTP12", 12 }, { "MPO24", 24 }, { "MTP24", 24 } };
    if (validTypes.count(type) == 0) {
      std::ostringstream errorMessage;
      errorMessage << aPath << ", line " << i << ": Unknown type '" << type << "' specified, valid types are";
      for (const auto& x : validTypes)
        errorMessage << " " << x.first;
      throw std::runtime_error(errorMessage.str());
    }

    if (tokens.size() != (validTypes.at(type) + 2))
      throw std::runtime_error(aPath + ", line " + std::to_string(i) + ": Invalid number of words, " + std::to_string(tokens.size()) + " (there should be " + std::to_string(validTypes.at(type) + 2) + " - ID, type, then one per channel)");

    // Finally append to the result
    swatch::phase2::OpticalFibreConnectorStub stub(id, type, validTypes.at(type));
    for (size_t i = 2; i < tokens.size(); i++) {
      if (tokens.at(i) != "-")
        stub.connectedOpticsMap[i - 1] = tokens.at(i);
    }
    lResult.push_back(stub);
  }

  return lResult;
}


}
}
