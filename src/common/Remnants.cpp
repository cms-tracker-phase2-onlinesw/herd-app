
#include "herd/app/Remnants.hpp"


#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include "swatch/core/utilities.hpp"


namespace herd {
namespace app {

CleanupTask::CleanupTask()
{
}


CleanupTask::CleanupTask(const std::function<void()>& aAction, const std::string& aDescription) :
  description(aDescription),
  action(aAction)
{
}


void CleanupTask::run() const
{
  action();
}


CleanupTask::operator bool() const
{
  return bool(action);
}


void Remnants::add(const std::string& aAction, const std::string& aParameter, const CleanupTask& aCleanupTask)
{
  Item item;
  item.action = aAction;
  item.parameter = aParameter;
  item.cleanup = aCleanupTask;

  mItems.push_back(item);
}


void Remnants::add(const Remnants& aOther)
{
  for (const auto& item : aOther.mItems)
    mItems.push_back(item);
}


void Remnants::cleanup(log4cplus::Logger& aLogger, const std::string& aContext) const
{
  for (const auto& item : mItems) {
    LOG4CPLUS_DEBUG(aLogger, aContext << ": " << item.cleanup.description << "' (parameter '" << item.parameter << "' from command '" << item.action << "')");
    try {
      item.cleanup.run();
    }
    catch (const std::exception& e) {
      LOG4CPLUS_ERROR(aLogger, aContext << ": " << item.cleanup.description << " - exception thrown [" << swatch::core::demangleName(typeid(e).name()) << "]: " << e.what());
    }
  }
}


} // namespace app
} // namespace herd