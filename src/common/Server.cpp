
#include "herd/app/Server.hpp"


#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <vector>

#include <log4cplus/loggingmacros.h>

#include "boost/lexical_cast.hpp"
#include "boost/uuid/random_generator.hpp"
#include "boost/uuid/uuid_io.hpp"
#include <boost/filesystem.hpp>

#include <json/reader.h>
#include <json/value.h>
#include <json/writer.h>

#include "httplib.h"

#include "swatch/action/BusyGuard.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/action/File.hpp"
#include "swatch/action/GateKeeper.hpp"
#include "swatch/action/StateMachine.hpp"
#include "swatch/action/Table.hpp"
#include "swatch/core/ParameterSet.hpp"
#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/PackageInfo.hpp"
#include "swatch/phase2/PackageRegistry.hpp"
#include "swatch/phase2/Processor.hpp"
#include "swatch/phase2/ServiceModule.hpp"

#include "herd/app/utilities.hpp"


using namespace swatch;


namespace herd {
namespace app {

log4cplus::Logger Server::sLogger = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("herd-server"));

const std::map<std::string, Server::GetCallback> Server::kGetCallbacks = {
  { "", &Server::executeGetGeneralInfo },
  { "devices", &Server::executeGetDevices },
  { "commands", &Server::executeGetCommands },
  { "commands/([^/]+)/([^/]+)", &Server::executeGetCommandStatus },
  { "lease", &Server::executeGetLeaseInfo },
  { "fsms", &Server::executeGetFSMs },
  { "fsms/([^/]+)/([^/]+)/([^/]+)/([^/]+)", &Server::executeGetTransitionStatus },
  { "monitoring", &Server::executeGetMonitoringData },
  { "monitoring/([^/]+)", &Server::executeGetMonitoringData },
  { "history", &Server::executeGetHistoryData }
};


const std::map<std::string, Server::PutCallback> Server::kPutCallbacks = {
  { "commands/([^/]+)/([^/]+)", &Server::executeRunCommand },
  { "lease/obtain", &Server::executeObtainLease },
  { "lease/renew", &Server::executeRenewLease },
  { "lease/retire", &Server::executeRetireLease },
  { "fsms/([^/]+)/([^/]+)/engage", &Server::executeEngageFSM },
  { "fsms/([^/]+)/([^/]+)/reset", &Server::executeResetFSM },
  { "fsms/([^/]+)/([^/]+)/disengage", &Server::executeDisengageFSM },
  { "fsms/([^/]+)/([^/]+)/([^/]+)/([^/]+)", &Server::executeRunTransition },
  { "input-masks/([^/]+)", &Server::executeMaskInputPorts },
  { "output-masks/([^/]+)", &Server::executeMaskOutputPorts },
  { "io-operating-modes/([^/]+)", &Server::executeSetPortOperatingModes },
  { "monitoring", &Server::executeUpdateMonitoringData },
  { "monitoring/([^/]+)", &Server::executeUpdateMonitoringData },
  { "properties/([^/]+)", &Server::executeUpdateProperties }
};


struct TruncatedJsonFormatter {
  TruncatedJsonFormatter(const Json::Value& aData) :
    data(aData)
  {
  }

  const Json::Value& data;
};

void writeTruncatedJson(std::ostream&, const Json::Value&, size_t aIndent = 0);

std::ostream& operator<<(std::ostream& aStream, const TruncatedJsonFormatter& aFmt)
{
  writeTruncatedJson(aStream, aFmt.data);
  return aStream;
}

Json::Value truncateJSON(const Json::Value& val)
{
  if (val.isObject()) {
    Json::Value result(Json::objectValue);
    for (auto const& key : val.getMemberNames()) {
      if (val[key].isObject() || val[key].isArray()) {
        result[key] = truncateJSON(val[key]);
      }
      else if (key == "contents") {
        result[key] = Json::Value(Json::nullValue);
      }
      else {
        result[key] = val[key];
      }
    }
    return result;
  }
  else if (val.isArray()) {
    Json::Value result(Json::arrayValue);
    for (unsigned int i = 0; i < val.size(); ++i) {
      if (val[i].isObject() || val[i].isArray()) {
        result[i] = truncateJSON(val[i]);
      }
      else if (val[i].isString() && val[i].asString() == "contents") {
        result[i] = Json::Value(Json::nullValue);
      }
      else {
        result[i] = val[i];
      }
    }
    return result;
  }
  return val;
}


Server::Lease::Lease(const std::string& aSupervisor, float aDuration) :
  supervisor(aSupervisor),
  id(boost::uuids::to_string(boost::uuids::random_generator()())),
  start(swatch::core::TimePoint::now()),
  deadline(start)
{
  deadline.steady += std::chrono::duration_cast<SteadyClock_t::duration>(std::chrono::duration<float>(aDuration));
  deadline.system += std::chrono::duration_cast<SteadyClock_t::duration>(std::chrono::duration<float>(aDuration));
}


Server::Lease::Lease(const std::string& aSupervisor, const std::string& aLeaseId, float aDuration) :
  supervisor(aSupervisor),
  id(aLeaseId),
  start(swatch::core::TimePoint::now()),
  deadline(start)
{
  deadline.steady += std::chrono::duration_cast<SteadyClock_t::duration>(std::chrono::duration<float>(aDuration));
  deadline.system += std::chrono::duration_cast<SteadyClock_t::duration>(std::chrono::duration<float>(aDuration));
}


bool Server::Lease::hasExpired() const
{
  return (deadline.steady <= SteadyClock_t::now());
}


float Server::Lease::remainingTime() const
{
  return std::chrono::duration<float>(deadline.steady - SteadyClock_t::now()).count();
}


bool Server::Lease::renew(float aDuration)
{
  if (hasExpired())
    return false;

  deadline = swatch::core::TimePoint::now();
  deadline.steady += std::chrono::duration_cast<SteadyClock_t::duration>(std::chrono::duration<float>(aDuration));
  deadline.system += std::chrono::duration_cast<SystemClock_t::duration>(std::chrono::duration<float>(aDuration));
  return true;
}


Server::ResponseSpec::ResponseSpec() :
  statusCode(200),
  data(std::make_shared<Json::Value>())
{
}


Server::ResponseSpec::ResponseSpec(const std::shared_ptr<Json::Value>& aData) :
  statusCode(200),
  data(aData)
{
}


Server::ResponseSpec::ResponseSpec(const uint16_t aStatusCode, const std::shared_ptr<Json::Value>& aData) :
  statusCode(aStatusCode),
  data(aData)
{
}


Server::Server(phase2::Board& aBoard, uint16_t aPort, const boost::optional<std::chrono::seconds>& aTimeout, bool aLogMessageData) :
  mBoard(aBoard),
  mStartTime(swatch::core::TimePoint::now()),
  mPort(aPort),
  mTimeout(aTimeout),
  mLogMessageData(aLogMessageData),
  mShutdownSignalRcvd(false, 0),
  mPendingResponseCount(0),
  mLastResponseSent(swatch::core::TimePoint::now())
{
}


const swatch::phase2::Board& Server::getBoard() const
{
  return mBoard;
}


swatch::phase2::Board& Server::getBoard()
{
  return mBoard;
}


int Server::run(const RunMode aMode)
{
  using std::placeholders::_1;
  using std::placeholders::_2;

  bool lStopSignalHandler = false;
  std::thread lSignalHandler(launchSignalHandler(lStopSignalHandler));
  initialiseThreadPool(mBoard.getProcessors().size() + 1);

  httplib::Server lServer;

  typedef std::function<void(const httplib::Request&, httplib::Response&)> HttpCallback_t;
  for (auto x : kGetCallbacks) {
    HttpCallback_t lFunc(std::bind(invokeGetCallback, std::ref(*this), x.second, _1, _2, mLogMessageData));
    lServer.Get(("/" + x.first).c_str(), lFunc);
  }

  for (auto x : kPutCallbacks) {
    HttpCallback_t lFunc(std::bind(invokePutCallback, std::ref(*this), x.second, _1, _2, mLogMessageData));
    lServer.Put(("/" + x.first).c_str(), lFunc);
    lServer.Options(("/" + x.first).c_str(), [](const httplib::Request&, httplib::Response& aResponse) {
      aResponse.set_header("Access-Control-Allow-Methods", "GET, PUT, OPTIONS");
      aResponse.set_header("Access-Control-Allow-Origin", "*");
      aResponse.set_header("Access-Control-Allow-Headers", "Content-Type");
    });
  }

  lServer.set_logger([this](const httplib::Request& req, const httplib::Response& res) {
    const std::string lEncoding(res.headers.count("Content-Encoding") > 0 ? "encoding: " + res.headers.find("Content-Encoding")->second : "no encoding");
    LOG4CPLUS_TRACE(sLogger, "HTTP " << req.method << " " << req.path << " (" << req.body.size() << " bytes) -> " << res.body.size() << "-byte response (status " << res.status << ", " << lEncoding << ")");
  });

  std::thread lListenThread([&lServer, this] { LOG4CPLUS_INFO(sLogger, "Starting server on port " << mPort); lServer.listen("0.0.0.0", mPort); });

  int lExitCode = this->supervise(aMode, lServer);
  lStopSignalHandler = true;
  lListenThread.join();
  lSignalHandler.join();

  return lExitCode;
}


Server::ResponseSpec Server::getErrorResponse(uint16_t aStatusCode, const std::string& aErrorType, const std::string& aMessage)
{
  ResponseSpec lResult;
  lResult.statusCode = aStatusCode;
  lResult.data = std::make_shared<Json::Value>(Json::objectValue);
  (*lResult.data)["errorType"] = aErrorType;
  (*lResult.data)["errorMessage"] = aMessage;

  return lResult;
}


std::string Server::writeToString(const Json::Value& aValue)
{
  Json::StreamWriterBuilder lBuilder;
  // lBuilder["indentation"] = "  ";
  std::unique_ptr<Json::StreamWriter> lWriter(lBuilder.newStreamWriter());

  std::ostringstream lOutputStream;
  lWriter->write(aValue, &lOutputStream);
  return lOutputStream.str();
}


struct SignalFmt {
  SignalFmt(int aSignal) :
    signal(aSignal)
  {
  }

  int signal;
};


std::ostream& operator<<(std::ostream& aOut, const SignalFmt& aSignal)
{
  switch (aSignal.signal) {
    case SIGABRT:
      return (aOut << "SIGABRT/SIGIOT") << " (" << aSignal.signal << ")";
    case SIGALRM:
      return (aOut << "SIGALRM") << " (" << aSignal.signal << ")";
    case SIGBUS:
      return (aOut << "SIGBUS") << " (" << aSignal.signal << ")";
    case SIGCHLD:
      return (aOut << "SIGCHLD/SIGCLD") << " (" << aSignal.signal << ")";
    case SIGCONT:
      return (aOut << "SIGCONT") << " (" << aSignal.signal << ")";
    // case SIGEMT:
    //   return (aOut << "SIGEMT") << " (" << aSignal.signal << ")";
    case SIGFPE:
      return (aOut << "SIGFPE") << " (" << aSignal.signal << ")";
    case SIGHUP:
      return (aOut << "SIGHUP") << " (" << aSignal.signal << ")";
    case SIGILL:
      return (aOut << "SIGILL") << " (" << aSignal.signal << ")";
    // case SIGINFO:
    //   return (aOut << "SIGINFO") << " (" << aSignal.signal << ")";
    case SIGINT:
      return (aOut << "SIGINT") << " (" << aSignal.signal << ")";
    case SIGIO:
      return (aOut << "SIGIO/SIGPOLL") << " (" << aSignal.signal << ")";
    case SIGKILL:
      return (aOut << "SIGKILL") << " (" << aSignal.signal << ")";
    // case SIGLOST:
    //   return (aOut << "SIGLOST") << " (" << aSignal.signal << ")";
    case SIGPIPE:
      return (aOut << "SIGPIPE") << " (" << aSignal.signal << ")";
    case SIGPROF:
      return (aOut << "SIGPROF") << " (" << aSignal.signal << ")";
    case SIGPWR:
      return (aOut << "SIGPWR") << " (" << aSignal.signal << ")";
    case SIGQUIT:
      return (aOut << "SIGQUIT") << " (" << aSignal.signal << ")";
    case SIGSEGV:
      return (aOut << "SIGSEGV") << " (" << aSignal.signal << ")";
    case SIGSTKFLT:
      return (aOut << "SIGSTKFLT") << " (" << aSignal.signal << ")";
    case SIGSTOP:
      return (aOut << "SIGSTOP") << " (" << aSignal.signal << ")";
    case SIGTSTP:
      return (aOut << "SIGTSTP") << " (" << aSignal.signal << ")";
    case SIGSYS:
      return (aOut << "SIGSYS/SIGUNUSED") << " (" << aSignal.signal << ")";
    case SIGTERM:
      return (aOut << "SIGTERM") << " (" << aSignal.signal << ")";
    case SIGTRAP:
      return (aOut << "SIGTRAP") << " (" << aSignal.signal << ")";
    case SIGTTIN:
      return (aOut << "SIGTTIN") << " (" << aSignal.signal << ")";
    case SIGTTOU:
      return (aOut << "SIGTTOU") << " (" << aSignal.signal << ")";
    case SIGURG:
      return (aOut << "SIGURG") << " (" << aSignal.signal << ")";
    case SIGUSR1:
      return (aOut << "SIGUSR1") << " (" << aSignal.signal << ")";
    case SIGUSR2:
      return (aOut << "SIGUSR2") << " (" << aSignal.signal << ")";
    case SIGVTALRM:
      return (aOut << "SIGVTALRM") << " (" << aSignal.signal << ")";
    case SIGXCPU:
      return (aOut << "SIGXCPU") << " (" << aSignal.signal << ")";
    case SIGXFSZ:
      return (aOut << "SIGXFSZ") << " (" << aSignal.signal << ")";
    case SIGWINCH:
      return (aOut << "SIGWINCH") << " (" << aSignal.signal << ")";
    default:
      return (aOut << "unknown") << " (" << aSignal.signal << ")";
  }
  return aOut;
}


int Server::supervise(const RunMode aMode, httplib::Server& aServer)
{
  // PART 1: Test each GET endpoint
  aServer.wait_until_ready();
  LOG4CPLUS_INFO(sLogger, "Server has started up. Checking response from each GET endpoint.");

  std::vector<std::string> lEndpoints {
    "/",
    "/devices",
    "/commands",
    "/lease",
    "/fsms",
    "/history",
    "/monitoring"
  };

  for (const auto& x : mBoard.getActionables()) {
    lEndpoints.push_back("/monitoring/" + x);

    const auto& lActionable = mBoard.getActionable(x);
    for (const auto& cmd : lActionable.getCommands())
      lEndpoints.push_back("/commands/" + x + "/" + cmd);

    for (const auto& fsmId : lActionable.getStateMachines()) {
      const auto& lFSM = lActionable.getStateMachine(fsmId);
      for (const auto& lState : lFSM.getStates()) {
        for (const auto& lTransition : lFSM.getTransitions(lState))
          lEndpoints.push_back("/fsms/" + x + "/" + fsmId + "/" + lState + "/" + lTransition.first);
      }
    }
  }

  httplib::Client lClient("localhost", mPort);
  lClient.set_read_timeout(30);
  size_t lErrorCount = 0;
  for (const auto& x : lEndpoints) {
    LOG4CPLUS_DEBUG(sLogger, "Performing startup check of GET " << x);
    if (const auto lResponse = lClient.Get(x)) {
      if (lResponse->status != httplib::StatusCode::OK_200) {
        LOG4CPLUS_ERROR(sLogger, "Startup check of GET " << x << "  =>  status code " << lResponse->status);
        lErrorCount++;
      }
    }
    else {
      const auto err = lResponse.error();
      LOG4CPLUS_ERROR(sLogger, "Startup check of GET " << x << "  =>  HTTP error: " << httplib::to_string(err));
      lErrorCount++;
    }
  }

  if (lErrorCount > 0) {
    if (aMode == RunMode::kIgnoreStartUpTestFailures)
      LOG4CPLUS_ERROR(sLogger, "Bad status code received from " << lErrorCount << " endpoints.");
    else {
      LOG4CPLUS_ERROR(sLogger, "Bad status code received from " << lErrorCount << " endpoints. Stopping server.");
      aServer.stop();
      return 1;
    }
  }

  if (aMode == RunMode::kStopAfterStartUpTests) {
    LOG4CPLUS_INFO(sLogger, "Server startup tests all passed. Shutting down server since running in test mode.");
    aServer.stop();
    return 0;
  }

  // PART 2: Stop server after inactivity if requested
  std::stringstream lLogMessage;
  if (lErrorCount == 0)
    lLogMessage << "Server startup tests all passed.";
  else
    lLogMessage << "Continuing after startup test failures (as instructed).";
  if (mTimeout)
    lLogMessage << " Will automatically stop server after " << mTimeout->count() << " seconds of inactivity.";
  LOG4CPLUS_INFO(sLogger, lLogMessage.str());

  stopAfterTimeout(aServer);
  return 0;
}


void Server::stopAfterTimeout(httplib::Server& aServer)
{
  using std::chrono::steady_clock;
  steady_clock::time_point lWakeUpTime;
  while (true) {
    std::unique_lock<std::mutex> lLock(mShutdownMutex);
    if (not mTimeout)
      mShutdownCondVar.wait(lLock);
    else {
      if (mPendingResponseCount > 0)
        lWakeUpTime = steady_clock::now() + *mTimeout;
      else
        lWakeUpTime = mLastResponseSent.steady + *mTimeout;

      mShutdownCondVar.wait_until(lLock, lWakeUpTime);
    }

    if (mShutdownSignalRcvd.first) {
      LOG4CPLUS_INFO(sLogger, "Stopping the server after receiving signal " << SignalFmt(mShutdownSignalRcvd.second));
      aServer.stop();
      return;
    }
    else if (mTimeout and (mPendingResponseCount == 0) and (steady_clock::now() >= (mLastResponseSent.steady + *mTimeout))) {
      const auto lTimeSinceLastResponse = std::chrono::duration<float>(steady_clock::now() - mLastResponseSent.steady).count();
      LOG4CPLUS_INFO(sLogger, "Stopping the server since timeout is " << mTimeout->count() << " seconds and last response was sent " << lTimeSinceLastResponse << " seconds ago");
      aServer.stop();
      return;
    }
  }
}


std::thread Server::launchSignalHandler(bool& aStop)
{
  // 1) Block termination signals on current thread - so that they're only delivered to the dedicated handler thread
  sigset_t lTerminationSignalSet = convertToSignalSet(getTerminationSignals());
  pthread_sigmask(SIG_BLOCK, &lTerminationSignalSet, nullptr);

  // 2) Launch the signal handler thread
  std::thread lThread([this, &aStop] { this->handleTerminationSignal(aStop); });

  return lThread;
}


void Server::handleTerminationSignal(bool& aStop)
{
  std::vector<int> lTerminationSignals = getTerminationSignals();
  sigset_t lTerminationSignalSet = convertToSignalSet(lTerminationSignals);

  std::ostringstream lSignalList;
  for (const auto i : lTerminationSignals)
    lSignalList << " " << SignalFmt(i);
  LOG4CPLUS_INFO(sLogger, "Started signal handler thread. Termination signals:" << lSignalList.str());

  int lRC;
  siginfo_t lSigInfo;
  timespec lTimeout;
  lTimeout.tv_sec = 0;
  lTimeout.tv_nsec = 500000000;

  while (true) {
    lRC = sigtimedwait(&lTerminationSignalSet, &lSigInfo, &lTimeout);

    if (lRC != -1) {
      LOG4CPLUS_INFO(sLogger, "Signal handler thread: Received a termination signal, " << SignalFmt(lRC));
      mShutdownSignalRcvd = std::make_pair(true, lRC);
      mShutdownCondVar.notify_all();
      return;
    }
    else {
      int lError = errno;
      if ((lError == EAGAIN) and aStop) {
        LOG4CPLUS_INFO(sLogger, "Signal handler thread: Stopping as requested");
        return;
      }
      else if (lError == EINTR)
        LOG4CPLUS_ERROR(sLogger, "Signal handler thread: sigtimedwait returned -1 and errno=EINTR, indicating a problem");
      else if (lError == EINVAL)
        LOG4CPLUS_ERROR(sLogger, "Signal handler thread: sigtimedwait returned -1 and errno=EINVAL, indicating a problem");
      else if (lError != EAGAIN)
        LOG4CPLUS_ERROR(sLogger, "Signal handler thread: sigtimedwait returned -1 and errno = " << lError << " (unexpected)");
    }
  }
}


std::vector<int> Server::getTerminationSignals()
{
  std::vector<int> lResult;
  if (not((std::getenv("RUN_HERD_IN_GDB") != 0) and (std::strcmp(std::getenv("RUN_HERD_IN_GDB"), "1") == 0)))
    lResult.push_back(SIGINT);
  lResult.push_back(SIGTERM);
  return lResult;
}


sigset_t Server::convertToSignalSet(const std::vector<int>& aSignals)
{
  sigset_t lSignalSet;
  sigemptyset(&lSignalSet);
  for (const auto i : aSignals)
    sigaddset(&lSignalSet, i);
  return lSignalSet;
}


void Server::initialiseThreadPool(size_t aNumberOfActionables)
{
  const size_t lHwConcurrency = std::thread::hardware_concurrency();
  const size_t lDesiredNumThreads = (lHwConcurrency == 0) ? aNumberOfActionables : std::min(lHwConcurrency, aNumberOfActionables);
  LOG4CPLUS_INFO(sLogger, "Initialising SWATCH thread pool with " << lDesiredNumThreads << " threads (given " << aNumberOfActionables << " actionables, HW concurrency = " << lHwConcurrency << ")");
  swatch::action::ThreadPool::getInstance(lDesiredNumThreads);
}


void Server::invokeGetCallback(Server& aServer, const Server::GetCallback& aCallback, const httplib::Request& aRequest, httplib::Response& aResponse, bool aLogMessageData)
{
  const auto lStart = swatch::core::TimePoint::now().steady;
  LOG4CPLUS_TRACE(sLogger, "HTTP " << aRequest.method << " " << aRequest.path << " (" << aRequest.body.size() << " bytes) from " << aRequest.remote_addr << ":" << aRequest.remote_port);

  {
    std::unique_lock<std::mutex> lLock(aServer.mShutdownMutex);
    aServer.mPendingResponseCount++;
  }

  const auto lStartOfCallback = swatch::core::TimePoint::now().steady;
  ResponseSpec lResult;
  try {
    const std::vector<std::string> lPathMatches(aRequest.matches.begin() + 1, aRequest.matches.end());
    lResult = aCallback(aServer, lPathMatches, aRequest.params, Json::Value());
  }
  catch (const std::exception& e) {
    LOG4CPLUS_ERROR(sLogger, "Exception [" << core::demangleName(typeid(e).name()) << "] thrown when handling " << aRequest.method << " " << aRequest.path << ": " << e.what());
    lResult = getErrorResponse(500, "ExceptionThrown", "Exception thrown (" + core::demangleName(typeid(e).name()) + "): " + e.what());
  }
  const auto lEndOfCallback = swatch::core::TimePoint::now().steady;

  const std::string lResponseData = writeToString(*lResult.data);
  aResponse.set_content(lResponseData.c_str(), "application/json");
  aResponse.set_header("Access-Control-Allow-Methods", "GET, PUT, OPTIONS");
  aResponse.set_header("Access-Control-Allow-Origin", "*");
  aResponse.status = lResult.statusCode;
  const auto lEndOfCreatingResponse = swatch::core::TimePoint::now().steady;

  std::unique_lock<std::mutex> lLock(aServer.mShutdownMutex);
  const auto lEnd = swatch::core::TimePoint::now();
  aServer.mLastResponseSent = lEnd;
  aServer.mPendingResponseCount--;

  // clang-format off
  LOG4CPLUS_TRACE(sLogger, "HTTP " << aRequest.method << " " << aRequest.path << " " <<
                           "handled in " << int(std::chrono::duration<float>(lEnd.steady - lStart).count() * 1e6) << " microseconds (" <<
                           int(1e6 * std::chrono::duration<float>(lEndOfCallback - lStartOfCallback).count()) << "us in callback, " <<
                           int(1e6 * std::chrono::duration<float>(lEndOfCreatingResponse - lEndOfCallback).count()) << "us writing " << lResponseData.size() << "-byte JSON, " <<
                           int(1e6 * std::chrono::duration<float>(lStartOfCallback - lStart).count()) << "us + " <<
                           int(1e6 * std::chrono::duration<float>(lEnd.steady - lEndOfCreatingResponse).count()) << "us updating counters at start & end)");
  // clang-format on
}


void Server::invokePutCallback(Server& aServer, const Server::PutCallback& aCallback, const httplib::Request& aRequest, httplib::Response& aResponse, bool aLogMessageData)
{
  const auto lStart = swatch::core::TimePoint::now().steady;
  LOG4CPLUS_TRACE(sLogger, "HTTP " << aRequest.method << " " << aRequest.path << " (" << aRequest.body.size() << " bytes) from " << aRequest.remote_addr << ":" << aRequest.remote_port);

  {
    std::unique_lock<std::mutex> lLock(aServer.mShutdownMutex);
    aServer.mPendingResponseCount++;
  }

  const auto lStartOfParsing = swatch::core::TimePoint::now().steady;
  const std::unique_ptr<Json::CharReader> lJsonReader(Json::CharReaderBuilder().newCharReader());
  Json::Value lRequestData;
  std::string lError;
  ResponseSpec lResult;

  auto lContentType = aRequest.get_header_value("Content-Type");

  std::chrono::steady_clock::time_point lStartOfCallback;
  if ((not aRequest.body.empty()) and (lContentType != "application/json")) {
    lResult = getErrorResponse(415, "InvalidRequest", "Request 'Content-Type' must be 'application/json' (received '" + lContentType + "').");
  }
  else if ((not aRequest.body.empty()) and (not lJsonReader->parse(aRequest.body.data(), aRequest.body.data() + aRequest.body.size(), &lRequestData, &lError))) {
    lResult = getErrorResponse(400, "InvalidRequest", "Request's body is not valid JSON: " + lError);
  }
  else {
    lStartOfCallback = swatch::core::TimePoint::now().steady;
    try {
      if (aLogMessageData and not aRequest.body.empty())
        LOG4CPLUS_TRACE(sLogger, "HTTP " << aRequest.method << " " << aRequest.path << " body:" << std::endl
                                         << TruncatedJsonFormatter(lRequestData));

      const std::vector<std::string> lPathMatches(aRequest.matches.begin() + 1, aRequest.matches.end());
      lResult = aCallback(aServer, lPathMatches, lRequestData);
    }
    catch (const std::exception& e) {
      LOG4CPLUS_ERROR(sLogger, "Exception [" << core::demangleName(typeid(e).name()) << "] thrown when handling " << aRequest.method << " " << aRequest.path << ": " << e.what());
      lResult = getErrorResponse(500, "ExceptionThrown", "Exception thrown (" + core::demangleName(typeid(e).name()) + "): " + e.what());
    }
  }
  const auto lEndOfCallback = swatch::core::TimePoint::now().steady;

  const std::string lResponseData = writeToString(*lResult.data);
  aResponse.set_content(lResponseData.c_str(), "application/json");
  aResponse.set_header("Access-Control-Allow-Methods", "GET, PUT, OPTIONS");
  aResponse.set_header("Access-Control-Allow-Origin", "*");
  aResponse.status = lResult.statusCode;
  const auto lEndOfCreatingResponse = swatch::core::TimePoint::now().steady;

  std::unique_lock<std::mutex> lLock(aServer.mShutdownMutex);
  const auto lEnd = swatch::core::TimePoint::now();
  aServer.mLastResponseSent = lEnd;
  aServer.mPendingResponseCount--;

  // clang-format off
  LOG4CPLUS_TRACE(sLogger, "HTTP " << aRequest.method << " " << aRequest.path << " " <<
                           "handled in " << int(std::chrono::duration<float>(lEnd.steady - lStart).count() * 1e6) << " microseconds (" <<
                           int(1e6 * std::chrono::duration<float>(lStartOfCallback - lStartOfParsing).count()) << "us parsing " << aRequest.body.size() << "-byte request, " <<
                           int(1e6 * std::chrono::duration<float>(lEndOfCallback - lStartOfCallback).count()) << "us in callback, " <<
                           int(1e6 * std::chrono::duration<float>(lEndOfCreatingResponse - lEndOfCallback).count()) << "us writing " << lResponseData.size() << "-byte JSON, " <<
                           int(1e6 * std::chrono::duration<float>(lStartOfParsing - lStart).count()) << "us + " <<
                           int(1e6 * std::chrono::duration<float>(lEnd.steady - lEndOfCreatingResponse).count()) << "us updating counters at start & end)");
  // clang-format on
}


void Server::postActionCallback(const action::Functionoid& aFunc)
{
  LOG4CPLUS_INFO(sLogger, "Running HERD server post-action callback for " << aFunc.getPath());

  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto* lTransition = dynamic_cast<const action::Transition*>(&aFunc)) {
    if (mEventHistory.back().type == HistoricalEvent::kTransition) {
      // Update the transition status in the event history
      const action::TransitionSnapshot lSnapshot = lTransition->getStatus();
      mEventHistory.back().status = truncateJSON(*encodeTransitionSnapshot(lSnapshot));
    }
    else {
      LOG4CPLUS_ERROR(sLogger, "Post-action callback for " << aFunc.getPath() << ": The last event in the history buffer is not a transition!");
    }
  }
  else if (const auto* lCommand = dynamic_cast<const action::Command*>(&aFunc)) {
    if (mEventHistory.back().type == HistoricalEvent::kCommand) {
      // Update the command status in the event history
      const action::CommandSnapshot lSnapshot = lCommand->getStatus();
      mEventHistory.back().status = truncateJSON(*encodeCommandSnapshot(lSnapshot));
    }
    else {
      LOG4CPLUS_ERROR(sLogger, "Post-action callback for " << aFunc.getPath() << ": The last event in the history buffer is not a command!");
    }
  }
  else
    LOG4CPLUS_ERROR(sLogger, "Post-action callback for " << aFunc.getPath() << ": Could not cast functionoid to Command or Transition!");

  // Remove files created to run command/transition (e.g. local copies of file-type parameters)
  mRemnants.at(&dynamic_cast<const action::ObjectFunctionoid&>(aFunc).getActionable()).cleanup(sLogger, "Post-action callback for " + aFunc.getPath());
  mRemnants.erase(&dynamic_cast<const action::ObjectFunctionoid&>(aFunc).getActionable());
}


Server::ResponseSpec Server::executeGetGeneralInfo(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  const auto lCurrentTime = swatch::core::SteadyTimePoint_t::clock::now();

  auto data = std::make_shared<Json::Value>(Json::objectValue);
  (*data)["hardwareType"] = mBoard.getHardwareType();
  (*data)["startTime"] = std::chrono::duration<double>(mStartTime.system.time_since_epoch()).count();
  (*data)["uptime"] = std::chrono::duration<float>(lCurrentTime - mStartTime.steady).count();

  (*data)["packages"] = Json::Value(Json::objectValue);
  using swatch::phase2::PackageInfo;
  using swatch::phase2::PackageRegistry;
  for (auto lIt = PackageRegistry::begin(); lIt != PackageRegistry::end(); lIt++) {
    Json::Value lPackageData(Json::objectValue);
    lPackageData["version"] = Json::Value(Json::arrayValue);
    lPackageData["version"].append(Json::Value::UInt(lIt->second.version.major));
    lPackageData["version"].append(Json::Value::UInt(lIt->second.version.minor));
    lPackageData["version"].append(Json::Value::UInt(lIt->second.version.patch));
    // lPackageData["version"].append(lIt->version.prerelease);

    Json::Value lVcsData(Json::nullValue);
    if (lIt->second.vcs) {
      lVcsData = Json::Value(Json::objectValue);
      lVcsData["type"] = "git";
      lVcsData["sha"] = lIt->second.vcs->sha;
      lVcsData["clean"] = lIt->second.vcs->clean;
      if (lIt->second.vcs->ref)
        lVcsData[lIt->second.vcs->ref->first == PackageInfo::Git::kTag ? "tag" : "branch"] = lIt->second.vcs->ref->second;
    }
    lPackageData["versionControlSystem"] = lVcsData;

    Json::Value lBuildData(Json::objectValue);
    if (const PackageInfo::LocalBuild* lBuildInfo = boost::get<const PackageInfo::LocalBuild>(&lIt->second.build)) {
      lBuildData["type"] = "manual";
      lBuildData["time"] = std::chrono::duration<double>(lBuildInfo->time.time_since_epoch()).count();
    }
    else if (const PackageInfo::GitLabBuild* lBuildInfo = boost::get<const PackageInfo::GitLabBuild>(&lIt->second.build)) {
      lBuildData["type"] = "gitlab";
      lBuildData["time"] = std::chrono::duration<double>(lBuildInfo->time.time_since_epoch()).count();
      lBuildData["serverURL"] = lBuildInfo->serverURL;
      lBuildData["projectPath"] = lBuildInfo->projectPath;
      lBuildData["projectID"] = Json::Value::UInt64(lBuildInfo->projectID);
      lBuildData["pipelineID"] = Json::Value::UInt64(lBuildInfo->pipelineID);
      lBuildData["jobID"] = Json::Value::UInt64(lBuildInfo->jobID);
    }
    lPackageData["build"] = lBuildData;

    (*data)["packages"][lIt->second.name] = lPackageData;
  }

  {
    std::lock_guard<std::mutex> lGuard(mMutex);
    (*data)["lease"] = *encodeLeaseInfo(mLease);
  }

  return Server::ResponseSpec(data);
}

Server::ResponseSpec Server::executeGetDevices(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  return Server::ResponseSpec(encodeDeviceInfo(getBoard()));
}


Server::ResponseSpec Server::executeGetLeaseInfo(const std::vector<std::string>&, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  std::lock_guard<std::mutex> lGuard(mMutex);
  return Server::ResponseSpec(encodeLeaseInfo(mLease));
}


Server::ResponseSpec Server::executeObtainLease(const std::vector<std::string>&, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  if (aRequestData.type() != Json::objectValue)
    return ResponseSpec(400, std::make_shared<Json::Value>("Root node in request data must be a JSON object"));

  for (auto x : std::vector<std::string> { "supervisor", "duration" }) {
    if (not aRequestData.isMember(x))
      return ResponseSpec(400, std::make_shared<Json::Value>("Request data is missing mandatory field: " + x));
  }

  const Json::Value& supervisor = aRequestData["supervisor"];
  if (not supervisor.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'supervisor' field is of incorrect type (must be a string)"));

  const Json::Value& duration = aRequestData["duration"];
  if (not duration.isNumeric())
    return ResponseSpec(400, std::make_shared<Json::Value>("'duration' field is of incorrect type (must be a number)"));

  if (duration.asFloat() <= 0.0)
    return ResponseSpec(400, std::make_shared<Json::Value>("'duration' field has invalid value, " + std::to_string(duration.asFloat()) + " (must be positive and non-zero)"));

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (mLease and (not mLease->hasExpired())) {
    std::string errorMessage;
    if (supervisor.asString() == mLease->supervisor) {
      LOG4CPLUS_DEBUG(sLogger, "Lease request (with supervisor '" << supervisor.asString() << "') denied - requestor already holds the lease");
      errorMessage = "You already hold the lease (expires in " + std::to_string(mLease->remainingTime()) + " seconds)";
    }
    else {
      LOG4CPLUS_DEBUG(sLogger, "Lease request (with supervisor '" << supervisor.asString() << "') denied - another application ('" << mLease->supervisor << "') still holds the lease");
      errorMessage = "Another application, '" + mLease->supervisor + "', already holds the lease (expires in " + std::to_string(mLease->remainingTime()) + " seconds)";
    }
    return getErrorResponse(409, "LeaseRequestDenied", errorMessage);
  }

  // PART 3: CHANGE CURRENT LEASE STATE
  mLease = Lease(supervisor.asString(), duration.asDouble());
  LOG4CPLUS_INFO(sLogger, "Lease created (supervisor = \"" << mLease->supervisor << "\", remainingTime = " << mLease->remainingTime() << ")");

  /// PART 4: Check the lease info and fill the history

  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kLeaseObtain;
  objHistoryData.deviceID = "NULL";
  objHistoryData.id = "NULL";
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NULL";
  objHistoryData.transID = "NULL";
  objHistoryData.params = "NULL";
  objHistoryData.status = "NULL";
  objHistoryData.lease = *encodeLeaseInfo(*mLease);

  appendToEventHistory(objHistoryData, lGuard);

  return ResponseSpec(encodeLeaseToken(*mLease));
}


Server::ResponseSpec Server::executeRenewLease(const std::vector<std::string>&, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  if (aRequestData.type() != Json::objectValue)
    return ResponseSpec(400, std::make_shared<Json::Value>("Root node in request data must be a JSON object"));

  for (auto x : std::vector<std::string> { "lease", "duration" }) {
    if (not aRequestData.isMember(x))
      return ResponseSpec(400, std::make_shared<Json::Value>("Request data is missing mandatory field: " + x));
  }

  const Json::Value& requestId = aRequestData["lease"];
  if (not requestId.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'lease' field is of incorrect type (must be a string)"));

  const Json::Value& duration = aRequestData["duration"];
  if (not duration.isNumeric())
    return ResponseSpec(400, std::make_shared<Json::Value>("'duration' field is of incorrect type (must be a number)"));

  if (duration.asFloat() <= 0.0)
    return ResponseSpec(400, std::make_shared<Json::Value>("'duration' field has invalid value, " + std::to_string(duration.asFloat()) + " (must be positive and non-zero)"));

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(requestId.asString(), lGuard))
    return *lResponse;

  // PART 3: CHANGE CURRENT LEASE STATE
  mLease->renew(duration.asFloat());
  LOG4CPLUS_INFO(sLogger, "Lease renewed (supervisor = \"" << mLease->supervisor << "\", remainingTime = " << mLease->remainingTime() << ")");

  // PART 4: Check lease info and fill the history
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kLeaseRenew;
  objHistoryData.deviceID = "NULL";
  objHistoryData.id = "NULL";
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NULL";
  objHistoryData.transID = "NULL";
  objHistoryData.params = "NULL";
  objHistoryData.status = "NULL";
  objHistoryData.lease = *encodeLeaseInfo(*mLease);

  appendToEventHistory(objHistoryData, lGuard);

  return ResponseSpec(encodeLeaseToken(*mLease));
}


Server::ResponseSpec Server::executeRetireLease(const std::vector<std::string>&, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  if (aRequestData.type() != Json::objectValue)
    return ResponseSpec(400, std::make_shared<Json::Value>("Root node in request data must be a JSON object"));

  for (auto x : std::vector<std::string> { "lease" }) {
    if (not aRequestData.isMember(x))
      return ResponseSpec(400, std::make_shared<Json::Value>("Request data is missing mandatory field: " + x));
  }

  const Json::Value& requestId = aRequestData["lease"];
  if (not requestId.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'lease' field is of incorrect type (must be a string)"));

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  const std::string lSupervisor(mLease ? mLease->supervisor : "");
  if (const auto lResponse = checkLease(requestId.asString(), lGuard))
    return *lResponse;

  // PART 2.b
  // before mLease is retired, check info abt it
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kLeaseRetire;
  objHistoryData.deviceID = "NULL";
  objHistoryData.id = "NULL";
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NULL";
  objHistoryData.transID = "NULL";
  objHistoryData.params = "NULL";
  objHistoryData.status = "NULL";
  objHistoryData.lease = *encodeLeaseInfo(*mLease);

  appendToEventHistory(objHistoryData, lGuard);

  // PART 3: CHANGE CURRENT LEASE STATE
  LOG4CPLUS_INFO(sLogger, "Retiring lease (supervisor = \"" << mLease->supervisor << "\", remainingTime = " << mLease->remainingTime() << ")");
  mLease.reset();

  return ResponseSpec();
}


Server::ResponseSpec Server::executeGetCommands(const std::vector<std::string>& aPathMatches, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  // // PART 1: INPUT VALIDATION
  // // Expect 1 path segments - DEVICE_ID
  ResponseSpec lResult;

  // PART 2: GET THE STATUS
  return ResponseSpec(encodeCommandInfo(mBoard));
}


Server::ResponseSpec Server::executeRunCommand(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 2 path segments - DEVICE_ID and COMMAND_ID
  if (aPathMatches.size() != 2)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getCommands().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified command ('" + aPathMatches.at(1) + "') does not exist");

  auto& lCommand = lDevice.getCommand(aPathMatches.at(1));

  // PART 1b: INPUT VALIDATION - REQUEST BODY
  if (aRequestData.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "Root node in request data must be a JSON object");

  for (auto x : std::vector<std::string> { "lease", "parameters" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lParamSetObj = aRequestData["parameters"];
  if (lParamSetObj.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "'parameters' field is of incorrect type (must be an object)");

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return getErrorResponse(400, "InvalidRequst", "'lease' field is of incorrect type (must be a string)");

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: START THE COMMAND
  const auto lStatus = lDevice.getStatus();
  if (lStatus.isRunning()) {
    std::ostringstream lMessage;
    lMessage << "'" << lDevice.getId() << "' is already busy running ";
    lMessage << action::BusyGuard::ActionFmt(lStatus.getRunningActions().at(0));
    return getErrorResponse(409, "ObjectBusy", lMessage.str());
  }

  LOG4CPLUS_INFO(sLogger, "Running command '" << lCommand.getId() << "' of device '" << lDevice.getId() << "'");
  const auto paramsAndRemnants = extractParameterSet(lCommand, lParamSetObj);
  mRemnants[&lDevice] = paramsAndRemnants.second;
  lCommand.exec(paramsAndRemnants.first, true);

  // PART 4: GET THE STATUS
  ResponseSpec lResult;
  lResult.data = encodeCommandSnapshot(lCommand.getStatus());

  // PART 5: Fill in the variables for history data
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kCommand;
  objHistoryData.deviceID = aPathMatches.at(0);
  objHistoryData.id = aPathMatches.at(1);
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NA";
  objHistoryData.transID = "NA";

  objHistoryData.params = truncateJSON(lParamSetObj);
  objHistoryData.status = *lResult.data;
  objHistoryData.lease = "NULL";

  appendToEventHistory(objHistoryData, lGuard);

  return lResult;
}


Server::ResponseSpec Server::executeGetCommandStatus(const std::vector<std::string>& aPathMatches, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  // PART 1: INPUT VALIDATION
  // Expect 2 path segments - DEVICE_ID and COMMAND_ID

  if (aPathMatches.size() != 2)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  const auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getCommands().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified command ('" + aPathMatches.at(1) + "') does not exist");

  auto& lCommand = lDevice.getCommand(aPathMatches.at(1));

  // PART 2: GET THE STATUS
  ResponseSpec lResult;
  lResult.data = encodeCommandSnapshot(lCommand.getStatus());
  return lResult;
}


Server::ResponseSpec Server::executeEngageFSM(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 2 path segments - DEVICE_ID, FSM_ID

  if (aPathMatches.size() != 2)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getStateMachines().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified FSM ('" + aPathMatches.at(1) + "') does not exist");

  for (auto x : std::vector<std::string> { "lease" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(404, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return getErrorResponse(400, "InvalidRequest", "'lease' field is of incorrect type (must be a string)");

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: ENGAGE THE FSM
  LOG4CPLUS_INFO(sLogger, "Engaging FSM '" << aPathMatches.at(1) << "' of device '" << lDevice.getId() << "'");
  lDevice.getStateMachine(aPathMatches.at(1)).engage(swatch::action::GateKeeper("empty"));

  /// PART 4: Fill in the variables for history data
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kFSMEngage;
  objHistoryData.deviceID = aPathMatches.at(0);
  objHistoryData.id = aPathMatches.at(1);
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NA";
  objHistoryData.transID = "NA";
  objHistoryData.params = "NA";
  objHistoryData.status = "NA";
  objHistoryData.lease = "NULL";

  appendToEventHistory(objHistoryData, lGuard);

  return ResponseSpec();
}


Server::ResponseSpec Server::executeResetFSM(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 2 path segments - DEVICE_ID, FSM_ID

  if (aPathMatches.size() != 2)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getStateMachines().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified FSM ('" + aPathMatches.at(1) + "') does not exist");

  for (auto x : std::vector<std::string> { "lease" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return getErrorResponse(400, "InvalidRequest", "'lease' field is of incorrect type (must be a string)");

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: RESET THE FSM
  LOG4CPLUS_INFO(sLogger, "Resetting FSM '" << aPathMatches.at(1) << "' of device '" << lDevice.getId() << "'");
  lDevice.getStateMachine(aPathMatches.at(1)).reset(swatch::action::GateKeeper("empty"));

  // PART 4 : Fill in the variables for history data
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kFSMReset;
  objHistoryData.deviceID = aPathMatches.at(0);
  objHistoryData.id = aPathMatches.at(1);
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NA";
  objHistoryData.transID = "NA";
  objHistoryData.params = "NA";
  objHistoryData.status = "NA";
  objHistoryData.lease = "NULL";

  appendToEventHistory(objHistoryData, lGuard);

  return ResponseSpec();
}


Server::ResponseSpec Server::executeDisengageFSM(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 2 path segments - DEVICE_ID, FSM_ID

  if (aPathMatches.size() != 2)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getStateMachines().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified FSM ('" + aPathMatches.at(1) + "') does not exist");

  for (auto x : std::vector<std::string> { "lease" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return getErrorResponse(400, "InvalidRequest", "'lease' field is of incorrect type (must be a string)");

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: DISENGAGE THE FSM
  LOG4CPLUS_INFO(sLogger, "Disengaging from FSM '" << aPathMatches.at(1) << "' of device '" << lDevice.getId() << "'");
  lDevice.getStateMachine(aPathMatches.at(1)).disengage();

  // PART 4: Fill in the variables for history data
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kFSMDisengage;
  objHistoryData.deviceID = aPathMatches.at(0);
  objHistoryData.id = aPathMatches.at(1);
  objHistoryData.time = swatch::core::TimePoint::now();
  objHistoryData.stateID = "NA";
  objHistoryData.transID = "NA";
  objHistoryData.params = "NA";
  objHistoryData.status = "NA";
  objHistoryData.lease = "NULL";

  appendToEventHistory(objHistoryData, lGuard);

  return ResponseSpec();
}


Server::ResponseSpec Server::executeGetFSMs(const std::vector<std::string>& aPathMatches, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  // // PART 1: INPUT VALIDATION
  // // Expect 1 path segments - DEVICE_ID
  ResponseSpec lResult;

  // if (aPathMatches.size() != 1) {
  //   lResult.data = Json::Value("Invalid URL path");
  //   lResult.statusCode = 404;
  //   return lResult;
  // }

  // if (aBoard.getActionables().count(aPathMatches.at(0)) == 0) {
  //   lResult.data = Json::Value("Specified device ('" + aPathMatches.at(0) + "') does not exist");
  //   lResult.statusCode = 404;
  //   return lResult;
  // }

  // PART 2: GET THE STATUS
  (*lResult.data) = Json::Value(Json::objectValue);
  for (const auto& lId : mBoard.getActionables())
    (*lResult.data)[lId] = encodeFsmInfo(mBoard.getActionable(lId));
  return lResult;
}


Server::ResponseSpec Server::executeRunTransition(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 4 path segments - DEVICE_ID, FSM_ID, STATE_ID, and TRANSITION_ID

  if (aPathMatches.size() != 4)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getStateMachines().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified FSM ('" + aPathMatches.at(1) + "') does not exist");

  auto& lFSM = lDevice.getStateMachine(aPathMatches.at(1));

  if (std::count(lFSM.getStates().begin(), lFSM.getStates().end(), aPathMatches.at(2)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified state ('" + aPathMatches.at(2) + "') does not exist in FSM '" + aPathMatches.at(1) + "'");

  if (lFSM.getTransitions(aPathMatches.at(2)).count(aPathMatches.at(3)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified transition ('" + aPathMatches.at(3) + "') does not exist in state '" + aPathMatches.at(2) + "' of FSM '" + aPathMatches.at(1) + "'");

  auto& lTransition = lFSM.getTransition(aPathMatches.at(2), aPathMatches.at(3));

  // PART 1b: INPUT VALIDATION - REQUEST BODY
  if (aRequestData.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "Root node in request data must be a JSON object");

  for (auto x : std::vector<std::string> { "lease", "parameters" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lParamSets = aRequestData["parameters"];

  if (lParamSets.type() != Json::arrayValue)
    return getErrorResponse(400, "InvalidRequest", "'parameters' field is of incorrect type (must be an array)");

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return getErrorResponse(400, "InvalidRequest", "'lease' field is of incorrect type (must be a string)");

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: START THE TRANSITION
  const auto lStatus = lDevice.getStatus();
  if (lStatus.isRunning()) {
    std::ostringstream lMessage;
    lMessage << "'" << lDevice.getId() << "' is already busy running ";
    lMessage << action::BusyGuard::ActionFmt(lStatus.getRunningActions().at(0));
    return getErrorResponse(409, "ObjectBusy", lMessage.str());
  }

  LOG4CPLUS_INFO(sLogger, "Running transition '" << lTransition.getId() << "' of device '" << lDevice.getId() << "'");
  const auto paramsAndRemnants = extractParameterSets(lTransition, lParamSets);
  mRemnants[&lDevice] = paramsAndRemnants.second;
  lTransition.exec(paramsAndRemnants.first, NULL, true);

  // PART 4: GET THE STATUS
  ResponseSpec lResult;
  lResult.data = encodeTransitionSnapshot(lTransition.getStatus());

  // PART 5: Fill in the variables for history data
  HistoricalEvent objHistoryData;
  objHistoryData.type = HistoricalEvent::kTransition;
  objHistoryData.deviceID = aPathMatches.at(0);
  objHistoryData.id = aPathMatches.at(1);
  objHistoryData.time = core::TimePoint::now();
  objHistoryData.stateID = aPathMatches.at(2);
  objHistoryData.transID = aPathMatches.at(3);

  objHistoryData.params = truncateJSON(lParamSets);
  objHistoryData.status = *lResult.data;
  objHistoryData.lease = "NULL";

  appendToEventHistory(objHistoryData, lGuard);

  return lResult;
}


Server::ResponseSpec Server::executeGetTransitionStatus(const std::vector<std::string>& aPathMatches, const std::multimap<std::string, std::string>&, const Json::Value&)
{
  // PART 1: INPUT VALIDATION
  // Expect 4 path segments - DEVICE_ID, FSM_ID, STATE_ID, and TRANSITION_ID

  if (aPathMatches.size() != 4)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getActionables().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified device ('" + aPathMatches.at(0) + "') does not exist");

  auto& lDevice = mBoard.getActionable(aPathMatches.at(0));

  if (lDevice.getStateMachines().count(aPathMatches.at(1)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified FSM ('" + aPathMatches.at(1) + "') does not exist");

  auto& lFSM = lDevice.getStateMachine(aPathMatches.at(1));

  if (std::count(lFSM.getStates().begin(), lFSM.getStates().end(), aPathMatches.at(2)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified state ('" + aPathMatches.at(2) + "') does not exist in FSM '" + aPathMatches.at(1) + "'");

  if (lFSM.getTransitions(aPathMatches.at(2)).count(aPathMatches.at(3)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified transition ('" + aPathMatches.at(3) + "') does not exist in state '" + aPathMatches.at(2) + "' of FSM '" + aPathMatches.at(1) + "'");

  auto& lTransition = lFSM.getTransition(aPathMatches.at(2), aPathMatches.at(3));

  // PART 2: GET THE STATUS
  ResponseSpec lResult;
  lResult.data = encodeTransitionSnapshot(lTransition.getStatus());
  return lResult;
}


Server::ResponseSpec Server::executeUpdateProperties(const std::vector<std::string>& aPathMatches, const Json::Value&)
{
  // PART 1: INPUT VALIDATION
  // Expect 1 path segment - ID of service module, a processor, or an optical module
  if (aPathMatches.size() != 1)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  core::Object* lObj = NULL;
  if (mBoard.getServiceModule().getId() == aPathMatches.at(0))
    lObj = &mBoard.getServiceModule();
  else if (mBoard.getProcessors().count(aPathMatches.at(0)) != 0)
    lObj = &mBoard.getProcessor(aPathMatches.at(0));
  else if (mBoard.getOpticalModules().count(aPathMatches.at(0)) != 0)
    lObj = &mBoard.getOpticalModule(aPathMatches.at(0));
  else
    return getErrorResponse(404, "InvalidRequest", "Specified ID ('" + aPathMatches.at(0) + "') does not match any processor, optical module or the service module");

  // PART 2: UPDATE THE PROPERTIES
  LOG4CPLUS_INFO(sLogger, "Updating properties of component '" << lObj->getPath() << "'");
  core::Object::iterator lObjectIt(*lObj);
  do {
    if (action::PropertyHolder* lPropHolder = dynamic_cast<action::PropertyHolder*>(&*lObjectIt))
      lPropHolder->updateProperties();
  } while (lObjectIt.next());

  return ResponseSpec();
}


Server::ResponseSpec Server::executeMaskInputPorts(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 1 path segment - PROCESSOR_ID
  if (aPathMatches.size() != 1)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getProcessors().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified processor FPGA ('" + aPathMatches.at(0) + "') does not exist");

  auto& lProcessor = mBoard.getProcessor(aPathMatches.at(0));

  // PART 1b: INPUT VALIDATION - REQUEST BODY
  if (aRequestData.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "Root node in request data must be a JSON object");

  for (auto x : std::vector<std::string> { "lease", "mask", "clear" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'lease' field is of incorrect type (must be a string)"));

  std::vector<phase2::InputPort*> lPortsToMask, lPortsToUnmask;
  std::vector<std::string> lMissingPorts;
  if (not aRequestData["mask"].isArray())
    return ResponseSpec(400, std::make_shared<Json::Value>("'mask' field is of incorrect type (must be an array of unsigned integers)"));

  if (not aRequestData["clear"].isArray())
    return ResponseSpec(400, std::make_shared<Json::Value>("'clear' field is of incorrect type (must be an array of unsigned integers)"));

  for (const Json::Value& x : aRequestData["mask"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'mask' field is of incorrect type (must be an array of unsigned integers)"));

    if (auto* lPort = &lProcessor.getInputPorts().getPort(x.asUInt()))
      lPortsToMask.push_back(lPort);
    else
      lMissingPorts.push_back(x.asString());
  }

  for (const Json::Value& x : aRequestData["clear"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'clear' field is of incorrect type (must be an array of unsigned integers)"));

    if (auto* lPort = &lProcessor.getInputPorts().getPort(x.asUInt()))
      lPortsToUnmask.push_back(lPort);
    else
      lMissingPorts.push_back(x.asString());
  }

  if (not lMissingPorts.empty()) {
    std::ostringstream lMessage;
    lMessage << "Processor FPGA '" << aPathMatches.at(0) << "' does not contain the following specified input ports:";
    for (const auto& x : lMissingPorts)
      lMessage << " " << x;
    return getErrorResponse(404, "InvalidRequest", lMessage.str());
  }

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: MASK/UNMASK SPECIFIED PORTS
  for (auto* x : lPortsToMask)
    x->setMasked(true);

  for (auto* x : lPortsToUnmask)
    x->setMasked(false);

  return ResponseSpec();
}


Server::ResponseSpec Server::executeMaskOutputPorts(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 1 path segment - PROCESSOR_ID
  if (aPathMatches.size() != 1)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getProcessors().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified processor FPGA ('" + aPathMatches.at(0) + "') does not exist");

  auto& lProcessor = mBoard.getProcessor(aPathMatches.at(0));

  // PART 1b: INPUT VALIDATION - REQUEST BODY
  if (aRequestData.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "Root node in request data must be a JSON object");

  for (auto x : std::vector<std::string> { "lease", "mask", "clear" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'lease' field is of incorrect type (must be a string)"));

  std::vector<phase2::OutputPort*> lPortsToMask, lPortsToUnmask;
  std::vector<std::string> lMissingPorts;
  if (not aRequestData["mask"].isArray())
    return ResponseSpec(400, std::make_shared<Json::Value>("'mask' field is of incorrect type (must be an array of unsigned integers)"));

  if (not aRequestData["clear"].isArray())
    return ResponseSpec(400, std::make_shared<Json::Value>("'clear' field is of incorrect type (must be an array of unsigned integers)"));

  for (const Json::Value& x : aRequestData["mask"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'mask' field is of incorrect type (must be an array of unsigned integers)"));

    if (auto* lPort = &lProcessor.getOutputPorts().getPort(x.asUInt()))
      lPortsToMask.push_back(lPort);
    else
      lMissingPorts.push_back(x.asString());
  }

  for (const Json::Value& x : aRequestData["clear"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'clear' field is of incorrect type (must be an array of unsigned integers)"));

    if (auto* lPort = &lProcessor.getOutputPorts().getPort(x.asUInt()))
      lPortsToUnmask.push_back(lPort);
    else
      lMissingPorts.push_back(x.asString());
  }

  if (not lMissingPorts.empty()) {
    std::ostringstream lMessage;
    lMessage << "Processor FPGA '" << aPathMatches.at(0) << "' does not contain the following specified output ports:";
    for (const auto& x : lMissingPorts)
      lMessage << " " << x;
    return getErrorResponse(404, "InvalidRequest", lMessage.str());
  }

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: MASK/UNMASK SPECIFIED PORTS
  for (auto* x : lPortsToMask)
    x->setMasked(true);

  for (auto* x : lPortsToUnmask)
    x->setMasked(false);

  return ResponseSpec();
}


Server::ResponseSpec Server::executeSetPortOperatingModes(const std::vector<std::string>& aPathMatches, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION
  // Expect 1 path segment - PROCESSOR_ID
  if (aPathMatches.size() != 1)
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  if (mBoard.getProcessors().count(aPathMatches.at(0)) == 0)
    return getErrorResponse(404, "InvalidRequest", "Specified processor FPGA ('" + aPathMatches.at(0) + "') does not exist");

  auto& lProcessor = mBoard.getProcessor(aPathMatches.at(0));

  // PART 1b: INPUT VALIDATION - REQUEST BODY (just top-level entries)
  if (aRequestData.type() != Json::objectValue)
    return getErrorResponse(400, "InvalidRequest", "Root node in request data must be a JSON object");

  for (auto x : std::vector<std::string> { "lease", "inputs", "outputs", "loopback", "standard" }) {
    if (not aRequestData.isMember(x))
      return getErrorResponse(400, "InvalidRequest", "Request data is missing mandatory field: " + x);
  }

  const Json::Value& lLease = aRequestData["lease"];
  if (not lLease.isString())
    return ResponseSpec(400, std::make_shared<Json::Value>("'lease' field is of incorrect type (must be a string)"));

  for (const std::string& x : std::vector<std::string> { "inputs", "outputs", "loopback", "standard" }) {
    if (not aRequestData[x].isArray())
      return ResponseSpec(400, std::make_shared<Json::Value>("'" + x + "' field is of incorrect type (must be an array of unsigned integers)"));
  }

  // PART 1c: INPUT VALIDATION - 'inputs' & 'outputs'
  const std::unordered_set<std::string> kValidModeNames { "PRBS7", "PRBS9", "PRBS15", "PRBS23", "PRBS31", "CSP" };
  const std::unordered_set<std::string> kValidIdleMethodNames { "idle1", "idle2" };
  const std::unordered_map<std::string, phase2::PRBSMode> kPRBSModes { { "PRBS7", phase2::kPRBS7 }, { "PRBS9", phase2::kPRBS9 }, { "PRBS15", phase2::kPRBS15 }, { "PRBS23", phase2::kPRBS23 }, { "PRBS31", phase2::kPRBS31 } };
  std::map<phase2::InputPort*, phase2::PRBSMode> lPRBSInputPorts;
  std::map<phase2::OutputPort*, phase2::PRBSMode> lPRBSOutputPorts;
  std::map<phase2::InputPort*, phase2::CSPSettings> lCSPInputPorts;
  std::map<phase2::OutputPort*, phase2::CSPSettings> lCSPOutputPorts;
  std::vector<size_t> lMissingPorts;

  for (const Json::Value& x : aRequestData["inputs"]) {
    if (not x.isArray())
      return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' entry is of incorrect format (must be an array)"));

    if ((x.size() < 2) or (not x[0].isArray()) or (not x[1].isString()))
      return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' entry is of incorrect format (must be an array containing at least two elements - an array and a string)"));

    if (kValidModeNames.count(x[1].asString()) == 0) {
      std::ostringstream lMessage;
      lMessage << "'inputs' entry is of incorrect format: 2nd element must be one of";
      for (const std::string& x : kValidModeNames)
        lMessage << " " << x;
      return ResponseSpec(400, std::make_shared<Json::Value>(lMessage.str()));
    }

    std::vector<phase2::InputPort*> lInputPorts;
    for (const Json::Value& y : x[0]) {
      if (not y.isUInt())
        return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' entry is of incorrect format: First element must be array of unsigned ints (port indices)"));

      if (y.asUInt() >= lProcessor.getInputPorts().getNumPorts())
        lMissingPorts.push_back(y.asUInt());
      else {
        phase2::InputPort* lPort = &lProcessor.getInputPorts().getPort(y.asUInt());
        if ((lCSPInputPorts.count(lPort) > 0) or (lPRBSInputPorts.count(lPort) > 0))
          return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' section is invalid: Input port " + std::to_string(y.asUInt()) + " specified multiple times"));
        lInputPorts.push_back(lPort);
      }
    }

    if (x[1].asString() == "CSP") {
      if ((x.size() != 5) or (not x[2].isUInt()) or (not x[3].isUInt()) or (not x[4].isString()))
        return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' entry is of incorrect format: Each CSP entry must be array of 5 elements (array, string, uint, uint, string)"));

      if (kValidIdleMethodNames.count(x[4].asString()) == 0)
        return ResponseSpec(400, std::make_shared<Json::Value>("'inputs' entry is of incorrect format: Invalid CSP idle method name, \"" + x[4].asString() + "\""));

      phase2::CSPSettings lCSPSettings;
      lCSPSettings.packetLength = x[2].asUInt();
      lCSPSettings.packetPeriodicity = x[3].asUInt();
      lCSPSettings.idleMethod = (x[4].asString() == "idle1") ? phase2::CSPSettings::kIdleMethod1 : phase2::CSPSettings::kIdleMethod2;

      for (const auto& lPort : lInputPorts)
        lCSPInputPorts[lPort] = lCSPSettings;
    }
    else {
      const auto lPRBSMode = kPRBSModes.at(x[1].asString());
      for (const auto& lPort : lInputPorts)
        lPRBSInputPorts[lPort] = lPRBSMode;
    }
  }

  for (const Json::Value& x : aRequestData["outputs"]) {
    if (not x.isArray())
      return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' entry is of incorrect format (must be an array)"));

    if ((x.size() < 2) or (not x[0].isArray()) or (not x[1].isString()))
      return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' entry is of incorrect format (must be an array containing at least two elements - an array and a string)"));

    if (kValidModeNames.count(x[1].asString()) == 0) {
      std::ostringstream lMessage;
      lMessage << "'outputs' entry is of incorrect format: 2nd element must be one of";
      for (const std::string& x : kValidModeNames)
        lMessage << " " << x;
      return ResponseSpec(400, std::make_shared<Json::Value>(lMessage.str()));
    }

    std::vector<phase2::OutputPort*> lOutputPorts;
    for (const Json::Value& y : x[0]) {
      if (not y.isUInt())
        return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' entry is of incorrect format: First element must be array of unsigned ints (port indices)"));

      if (y.asUInt() >= lProcessor.getOutputPorts().getNumPorts())
        lMissingPorts.push_back(y.asUInt());
      else {
        phase2::OutputPort* lPort = &lProcessor.getOutputPorts().getPort(y.asUInt());
        if ((lCSPOutputPorts.count(lPort) > 0) or (lPRBSOutputPorts.count(lPort) > 0))
          return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' section is invalid: Output port " + std::to_string(y.asUInt()) + " specified multiple times"));
        lOutputPorts.push_back(lPort);
      }
    }

    if (x[1].asString() == "CSP") {
      if ((x.size() != 5) or (not x[2].isUInt()) or (not x[3].isUInt()) or (not x[4].isString()))
        return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' entry is of incorrect format: Each CSP entry must be array of 5 elements (array, string, uint, uint, string)"));

      if (kValidIdleMethodNames.count(x[4].asString()) == 0)
        return ResponseSpec(400, std::make_shared<Json::Value>("'outputs' entry is of incorrect format: Invalid CSP idle method name, \"" + x[4].asString() + "\""));

      phase2::CSPSettings lCSPSettings;
      lCSPSettings.packetLength = x[2].asUInt();
      lCSPSettings.packetPeriodicity = x[3].asUInt();
      lCSPSettings.idleMethod = (x[4].asString() == "idle1") ? phase2::CSPSettings::kIdleMethod1 : phase2::CSPSettings::kIdleMethod2;

      for (const auto& lPort : lOutputPorts)
        lCSPOutputPorts[lPort] = lCSPSettings;
    }
    else {
      const auto lPRBSMode = kPRBSModes.at(x[1].asString());
      for (const auto& lPort : lOutputPorts)
        lPRBSOutputPorts[lPort] = lPRBSMode;
    }
  }

  // PART 1d: INPUT VALIDATION - 'loopback' & 'standard'
  std::vector<std::pair<phase2::InputPort*, phase2::OutputPort*>> lLoopbackPorts, lNormalPorts;

  for (const Json::Value& x : aRequestData["loopback"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'loopback' field is of incorrect type (must be an array of unsigned integers)"));

    if (x.asUInt() >= lProcessor.getInputPorts().getNumPorts())
      lMissingPorts.push_back(x.asUInt());
    else if (x.asUInt() >= lProcessor.getOutputPorts().getNumPorts())
      lMissingPorts.push_back(x.asUInt());
    else {
      auto* lInput = &lProcessor.getInputPorts().getPort(x.asUInt());
      auto* lOutput = &lProcessor.getOutputPorts().getPort(x.asUInt());
      lLoopbackPorts.push_back({ lInput, lOutput });
    }
  }

  for (const Json::Value& x : aRequestData["standard"]) {
    if (not x.isUInt())
      return ResponseSpec(400, std::make_shared<Json::Value>("'standard' field is of incorrect type (must be an array of unsigned integers)"));

    if (x.asUInt() >= lProcessor.getInputPorts().getNumPorts())
      lMissingPorts.push_back(x.asUInt());
    else if (x.asUInt() >= lProcessor.getOutputPorts().getNumPorts())
      lMissingPorts.push_back(x.asUInt());
    else {
      auto* lInput = &lProcessor.getInputPorts().getPort(x.asUInt());
      auto* lOutput = &lProcessor.getOutputPorts().getPort(x.asUInt());
      lNormalPorts.push_back({ lInput, lOutput });
    }
  }

  if (not lMissingPorts.empty()) {
    std::ostringstream lMessage;
    lMessage << "Processor FPGA '" << aPathMatches.at(0) << "' does not contain the following specified input/output ports:";
    for (const auto& x : lMissingPorts)
      lMessage << " " << x;
    return getErrorResponse(404, "InvalidRequest", lMessage.str());
  }

  // PART 2: CHECK AGAINST CURRENT LEASE STATE
  std::lock_guard<std::mutex> lGuard(mMutex);
  if (const auto lResponse = checkLease(lLease.asString(), lGuard))
    return *lResponse;

  // PART 3: UPDATE I/O PORT OBJECTS
  for (const auto& x : lPRBSInputPorts)
    x.first->setOperatingMode(x.second);
  for (const auto& x : lCSPInputPorts)
    x.first->setOperatingMode(x.second);

  for (const auto& x : lPRBSOutputPorts)
    x.first->setOperatingMode(x.second);
  for (const auto& x : lCSPOutputPorts)
    x.first->setOperatingMode(x.second);

  for (const auto& x : lLoopbackPorts) {
    x.first->setInLoopback(true);
    x.second->setInLoopback(true);
  }

  for (const auto& x : lNormalPorts) {
    x.first->setInLoopback(false);
    x.second->setInLoopback(false);
  }

  return ResponseSpec();
}


Server::ResponseSpec Server::executeGetMonitoringData(const std::vector<std::string>& aPathMatches, const std::multimap<std::string, std::string>& aParams, const Json::Value& aRequestData)
{
  // PART 1: INPUT VALIDATION - PARAMETERS
  boost::optional<size_t> lDepth;
  const auto lDepthParamIt = aParams.find("depth");
  if (lDepthParamIt != aParams.end()) {
    if (lDepthParamIt->second.find_first_not_of("0123456789") != std::string::npos)
      return getErrorResponse(400, "InvalidRequest", "'depth' parameter has invalid value (must be a positive integer)");

    lDepth = std::stoul(lDepthParamIt->second);
    if (*lDepth == 0)
      return getErrorResponse(400, "InvalidRequest", "'depth' parameter has invalid value (must be a positive, non-zero integer)");
  }

  // PART 2: INPUT VALIDATION ON PATH & FETCH DATA (since not easily separated)
  // Expect 0 or 1 path segments - DEVICE_OR_MODULE_ID

  if (aPathMatches.size() == 0) {
    return Server::ResponseSpec(encodeMonitoringData(getBoard(), lDepth));
  }
  else if (aPathMatches.size() == 1) {
    if (mBoard.getActionables().count(aPathMatches.at(0)) > 0) {
      const auto& lActionable = mBoard.getActionable(aPathMatches.at(0));
      Server::ResponseSpec lResponse;
      encodeMonitorableObject(*lResponse.data, lActionable, lDepth);
      return lResponse;
    }
    else if (mBoard.getOpticalModules().count(aPathMatches.at(0)) > 0) {
      const auto& lModule = mBoard.getOpticalModule(aPathMatches.at(0));
      Server::ResponseSpec lResponse;
      encodeMonitorableObject(*lResponse.data, lModule, lDepth);
      return lResponse;
    }
    else
      return getErrorResponse(404, "InvalidRequest", "Specified device/module ('" + aPathMatches.at(0) + "') does not exist");
  }
  else
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");
}


Server::ResponseSpec Server::executeGetHistoryData(const std::vector<std::string>& aPathMatches,
                                                   const std::multimap<std::string, std::string>&, const Json::Value& aRequestData)
{
  std::lock_guard<std::mutex> lGuard(mMutex);
  auto data = encodeHistoryData(mEventHistory);

  return Server::ResponseSpec(data);
}


void Server::appendToEventHistory(const HistoricalEvent& aEvent, const std::lock_guard<std::mutex>&)
{
  mEventHistory.push_back(aEvent);

  if (mEventHistory.size() > kMaxEventHistoryLength) {
    int nExtra = mEventHistory.size() - kMaxEventHistoryLength;
    mEventHistory.erase(mEventHistory.begin(), mEventHistory.begin() + nExtra);

    LOG4CPLUS_TRACE(sLogger, "Removed the first " << nExtra << " items from the action history to keep a max length of " << kMaxEventHistoryLength);
  }
}


namespace {
void updateMetrics(log4cplus::Logger& aLogger, swatch::core::MonitorableObject& aObj)
{
  LOG4CPLUS_INFO(aLogger, "Updating monitoring data for component '" << aObj.getPath() << "'");
  const auto lStart = std::chrono::steady_clock::now();

  // Create one metric update guard to be used for all descendant monitorable objects, to avoid the actionable
  // ... object's status continually flip-flopping between "updating metrics" & "not uptdating metrics"
  swatch::core::MetricUpdateGuard lUpdateGuard(aObj);

  // Crawl through all non-disabled monitorable objects in hierarchy, and call update method of their child metrics
  std::string lDisabledObjPath;
  for (swatch::core::Object::iterator it = aObj.begin(); it != aObj.end(); it++) {
    if (swatch::core::MonitorableObject* lMonSubObj = dynamic_cast<swatch::core::MonitorableObject*>(&*it)) {
      // Skip this sub-object if under a disabled object
      if ((!lDisabledObjPath.empty()) && (lMonSubObj->getPath().find(lDisabledObjPath) == size_t(0)))
        continue;
      else if (lMonSubObj->getMonitoringStatus() == swatch::core::monitoring::kDisabled) {
        lDisabledObjPath = lMonSubObj->getPath();
        continue;
      }
      else {
        LOG4CPLUS_DEBUG(aLogger, "Updating monitoring data for '" << lMonSubObj->getPath() << "'");
        lMonSubObj->updateMetrics(lUpdateGuard);
      }
    }
  }

  const std::chrono::duration<float, std::milli> lDurationInMs(std::chrono::steady_clock::now() - lStart);
  LOG4CPLUS_INFO(aLogger, "Monitoring data for component '" << aObj.getPath() << "' updated. Took " << lDurationInMs.count() << "ms");
}
}


Server::ResponseSpec Server::executeUpdateMonitoringData(const std::vector<std::string>& aPathMatches, const Json::Value&)
{
  if (aPathMatches.size() == 0) {
    updateMetrics(sLogger, getBoard().getServiceModule());

    for (const auto& deviceId : getBoard().getProcessors())
      updateMetrics(sLogger, getBoard().getProcessor(deviceId));

    for (const auto& opticsId : getBoard().getOpticalModules())
      updateMetrics(sLogger, getBoard().getOpticalModule(opticsId));
  }
  else if (aPathMatches.size() == 1) {
    if (mBoard.getActionables().count(aPathMatches.at(0)) > 0)
      updateMetrics(sLogger, getBoard().getActionable(aPathMatches.at(0)));
    else if (mBoard.getOpticalModules().count(aPathMatches.at(0)) > 0)
      updateMetrics(sLogger, getBoard().getOpticalModule(aPathMatches.at(0)));
    else
      return getErrorResponse(404, "InvalidRequest", "Specified device/module ('" + aPathMatches.at(0) + "') does not exist");
  }
  else
    return getErrorResponse(404, "InvalidRequest", "Invalid URL path");

  return Server::ResponseSpec();
}


boost::optional<Server::ResponseSpec> Server::checkLease(const std::string& aLeaseToken, const std::lock_guard<std::mutex>& aGuard)
{
  std::ostringstream lExpiredMessage;

  if (not mLease)
    return getErrorResponse(409, "IncorrectLeaseToken", "No application currently holds the lease.");

  if (mLease->hasExpired()) {
    if (mLease->id == aLeaseToken)
      return getErrorResponse(409, "LeaseExpired", "No application currently holds the lease. You obtained the last lease, but that expired " + std::to_string(-mLease->remainingTime()) + " seconds ago.");
    else
      return getErrorResponse(409, "IncorrectLeaseToken", "No application currently holds the lease. Another application obtained the last lease (which expired " + std::to_string(-mLease->remainingTime()) + "s ago).");
  }

  if (mLease->id != aLeaseToken)
    return getErrorResponse(409, "IncorrectLeaseToken", "Current lease (held by '" + mLease->supervisor + "', expires in " + std::to_string(mLease->remainingTime()) + " seconds) does not match supplied token");

  return boost::none;
}


void writeTruncatedJson(std::ostream& aStream, const Json::Value& aValue, size_t aIndent)
{
  switch (aValue.type()) {
    case Json::nullValue:
      aStream << "null";
      break;

    case Json::intValue:
      aStream << aValue.asInt();
      break;

    case Json::uintValue:
      aStream << aValue.asUInt();
      break;

    case Json::realValue:
      aStream << aValue.asDouble();
      break;

    case Json::stringValue: {
      const std::string lValue(aValue.asString());
      if (lValue.size() > 50)
        aStream << "\"" << lValue.substr(0, 50) << "\" (truncated)";
      else
        aStream << "\"" << lValue << "\"";
      break;
    }
    case Json::booleanValue:
      aStream << (aValue.asBool() ? "true" : "false");
      break;

    case Json::arrayValue:
      if (aValue.empty()) {
        aStream << "[]";
        break;
      }

      aStream << "[" << std::endl
              << std::string(aIndent + 2, ' ');

      if (not aValue.empty())
        writeTruncatedJson(aStream, aValue[0], aIndent + 2);
      for (size_t i = 1; i < aValue.size(); i++) {
        aStream << "," << std::endl
                << std::string(aIndent + 2, ' ');
        writeTruncatedJson(aStream, aValue[int(i)], aIndent + 2);
      }

      aStream << std::endl
              << std::string(aIndent, ' ') << "]";
      break;

    case Json::objectValue:
      if (aValue.empty()) {
        aStream << "{}";
        break;
      }
      aStream << "{" << std::endl
              << std::string(aIndent + 2, ' ');

      const auto lMembers = aValue.getMemberNames();
      for (size_t i = 0; not lMembers.empty(); i++) {
        aStream << "\"" << lMembers.at(i) << "\": ";
        writeTruncatedJson(aStream, aValue.get(lMembers.at(i), Json::Value()), aIndent + 2);

        if (i == (lMembers.size() - 1))
          break;

        aStream << "," << std::endl
                << std::string(aIndent + 2, ' ');
      }

      aStream << std::endl
              << std::string(aIndent, ' ') << "}";
      break;
  }
}


} // namespace app
} // namespace herd
