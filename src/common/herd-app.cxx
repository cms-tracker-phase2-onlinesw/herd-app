
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <regex>

#include <log4cplus/loggingmacros.h>
#include <log4cplus/version.h>
#if LOG4CPLUS_VERSION >= 2000000
#include <log4cplus/initializer.h>
#endif

#include <boost/filesystem/path.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional/optional.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>

#include <yaml-cpp/yaml.h>

#include "swatch/action/Command.hpp"
#include "swatch/action/StateMachine.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/Device.hpp"
#include "swatch/phase2/DeviceStub.hpp"
#include "swatch/phase2/PackageInfo.hpp"
#include "swatch/phase2/PackageRegistry.hpp"

#include "herd/app/Config.hpp"
#include "herd/app/Server.hpp"
#include "herd/app/utilities.hpp"


using namespace swatch;
using namespace swatch::action;
using namespace swatch::core;
namespace bpo = boost::program_options;


enum TestMode {
  kParseConfig,
  kLoadLibraries,
  kConstructDevices,
  kStartServer
};

struct Settings {
  uint16_t port;
  boost::optional<TestMode> testMode;
  bool keepGoing;
  bool catchExceptions;
  std::string configFilePath;
  boost::optional<std::chrono::seconds> inactivityTimeout;
  bool logMessageData;
  boost::optional<log4cplus::LogLevel> logLevel;
};


Settings parseArgs(int argc, char* argv[]);
int mainCore(const Settings&);

void logPackageInfo(const swatch::phase2::PackageInfo& aPackage);
void printPackageInfo(const swatch::phase2::PackageInfo& aPackage);


herd::app::Server* serverPtr = NULL;
void postActionCallback(const swatch::action::Functionoid& aFunctionoid)
{
  if (serverPtr != NULL)
    serverPtr->postActionCallback(aFunctionoid);
  else
    LOG4CPLUS_ERROR(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("")), "Cannot run server class' post-action callback since serverPtr is NULL");
}


int main(int argc, char* argv[])
{
  const Settings settings = parseArgs(argc, argv);

  // Initialise log4cplus (and ensure that appropriate cleanup is done when main exits)
#if LOG4CPLUS_VERSION >= 2000000
  log4cplus::Initializer logInitializer;
#endif
  if (settings.logLevel != boost::none) {
    std::cout << "Setting log level of root log4cplus logger" << std::endl;
    log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("")).setLogLevel(*settings.logLevel);
  }

  if (settings.catchExceptions) {
    try {
      return mainCore(settings);
    }
    catch (const std::exception& e) {
      std::cout << "ERROR";
      if (typeid(e) != typeid(std::runtime_error))
        std::cout << " ('" << demangleName(typeid(e).name()) << "' caught)";
      std::cout << ": " << e.what() << std::endl;
      return 1;
    }
  }
  else
    return mainCore(settings);
}


const std::map<std::string, log4cplus::LogLevel> kLogLevelMap = {
  { "trace", log4cplus::TRACE_LOG_LEVEL },
  { "debug", log4cplus::DEBUG_LOG_LEVEL },
  { "info", log4cplus::INFO_LOG_LEVEL },
  { "warn", log4cplus::WARN_LOG_LEVEL },
  { "error", log4cplus::ERROR_LOG_LEVEL },
  { "fatal", log4cplus::FATAL_LOG_LEVEL },
  { "TRACE", log4cplus::TRACE_LOG_LEVEL },
  { "DEBUG", log4cplus::DEBUG_LOG_LEVEL },
  { "INFO", log4cplus::INFO_LOG_LEVEL },
  { "WARN", log4cplus::WARN_LOG_LEVEL },
  { "ERROR", log4cplus::ERROR_LOG_LEVEL },
  { "FATAL", log4cplus::FATAL_LOG_LEVEL }
};


Settings parseArgs(int argc, char* argv[])
{
  Settings settings;
  std::string testMode;
  std::string inactivityTimeoutStr;

  bpo::options_description options("Allowed options");
  // clang-format off
  options.add_options()
    ("help,h", "produce help message")
    ("version", "print version numbers")
    ("port,p", bpo::value<uint16_t>(&settings.port)->default_value(3000))
    ("test,t", bpo::value<std::string>(&testMode), "Run in test mode (i.e. exit before connecting to remote app). 'lint': parse config files. 'plugin': load plugin libraries. 'devices': create devices.")
    ("do-not-catch-exceptions", "Do not catch exceptions when starting up server and creating required objects beforehand (useful e.g. when running in GDB)")
    ("keep-going,k", "Keep going if startup tests fail")
    ("inactivity-timeout", bpo::value<std::string>(&inactivityTimeoutStr), "Stop if no HTTP requests received after specified period. Format: XhYmZs (or subset thereof)")
    ("log-level", bpo::value<std::string>(), "Override log level for log4plus root logger")
    ("log-http-data", "Include data from HTTP requests & responses in logs");

  bpo::options_description hiddenOptions;
  hiddenOptions.add_options()
    ("config-file", bpo::value<std::string>(&settings.configFilePath));
  // clang-format on

  bpo::options_description allOptions;
  allOptions.add(options);
  allOptions.add(hiddenOptions);

  bpo::positional_options_description args;
  args.add("config-file", 1);

  bpo::variables_map varMap;
  try {
    bpo::store(bpo::command_line_parser(argc, argv).options(allOptions).positional(args).run(), varMap);
    bpo::notify(varMap);
  }
  catch (const boost::program_options::error& e) {
    std::cout << "ERROR: " << e.what() << std::endl;
    std::exit(1);
  }

  if (varMap.count("help")) {
    std::cout << "usage: " << argv[0] << " [OPTIONS] config-file" << std::endl
              << std::endl;
    std::cout << options << std::endl;
    std::exit(0);
  }

  if (varMap.count("version")) {
    for (auto lIt = swatch::phase2::PackageRegistry::begin(); lIt != swatch::phase2::PackageRegistry::end(); lIt++)
      printPackageInfo(lIt->second);
    std::exit(0);
  }

  if (varMap.count("config-file") == 0) {
    std::cout << "ERROR: Config file path not specified!" << std::endl;
    std::exit(1);
  }

  if (varMap.count("test")) {
    if (testMode == "lint")
      settings.testMode = kParseConfig;
    else if (testMode == "plugin")
      settings.testMode = kLoadLibraries;
    else if (testMode == "devices")
      settings.testMode = kConstructDevices;
    else if (testMode == "server")
      settings.testMode = kStartServer;
    else {
      std::cout << "ERROR: 'test' option has invalid value '" << testMode << "'. Possible values are:" << std::endl;
      std::cout << "         - lint:    Just parse config file" << std::endl;
      std::cout << "         - plugin:  Parse config file and load plugin libraries" << std::endl;
      std::cout << "         - devices: Parse config, load plugins and create device instances" << std::endl;
      std::cout << "         - server:  All of the above, then start server and check for good response from all GET endpoints" << std::endl;
      std::exit(1);
    }
  }

  settings.keepGoing = (varMap.count("keep-going") > 0);
  settings.catchExceptions = not(varMap.count("do-not-catch-exceptions") > 0);
  settings.logMessageData = (varMap.count("log-http-data") > 0);

  if (varMap.count("inactivity-timeout")) {
    const std::regex lTimeoutRegex("(?:([0-9]+)h)?(?:([0-9]+)m)?(?:([0-9]+)s)?");
    std::smatch lResults;

    if (std::regex_match(inactivityTimeoutStr, lResults, lTimeoutRegex) and (lResults.size() == 4)) {
      settings.inactivityTimeout = std::chrono::seconds(0);

      if (lResults[1].matched)
        (*settings.inactivityTimeout) += std::chrono::seconds(60 * 60 * std::stoul(lResults[1].str()));
      if (lResults[2].matched)
        (*settings.inactivityTimeout) += std::chrono::seconds(60 * std::stoul(lResults[2].str()));
      if (lResults[3].matched)
        (*settings.inactivityTimeout) += std::chrono::seconds(std::stoul(lResults[3].str()));
    }
    else {
      std::cout << "ERROR: --inactivity-timeout value is invalid. Format should be XhYmZs (or subset thereof)." << std::endl;
      std::exit(1);
    }
  }

  if (varMap.count("log-level")) {
    const auto logLevelIt = kLogLevelMap.find(varMap["log-level"].as<std::string>());
    if (logLevelIt != kLogLevelMap.end())
      settings.logLevel = logLevelIt->second;
    else {
      std::cout << "ERROR: Invalid log level ('" << varMap["log-level"].as<std::string>() << "') specified." << std::endl;
      std::cout << "       Valid values are:";
      for (const auto& x : kLogLevelMap)
        std::cout << " " << x.first;
      std::cout << std::endl;
      std::exit(1);
    }
  }

  if (settings.keepGoing and settings.testMode) {
    std::cout << "ERROR: Cannot use '--keep-going' flag if running in test mode!" << std::endl;
    std::exit(1);
  }

  return settings;
}


int mainCore(const Settings& aSettings)
{
  std::cout << "Parsing config file '" << aSettings.configFilePath << "'" << std::endl;
  herd::app::Config cfg;
  try {
    cfg = herd::app::parseConfigFile(aSettings.configFilePath);
  }
  // User-friendly translation for 'YAML: BadFile' exception
  catch (const YAML::BadFile& e) {
    throw std::runtime_error("Error reading config file (" + std::string(e.what()) + ")");
  }
  if ((aSettings.testMode != boost::none) and (*aSettings.testMode == kParseConfig)) {
    std::cout << "Success! Exiting early as running in test mode." << std::endl;
    return 0;
  }

  std::cout << "Loading libraries" << std::endl;
  herd::app::loadLibraries(cfg.libraries, boost::filesystem::path(aSettings.configFilePath).parent_path());
  for (auto lIt = swatch::phase2::PackageRegistry::begin(); lIt != swatch::phase2::PackageRegistry::end(); lIt++)
    logPackageInfo(lIt->second);
  // TODO: Check that cfg.hardwareType has been registered (to protect against typos)
  if (not Factory::get()->contains(cfg.serviceModule.creator))
    throw std::runtime_error("Service module class '" + cfg.serviceModule.creator + "' specified in config file has not been registered");
  for (const auto& x : cfg.processors) {
    if (not Factory::get()->contains(x.creator))
      throw std::runtime_error("Processor class '" + x.creator + "' specified in config file has not been registered");
  }
  if ((aSettings.testMode != boost::none) and (*aSettings.testMode == kLoadLibraries)) {
    std::cout << "Success! Exiting early as running in test mode." << std::endl;
    return 0;
  }

  std::cout << std::endl;
  std::cout << "Constructing board & device objects" << std::endl;
  phase2::Board board(cfg.hardwareType, cfg.serviceModule, cfg.processors, cfg.opticalFibreConnectors, postActionCallback);

  if ((aSettings.testMode != boost::none) and (*aSettings.testMode == kConstructDevices)) {
    std::cout << "Success! Exiting early as running in test mode." << std::endl;
    return 0;
  }

  using herd::app::Server;
  Server server(board, aSettings.port, aSettings.inactivityTimeout, aSettings.logMessageData);
  serverPtr = &server;

  Server::RunMode runMode = Server::RunMode::kNormal;
  if (aSettings.testMode and (*aSettings.testMode == TestMode::kStartServer))
    runMode = Server::RunMode::kStopAfterStartUpTests;
  else if (aSettings.keepGoing)
    runMode = Server::RunMode::kIgnoreStartUpTestFailures;
  return server.run(runMode);
}


void logPackageInfo(const swatch::phase2::PackageInfo& aPackage)
{
  using swatch::phase2::PackageInfo;
  std::ostringstream lMessage;

  // 1) Build info
  if (const PackageInfo::LocalBuild* lBuildInfo = boost::get<const PackageInfo::LocalBuild>(&aPackage.build)) {
    const std::time_t lBuildTime = std::chrono::system_clock::to_time_t(lBuildInfo->time);
    lMessage << "Built manually at ";
    lMessage << std::put_time(std::localtime(&lBuildTime), "%F %T %Z");
  }
  else if (const PackageInfo::GitLabBuild* lBuildInfo = boost::get<const PackageInfo::GitLabBuild>(&aPackage.build)) {
    const std::time_t lBuildTime = std::chrono::system_clock::to_time_t(lBuildInfo->time);
    lMessage << "Built at ";
    lMessage << std::put_time(std::localtime(&lBuildTime), "%F %T %Z");
    lMessage << " in GitLab CI pipeline " << lBuildInfo->pipelineID << " (job " << lBuildInfo->jobID << ")";
  }

  // 2) VCS info
  if (aPackage.vcs) {
    lMessage << ", from ";
    if (aPackage.vcs->ref)
      lMessage << (aPackage.vcs->ref->first == PackageInfo::Git::kBranch ? "branch \"" : "tag \"") << aPackage.vcs->ref->second << "\" (";
    lMessage << "commit " << aPackage.vcs->sha.substr(0, 8);
    if (aPackage.vcs->ref)
      lMessage << ")";

    if (not aPackage.vcs->clean)
      lMessage << " with local changes";
  }
  else
    lMessage << " [no VCS info]";

  LOG4CPLUS_INFO(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("")), "Package " << aPackage.name << ": " << lMessage.str());
}


void printPackageInfo(const swatch::phase2::PackageInfo& aPackage)
{
  using swatch::phase2::PackageInfo;

  std::cout << aPackage.name << std::endl;
  std::cout << "  Version: ";
  std::cout << aPackage.version.major << "." << aPackage.version.minor << "." << aPackage.version.patch << std::endl;
  if (aPackage.vcs) {
    if (not aPackage.vcs->ref)
      std::cout << "  Commit " << aPackage.vcs->sha.substr(0, 8);
    else if (aPackage.vcs->ref->first == PackageInfo::Git::kBranch)
      std::cout << "  Branch " << aPackage.vcs->ref->second << ", commit " << aPackage.vcs->sha.substr(0, 8);
    else
      std::cout << "  Tag " << aPackage.vcs->ref->second;

    if (not aPackage.vcs->clean)
      std::cout << " + local changes" << std::endl;
    else
      std::cout << std::endl;
  }

  if (const PackageInfo::LocalBuild* lBuildInfo = boost::get<const PackageInfo::LocalBuild>(&aPackage.build)) {
    const std::time_t lBuildTime = std::chrono::system_clock::to_time_t(lBuildInfo->time);
    std::cout << "  Built manually, at " << std::put_time(std::localtime(&lBuildTime), "%F %T %Z") << std::endl;
  }
  else if (const PackageInfo::GitLabBuild* lBuildInfo = boost::get<const PackageInfo::GitLabBuild>(&aPackage.build)) {
    const std::time_t lBuildTime = std::chrono::system_clock::to_time_t(lBuildInfo->time);
    std::cout << "  Built by GitLab CI, at " << std::put_time(std::localtime(&lBuildTime), "%F %T %Z") << std::endl;
    const std::string lProjectBaseURL(lBuildInfo->serverURL + "/" + lBuildInfo->projectPath);
    std::cout << "    Pipeline URL: " << lProjectBaseURL << "/-/pipelines/" << lBuildInfo->pipelineID << std::endl;
    std::cout << "    Job URL:      " << lProjectBaseURL << "/-/jobs/" << lBuildInfo->jobID << std::endl;
  }
}
