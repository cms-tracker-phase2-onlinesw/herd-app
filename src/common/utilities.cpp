
#include "herd/app/utilities.hpp"


#include <dlfcn.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <typeindex>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <json/value.h>

#include "swatch/action/Command.hpp"
#include "swatch/action/File.hpp"
#include "swatch/action/Property.hpp"
#include "swatch/action/PropertySnapshot.hpp"
#include "swatch/action/StateMachine.hpp"
#include "swatch/action/Table.hpp"
#include "swatch/core/ErrorInfo.hpp"
#include "swatch/core/MetricSnapshot.hpp"
#include "swatch/core/MonitorableObject.hpp"
#include "swatch/core/ParameterSet.hpp"
#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/GreaterThan.hpp"
#include "swatch/core/rules/InRange.hpp"
#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/LesserThan.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/rules/None.hpp"
#include "swatch/core/rules/OfSize.hpp"
#include "swatch/core/rules/OutOfRange.hpp"
#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/Device.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OpticalFibreConnector.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/Processor.hpp"
#include "swatch/phase2/ServiceModule.hpp"

#include "herd/app/Remnants.hpp"


using namespace swatch;


namespace herd {
namespace app {


const std::map<std::type_index, std::string> kTypeNameMap = {
  { std::type_index(typeid(bool)), "bool" },
  { std::type_index(typeid(uint16_t)), "uint16" },
  { std::type_index(typeid(uint32_t)), "uint32" },
  { std::type_index(typeid(uint64_t)), "uint64" },
  { std::type_index(typeid(int32_t)), "int32" },
  { std::type_index(typeid(int64_t)), "int64" },
  { std::type_index(typeid(float)), "float" },
  { std::type_index(typeid(double)), "double" },
  { std::type_index(typeid(std::string)), "string" },
  { std::type_index(typeid(action::File)), "file" },
  { std::type_index(typeid(std::vector<bool>)), "vector:bool" },
  { std::type_index(typeid(std::vector<uint16_t>)), "vector:uint16" },
  { std::type_index(typeid(std::vector<uint32_t>)), "vector:uint32" },
  { std::type_index(typeid(std::vector<uint64_t>)), "vector:uint64" },
  { std::type_index(typeid(std::vector<int32_t>)), "vector:int32" },
  { std::type_index(typeid(std::vector<int64_t>)), "vector:int64" },
  { std::type_index(typeid(std::vector<float>)), "vector:float" },
  { std::type_index(typeid(std::vector<double>)), "vector:double" },
  { std::type_index(typeid(std::vector<std::string>)), "vector:string" },
  { std::type_index(typeid(std::vector<action::File>)), "vector:file" },
  { std::type_index(typeid(action::Table)), "table" },
};


namespace fields {
const Json::StaticString kPropertyType("type");
const Json::StaticString kPropertyGroup("group");
const Json::StaticString kPropertyDescription("description");
const Json::StaticString kPropertyValue("value");
const Json::StaticString kPropertyUpdateTime("updateTime");
const Json::StaticString kPropertyUpdaterIfDelegated("updaterIfDelegated");
const Json::StaticString kPropertyUpdateDuration("propertyUpdateDuration");
const Json::StaticString kPropertyUpdateError("propertyUpdateError");

const Json::StaticString kPropertyUnit("unit");
const Json::StaticString kPropertyFormat("format");
const Json::StaticString kPropertyFloatNotationFix("fix");
const Json::StaticString kPropertyFloatNotationSci("sci");
const Json::StaticString kPropertyBaseBin("bin");
const Json::StaticString kPropertyBaseOct("oct");
const Json::StaticString kPropertyBaseHex("hex");
const Json::StaticString kPropertyBaseDec("dec");

const Json::StaticString kPortIndex("index");
const Json::StaticString kPortPath("path");
const Json::StaticString kPortIsMasked("isMasked");
const Json::StaticString kPortIsInLoopback("isInLoopback");
const Json::StaticString kPortIsPresent("isPresent");
const Json::StaticString kPortOperatingMode("operatingMode");
const Json::StaticString kPortOperatingModeCSP("CSP");
const Json::StaticString kPortOperatingModeCSPIdle1("idle1");
const Json::StaticString kPortOperatingModeCSPIdle2("idle2");
const Json::StaticString kPortConnectedObject("connectedObject");
const Json::StaticString kPortConnectedObjectProc("proc");
const Json::StaticString kPortConnectedObjectOpto("opto");

const Json::StaticString kOpticsInputs("inputs");
const Json::StaticString kOpticsOutputs("outputs");
const Json::StaticString kOpticsChannelId("id");
const Json::StaticString kOpticsChannelIndex("index");
const Json::StaticString kOpticsConnectedPort("connectedPort");
const Json::StaticString kOpticsConnectedFibre("connectedFibre");
const Json::StaticString kOpticsIsPresent("isPresent");

const Json::StaticString kMonObjAlias("alias");
const Json::StaticString kMonObjSetting("setting");
const Json::StaticString kMonObjStatus("status");
const Json::StaticString kMonObjMonitorables("monitorables");
const Json::StaticString kMonObjMetrics("metrics");
const Json::StaticString kMonObjUpdateDuration("updateDuration");
const Json::StaticString kMonObjUpdateError("updateError");

const Json::StaticString kMonEnabled("enabled");
const Json::StaticString kMonNonCritical("non-critical");
const Json::StaticString kMonDisabled("disabled");

const Json::StaticString kMonGood("good");
const Json::StaticString kMonWarning("warning");
const Json::StaticString kMonError("error");
const Json::StaticString kMonNoLimit("no limit");
const Json::StaticString kMonUnknown("unknown");

const Json::StaticString kMetricAlias("alias");
const Json::StaticString kMetricType("type");
const Json::StaticString kMetricValue("value");
const Json::StaticString kMetricUpdateTime("updateTime");
const Json::StaticString kMetricUpdaterIfDelegated("updaterIfDelegated");
const Json::StaticString kMetricSetting("setting");
const Json::StaticString kMetricStatus("status");
const Json::StaticString kMetricErrorCondition("errorCondition");
const Json::StaticString kMetricWarnCondition("warningCondition");
const Json::StaticString kMetricUnit("unit");
const Json::StaticString kMetricFormat("format");
const Json::StaticString kMetricFloatNotationFix("fix");
const Json::StaticString kMetricFloatNotationSci("sci");
const Json::StaticString kMetricBaseBin("bin");
const Json::StaticString kMetricBaseOct("oct");
const Json::StaticString kMetricBaseHex("hex");
const Json::StaticString kMetricBaseDec("dec");
}


std::string encodeToBase64(const std::string& aString)
{
  using namespace boost::archive::iterators;
  using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;

  std::string lResult(It(aString.begin()), It(aString.end()));
  lResult.append((3 - aString.size() % 3) % 3, '=');
  return lResult;
}


std::string decodeFromBase64(const std::string& aSource)
{
  using namespace boost::archive::iterators;
  using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

  // Count padding character '=' to remove them later
  size_t lLength = aSource.size();
  size_t lPaddingCount = 0;
  while ((lPaddingCount < lLength) and (aSource.at(lLength - lPaddingCount - 1) == '='))
    lPaddingCount++;

  std::string lResult(It(aSource.begin()), It(aSource.end() - lPaddingCount));
  return lResult;
}


template <typename T>
Json::Value convertPropertyValue(const action::PropertySnapshot& aSnapshot)
{
  try {
    return Json::Value(aSnapshot.getValue<T>());
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <>
Json::Value convertPropertyValue<uint64_t>(const action::PropertySnapshot& aSnapshot)
{
  try {
    return Json::Value(Json::Value::UInt64(aSnapshot.getValue<uint64_t>()));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(uint64_t).name()) + "'");
  }
}


template <>
Json::Value convertPropertyValue<int64_t>(const action::PropertySnapshot& aSnapshot)
{
  try {
    return Json::Value(Json::Value::Int64(aSnapshot.getValue<int64_t>()));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(int64_t).name()) + "'");
  }
}


template <typename T>
Json::Value encodeTableCell(const action::Table& aTable, size_t aColumn, size_t aRow)
{
  try {
    return Json::Value(aTable.at<T>(aColumn, aRow));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting table cell to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <>
Json::Value encodeTableCell<uint64_t>(const action::Table& aTable, size_t aColumn, size_t aRow)
{
  try {
    return Json::Value(Json::Value::Int64(aTable.at<uint64_t>(aColumn, aRow)));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting table cell to type '" + core::demangleName(typeid(uint64_t).name()) + "'");
  }
}


template <>
Json::Value encodeTableCell<int64_t>(const action::Table& aTable, size_t aColumn, size_t aRow)
{
  try {
    return Json::Value(Json::Value::UInt64(aTable.at<int64_t>(aColumn, aRow)));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting table cell to type '" + core::demangleName(typeid(int64_t).name()) + "'");
  }
}


template <>
Json::Value convertPropertyValue<action::Table>(const action::PropertySnapshot& aSnapshot)
{
  typedef std::function<Json::Value(const action::Table&, size_t, size_t)> TableCellConverter;
  static const std::map<std::type_index, TableCellConverter> kTableCellConverters {
    { std::type_index(typeid(bool)), encodeTableCell<bool> },
    { std::type_index(typeid(uint16_t)), encodeTableCell<uint16_t> },
    { std::type_index(typeid(uint32_t)), encodeTableCell<uint32_t> },
    { std::type_index(typeid(uint64_t)), encodeTableCell<uint64_t> },
    { std::type_index(typeid(int)), encodeTableCell<int> },
    { std::type_index(typeid(int64_t)), encodeTableCell<int64_t> },
    { std::type_index(typeid(float)), encodeTableCell<float> },
    { std::type_index(typeid(double)), encodeTableCell<double> },
    { std::type_index(typeid(std::string)), encodeTableCell<std::string> }
  };

  const action::Table& lTable = aSnapshot.getValue<action::Table>();

  Json::Value lResult;
  lResult["columns"] = Json::Value(Json::arrayValue);
  lResult["data"] = Json::Value(Json::arrayValue);
  for (size_t i = 0; i < lTable.getNumberOfRows(); i++)
    lResult["data"].append(Json::Value(Json::arrayValue));

  for (size_t i = 0; i < lTable.getColumns().size(); i++) {
    Json::Value lColumn(Json::objectValue);
    lColumn["name"] = lTable.getColumns().at(i).name;
    lColumn["type"] = kTypeNameMap.at(*lTable.getColumns().at(i).type);
    lResult["columns"].append(lColumn);

    for (size_t j = 0; j < lTable.getNumberOfRows(); j++)
      lResult["data"][Json::ArrayIndex(j)].append(kTableCellConverters.at(*lTable.getColumns().at(i).type)(lTable, i, j));
  }

  return lResult;
}


template <typename T>
void addFloatingPointPropertyAttributes(Json::Value& aJsonNode, const action::AbstractProperty& aProperty)
{
  if (const auto lProperty = dynamic_cast<const action::Property<T>*>(&aProperty)) {
    aJsonNode[fields::kPropertyUnit] = lProperty->getUnit().empty() ? Json::Value() : Json::Value(lProperty->getUnit());
    aJsonNode[fields::kPropertyFormat] = Json::Value(Json::arrayValue);
    aJsonNode[fields::kPropertyFormat].append(lProperty->getNotation() == core::format::kFixedPoint ? fields::kPropertyFloatNotationFix : fields::kPropertyFloatNotationSci);
    if (lProperty->getPrecision())
      aJsonNode[fields::kPropertyFormat].append(Json::Value::UInt(*lProperty->getPrecision()));
  }
  else
    throw std::runtime_error("Could not convert property '" + aProperty.getName() + "' to type '" + core::demangleName(typeid(action::Property<T>).name()) + "'");
}


template <typename T>
void addIntegralPropertyAttributes(Json::Value& aJsonNode, const action::AbstractProperty& aProperty)
{
  if (const auto lProperty = dynamic_cast<const action::Property<T>*>(&aProperty)) {
    aJsonNode[fields::kPropertyUnit] = lProperty->getUnit().empty() ? Json::Value() : Json::Value(lProperty->getUnit());
    aJsonNode[fields::kPropertyFormat] = Json::Value(Json::arrayValue);
    switch (lProperty->getBase()) {
      case core::format::kBinary:
        aJsonNode[fields::kPropertyFormat].append(fields::kPropertyBaseBin);
        break;
      case core::format::kOctal:
        aJsonNode[fields::kPropertyFormat].append(fields::kPropertyBaseOct);
        break;
      case core::format::kHeximal:
        aJsonNode[fields::kPropertyFormat].append(fields::kPropertyBaseHex);
        break;
      case core::format::kDecimal:
        aJsonNode[fields::kPropertyFormat].append(fields::kPropertyBaseDec);
        break;
    }
    if (lProperty->getWidth()) {
      aJsonNode[fields::kPropertyFormat].append(Json::Value::UInt(*lProperty->getWidth()));
      aJsonNode[fields::kPropertyFormat].append(lProperty->getFillChar());
    }
  }
  else
    throw std::runtime_error("Could not convert property '" + aProperty.getName() + "' to type '" + core::demangleName(typeid(action::Property<T>).name()) + "'");
}


void encodeProperties(Json::Value& aJsonNode, const swatch::action::PropertyHolder& aObj)
{
  typedef std::function<Json::Value(const action::PropertySnapshot&)> PropertyValueConverter;
  static const std::map<std::type_index, PropertyValueConverter> kPropertyValueConverters {
    { std::type_index(typeid(bool)), convertPropertyValue<bool> },
    { std::type_index(typeid(uint16_t)), convertPropertyValue<uint16_t> },
    { std::type_index(typeid(uint32_t)), convertPropertyValue<uint32_t> },
    { std::type_index(typeid(uint64_t)), convertPropertyValue<uint64_t> },
    { std::type_index(typeid(int)), convertPropertyValue<int> },
    { std::type_index(typeid(int64_t)), convertPropertyValue<int64_t> },
    { std::type_index(typeid(float)), convertPropertyValue<float> },
    { std::type_index(typeid(double)), convertPropertyValue<double> },
    { std::type_index(typeid(std::string)), convertPropertyValue<std::string> },
    { std::type_index(typeid(action::Table)), convertPropertyValue<action::Table> }
  };

  typedef std::function<void(Json::Value&, const action::AbstractProperty&)> PropertyAttributeEncoder;
  static const std::map<std::type_index, PropertyAttributeEncoder> kPropertyAttributeEncoders {
    { std::type_index(typeid(uint16_t)), addIntegralPropertyAttributes<uint16_t> },
    { std::type_index(typeid(uint32_t)), addIntegralPropertyAttributes<uint32_t> },
    { std::type_index(typeid(uint64_t)), addIntegralPropertyAttributes<uint64_t> },
    { std::type_index(typeid(int)), addIntegralPropertyAttributes<int> },
    { std::type_index(typeid(int64_t)), addIntegralPropertyAttributes<int64_t> },
    { std::type_index(typeid(float)), addFloatingPointPropertyAttributes<float> },
    { std::type_index(typeid(double)), addFloatingPointPropertyAttributes<double> }
  };

  Json::Value& propertyMap = aJsonNode["properties"];
  propertyMap = Json::Value(Json::objectValue);
  for (const auto& x : aObj.getProperties()) {
    const auto snapshot = x.second->getSnapshot();

    Json::Value& propertyData = propertyMap[x.first];
    propertyData = Json::Value(Json::objectValue);
    propertyData[fields::kPropertyType] = kTypeNameMap.at(snapshot.getType());
    propertyData[fields::kPropertyGroup] = snapshot.getGroup();
    propertyData[fields::kPropertyDescription] = snapshot.getDescription();

    if (snapshot.isValueKnown())
      propertyData[fields::kPropertyValue] = kPropertyValueConverters.at(snapshot.getType())(snapshot);
    else
      propertyData[fields::kPropertyValue] = Json::Value();

    propertyData[fields::kPropertyUpdateTime] = std::chrono::duration<double>(snapshot.getUpdateTimestamp().system.time_since_epoch()).count();
    propertyData[fields::kPropertyUpdaterIfDelegated] = Json::Value();

    const auto it = kPropertyAttributeEncoders.find(snapshot.getType());
    if (it != kPropertyAttributeEncoders.end())
      it->second(propertyData, *x.second);
  }

  aJsonNode[fields::kPropertyUpdateDuration] = Json::Value(std::chrono::duration<float>(aObj.getPropertyUpdateDuration()).count());
  aJsonNode[fields::kPropertyUpdateError] = encodeErrorInfo(aObj.getPropertyUpdateErrorInfo());
}


void encodeDeviceInfo(Json::Value& aJsonNode, const phase2::Device& aDevice)
{
  aJsonNode["role"] = aDevice.getStub().role;
  aJsonNode["type"] = aDevice.getStub().creator;
  aJsonNode["uri"] = aDevice.getStub().uri;
  aJsonNode["addressTable"] = aDevice.getStub().addressTable;

  auto status = aDevice.getStatus();
  if (status.isEngaged()) {
    aJsonNode["fsm"] = status.getStateMachineId();
    aJsonNode["state"] = status.getState();
  }
  else {
    aJsonNode["fsm"] = Json::Value();
    aJsonNode["state"] = Json::Value();
  }

  aJsonNode["runningActions"] = Json::Value(Json::arrayValue);
  for (const auto& action : status.getRunningActions())
    aJsonNode["runningActions"].append(action->getId());

  aJsonNode["isPresent"] = aDevice.isPresent();
  encodeProperties(aJsonNode, aDevice);
}


const std::map<phase2::PRBSMode, std::string> kPRBSModeNames { { phase2::kPRBS7, "PRBS7" }, { phase2::kPRBS9, "PRBS9" }, { phase2::kPRBS15, "PRBS15" }, { phase2::kPRBS23, "PRBS23" }, { phase2::kPRBS31, "PRBS31" } };

template <typename T>
void encodePortInfo(Json::Value& aJsonNode, const phase2::Board& aBoard, const T& aPort)
{
  aJsonNode[fields::kPortIndex] = Json::Value::UInt64(aPort.getIndex());
  aJsonNode[fields::kPortPath] = aPort.getPath();
  aJsonNode[fields::kPortIsMasked] = aPort.isMasked();
  aJsonNode[fields::kPortIsInLoopback] = aPort.isInLoopback();
  aJsonNode[fields::kPortIsPresent] = aPort.isPresent();
  encodeProperties(aJsonNode, aPort);

  Json::Value& modeInfo = aJsonNode[fields::kPortOperatingMode];
  modeInfo = Json::Value(Json::arrayValue);
  if (aPort.getOperatingMode() == phase2::kPRBS)
    modeInfo.append(Json::Value(kPRBSModeNames.at(aPort.getPRBSMode())));
  else {
    modeInfo.append(fields::kPortOperatingModeCSP);
    modeInfo.append(Json::Value::UInt(aPort.getCSPSettings().packetLength));
    modeInfo.append(Json::Value::UInt(aPort.getCSPSettings().packetPeriodicity));
    modeInfo.append(aPort.getCSPSettings().idleMethod == phase2::CSPSettings::kIdleMethod1 ? fields::kPortOperatingModeCSPIdle1 : fields::kPortOperatingModeCSPIdle2);
  }

  Json::Value& connectionInfo = aJsonNode[fields::kPortConnectedObject];
  if (aPort.getConnection()) {
    connectionInfo = Json::Value(Json::arrayValue);
    if (aBoard.getProcessors().count(aPort.getConnection()->component) > 0)
      connectionInfo.append(fields::kPortConnectedObjectProc);
    else
      connectionInfo.append(fields::kPortConnectedObjectOpto);
    connectionInfo.append(aPort.getConnection()->component);
    connectionInfo.append(Json::Value::UInt(aPort.getConnection()->index));
  }
}


std::shared_ptr<Json::Value> encodeDeviceInfo(const phase2::Board& aBoard)
{
  auto data = std::make_shared<Json::Value>(Json::objectValue);

  const auto& srv = aBoard.getServiceModule();
  encodeDeviceInfo((*data)["serviceModule"], srv);
  (*data)["serviceModule"]["id"] = srv.getId();

  (*data)["processors"] = Json::Value(Json::objectValue);

  for (auto id : aBoard.getProcessors()) {
    const phase2::Processor& processor = aBoard.getProcessor(id);

    encodeDeviceInfo((*data)["processors"][id], processor);

    // Add information about input ports
    Json::Value& inputInfo = (*data)["processors"][id]["inputs"];
    inputInfo = Json::Value(Json::objectValue);
    for (const auto* port : processor.getInputPorts().getPorts())
      encodePortInfo(inputInfo[port->getId()], aBoard, *port);

    // Add information about output ports
    Json::Value& outputInfo = (*data)["processors"][id]["outputs"];
    outputInfo = Json::Value(Json::objectValue);
    for (const auto* port : processor.getOutputPorts().getPorts())
      encodePortInfo(outputInfo[port->getId()], aBoard, *port);

    // Add CSP index offset (for dual-FPGA boards)
    (*data)["processors"][id]["cspIndexOffset"] = Json::Value::UInt64(processor.getCSPIndexOffset());

    // Add board-local TCDS2 source
    if (processor.getTCDS2Source())
      (*data)["processors"][id]["tcds2Source"] = Json::Value(*processor.getTCDS2Source());
    else
      (*data)["processors"][id]["tcds2Source"] = Json::Value(Json::nullValue);
  }

  encodeOpticsInfo((*data)["optics"], aBoard);

  encodeFrontPanelInfo((*data)["fibreConnectors"], aBoard);

  return data;
}


void encodeOpticsInfo(Json::Value& aJsonNode, const phase2::Board& aBoard)
{
  aJsonNode = Json::Value(Json::objectValue);

  for (auto moduleId : aBoard.getOpticalModules()) {
    const phase2::OpticalModule& optoModule = aBoard.getOpticalModule(moduleId);

    // 1) Encode info about inputs
    aJsonNode[moduleId][fields::kOpticsInputs] = Json::Value(Json::arrayValue);
    for (const auto* channel : optoModule.getInputs()) {
      Json::Value& channelInfo = aJsonNode[moduleId][fields::kOpticsInputs].append(Json::Value(Json::objectValue));
      channelInfo[fields::kOpticsChannelId] = channel->getId();
      channelInfo[fields::kOpticsChannelIndex] = Json::Value::UInt64(channel->getIndex());
      encodeProperties(channelInfo, *channel);

      Json::Value& connPortInfo = channelInfo[fields::kOpticsConnectedPort];
      if (const phase2::InputPort* connectedPort = aBoard.getConnectedInputPort(*channel)) {
        const std::string path = connectedPort->getPath();
        connPortInfo = Json::Value(Json::arrayValue);
        connPortInfo.append(path.substr(0, path.find('.')));
        connPortInfo.append(Json::Value::UInt64(connectedPort->getIndex()));
      }

      const auto lFibreConnector = aBoard.getConnectedFrontPanelChannel(*channel);
      Json::Value& connFibreInfo = channelInfo[fields::kOpticsConnectedFibre];
      if (lFibreConnector.first != NULL) {
        connFibreInfo = Json::Value(Json::arrayValue);
        connFibreInfo.append(lFibreConnector.first->getId());
        connFibreInfo.append(Json::Value::UInt(lFibreConnector.second));
      }
    }

    // 2) Encode info about outputs
    aJsonNode[moduleId][fields::kOpticsOutputs] = Json::Value(Json::arrayValue);
    for (const auto* channel : optoModule.getOutputs()) {
      Json::Value& channelInfo = aJsonNode[moduleId][fields::kOpticsOutputs].append(Json::Value(Json::objectValue));
      channelInfo[fields::kOpticsChannelId] = channel->getId();
      channelInfo[fields::kOpticsChannelIndex] = Json::Value::UInt64(channel->getIndex());
      encodeProperties(channelInfo, *channel);

      Json::Value& connPortInfo = channelInfo[fields::kOpticsConnectedPort];
      if (const phase2::OutputPort* connectedPort = aBoard.getConnectedOutputPort(*channel)) {
        const std::string path = connectedPort->getPath();
        connPortInfo = Json::Value(Json::arrayValue);
        connPortInfo.append(path.substr(0, path.find('.')));
        connPortInfo.append(Json::Value::UInt64(connectedPort->getIndex()));
      }

      const auto lFibreConnector = aBoard.getConnectedFrontPanelChannel(*channel);
      Json::Value& connFibreInfo = channelInfo[fields::kOpticsConnectedFibre];
      if (lFibreConnector.first != NULL) {
        connFibreInfo = Json::Value(Json::arrayValue);
        connFibreInfo.append(lFibreConnector.first->getId());
        connFibreInfo.append(Json::Value::UInt(lFibreConnector.second));
      }
    }

    // 3) Encode 'isPresent' flag
    aJsonNode[moduleId][fields::kOpticsIsPresent] = optoModule.isPresent();

    // 4) Encode properties
    encodeProperties(aJsonNode[moduleId], optoModule);
  }
}


void encodeFrontPanelInfo(Json::Value& aJsonNode, const swatch::phase2::Board& aBoard)
{
  aJsonNode = Json::Value(Json::objectValue);

  for (auto fibreConnectorId : aBoard.getFrontPanelOptoConnectors()) {
    const phase2::OpticalFibreConnector* connector = aBoard.getFrontPanelOptoConnector(fibreConnectorId);

    if (connector == NULL)
      aJsonNode[fibreConnectorId] = Json::Value();
    else {
      aJsonNode[fibreConnectorId]["id"] = connector->getId();
      aJsonNode[fibreConnectorId]["type"] = connector->getType();

      Json::Value channelData(Json::objectValue);
      for (const auto i : connector->getChannels()) {
        const std::string key = std::to_string(i);
        const auto* connectedOpticsChannel = aBoard.getConnectedOpticalChannel(*connector, i);
        if (connectedOpticsChannel == NULL)
          channelData[key] = Json::Value();
        else {
          channelData[key] = Json::Value(Json::arrayValue);
          const std::string opticalChannelPath = connectedOpticsChannel->getPath();
          channelData[key].append(opticalChannelPath.substr(0, opticalChannelPath.find(".")));
          if (connectedOpticsChannel->getDirection() == phase2::OpticalModuleChannel::kRx) {
            channelData[key].append("rx");
            channelData[key].append(Json::Value::UInt(connectedOpticsChannel->getIndex()));
          }
          else {
            channelData[key].append("tx");
            channelData[key].append(Json::Value::UInt(connectedOpticsChannel->getIndex()));
          }
        }
      }

      aJsonNode[fibreConnectorId]["channels"] = channelData;
    }
  }
}


template <typename T>
inline Json::Value convertNativeTypeToJson(const T x)
{
  return Json::Value(x);
}

template <>
inline Json::Value convertNativeTypeToJson<uint64_t>(const uint64_t x)
{
  // Explicit conversion to Json::UInt64 required to avoid 'ambiguous conversion' errors on CentOS7
  return Json::Value(Json::Value::UInt64(x));
}

template <>
inline Json::Value convertNativeTypeToJson<int64_t>(const int64_t x)
{
  // Explicit conversion to Json::Int64 required to avoid 'ambiguous conversion' errors on CentOS7
  return Json::Value(Json::Value::Int64(x));
}


template <typename T>
Json::Value convertValue(const core::ParameterSet& aParamSet, const std::string& aId)
{
  try {
    return Json::Value(convertNativeTypeToJson<T>(aParamSet.get<T>(aId)));
  }
  catch (const boost::bad_any_cast&) {
    throw std::runtime_error("Error occurred when converting command result to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <typename T>
Json::Value convertVectorValue(const core::ParameterSet& aParamSet, const std::string& aId)
{
  try {
    Json::Value lResult(Json::arrayValue);
    for (const T& x : aParamSet.get<std::vector<T>>(aId))
      lResult.append(convertNativeTypeToJson<T>(x));
    return lResult;
  }
  catch (const boost::bad_any_cast&) {
    throw std::runtime_error("Error occurred when converting command result to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


Json::Value convertFileValue(const core::ParameterSet& aParamSet, const std::string& aId)
{
  return Json::Value();
}


Json::Value convertFileVectorValue(const core::ParameterSet& aParamSet, const std::string& aId)
{
  Json::Value lResult(Json::arrayValue);
  try {
    for (size_t i = 0; i < aParamSet.get<std::vector<action::File>>(aId).size(); i++)
      lResult.append(Json::Value());
  }
  catch (const boost::bad_any_cast&) {
    throw std::runtime_error("Error occurred when converting command result to type 'std::vector<action::File>'");
  }
  return lResult;
}


std::shared_ptr<Json::Value> encodeCommandInfo(const swatch::phase2::Board& aBoard)
{
  auto result = std::make_shared<Json::Value>(Json::objectValue);

  for (const auto& lId : aBoard.getActionables())
    encodeCommandInfo((*result)[lId], aBoard.getActionable(lId));

  return result;
}


void encodeCommandInfo(Json::Value& aJsonNode, const action::ActionableObject& aDevice)
{
  aJsonNode = Json::Value(Json::objectValue);

  typedef std::function<Json::Value(const core::ParameterSet&, const std::string&)> ValueEncoder;
  static const std::map<std::type_index, ValueEncoder> kValueEncoderMap = {
    { std::type_index(typeid(bool)), convertValue<bool> },
    { std::type_index(typeid(uint32_t)), convertValue<uint32_t> },
    { std::type_index(typeid(uint64_t)), convertValue<uint64_t> },
    { std::type_index(typeid(int32_t)), convertValue<int32_t> },
    { std::type_index(typeid(int64_t)), convertValue<int64_t> },
    { std::type_index(typeid(float)), convertValue<float> },
    { std::type_index(typeid(double)), convertValue<double> },
    { std::type_index(typeid(swatch::action::File)), convertFileValue },
    { std::type_index(typeid(std::string)), convertValue<std::string> },
    { std::type_index(typeid(std::vector<bool>)), convertVectorValue<bool> },
    { std::type_index(typeid(std::vector<uint32_t>)), convertVectorValue<uint32_t> },
    { std::type_index(typeid(std::vector<uint64_t>)), convertVectorValue<uint64_t> },
    { std::type_index(typeid(std::vector<int32_t>)), convertVectorValue<int32_t> },
    { std::type_index(typeid(std::vector<int64_t>)), convertVectorValue<int64_t> },
    { std::type_index(typeid(std::vector<float>)), convertVectorValue<float> },
    { std::type_index(typeid(std::vector<double>)), convertVectorValue<double> },
    { std::type_index(typeid(std::vector<std::string>)), convertVectorValue<std::string> },
    { std::type_index(typeid(std::vector<swatch::action::File>)), convertFileVectorValue },
  };

  for (auto commandId : aDevice.getCommands()) {
    const action::Command& cmd = aDevice.getCommand(commandId);

    Json::Value& commandInfo = aJsonNode[cmd.getId()];
    commandInfo = Json::Value(Json::objectValue);
    commandInfo["alias"] = cmd.getAlias();
    commandInfo["description"] = cmd.getDescription();

    Json::Value& parameterInfo = commandInfo["parameters"];
    parameterInfo = Json::Value(Json::objectValue);
    for (auto paramId : cmd.getDefaultParams().keys()) {
      const auto& type = cmd.getDefaultParams().getType(paramId);
      const std::string typeName = kTypeNameMap.at(std::type_index(type));
      parameterInfo[paramId] = Json::Value(Json::objectValue);
      parameterInfo[paramId]["type"] = typeName;

      auto paramSpec = cmd.getParameters().at(paramId);
      parameterInfo[paramId]["description"] = paramSpec.description;
      parameterInfo[paramId]["rule"] = encodeParameterRule(paramSpec.rule);

      const auto encoderIt = kValueEncoderMap.find(std::type_index(type));
      if (encoderIt != kValueEncoderMap.end())
        parameterInfo[paramId]["defaultValue"] = encoderIt->second(cmd.getDefaultParams(), paramId);
      else
        throw std::runtime_error("Cannot encode default value for parameter '" + paramId + "' (type '" + core::demangleName(type.name()) + "')");
    }

    Json::Value& constraintInfo = commandInfo["constraints"];
    constraintInfo = Json::Value(Json::objectValue);
    for (auto x : cmd.getConstraints()) {
      constraintInfo[x.first][1] = boost::lexical_cast<std::string>(*x.second);
      for (auto parameter : x.second->getParameterNames())
        constraintInfo[x.first][0].append(parameter);
    }
  }
}


namespace rules {

void encodeFiniteNumber(const core::AbstractRule&, Json::Value& x)
{
  x.append("FiniteNumber");
}

void encodeFiniteVector(const core::AbstractRule&, Json::Value& x)
{
  x.append("FiniteVector");
}

template <typename T>
void encodeGreaterThan(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::GreaterThan<T>&>(aRule);
  x.append("GreaterThan");
  x.append(convertNativeTypeToJson<T>(lRule.getLowerBound()));
}

template <typename T>
void encodeInRange(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::InRange<T>&>(aRule);
  x.append("InRange");
  x.append(convertNativeTypeToJson<T>(lRule.getRange().at(0)));
  x.append(convertNativeTypeToJson<T>(lRule.getRange().at(1)));
}

template <typename T>
void encodeIsAmong(const core::AbstractRule& aRule, Json::Value& aValue)
{
  const auto& lRule = dynamic_cast<const core::rules::IsAmong<T>&>(aRule);
  aValue.append("IsAmong");
  for (const T& x : lRule.getChoices())
    aValue.append(convertNativeTypeToJson<T>(x));
}

template <typename T>
void encodeLesserThan(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::LesserThan<T>&>(aRule);
  x.append("LesserThan");
  x.append(convertNativeTypeToJson<T>(lRule.getUpperBound()));
}

void encodeNone(const core::AbstractRule&, Json::Value& x)
{
  x = Json::Value();
}


template <typename T>
void encodeOfSize(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::OfSize<std::vector<T>>&>(aRule);
  x.append("OfSize");
  x.append(Json::UInt(lRule.getSize()));
}

template <typename T>
void encodeOutOfRange(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::OutOfRange<T>&>(aRule);
  x.append("OutOfRange");
  x.append(convertNativeTypeToJson<T>(lRule.getRange().at(0)));
  x.append(convertNativeTypeToJson<T>(lRule.getRange().at(1)));
}

template <typename T>
void encodeAll(const core::AbstractRule& aRule, Json::Value& x)
{
  const auto& lRule = dynamic_cast<const core::rules::All<T>&>(aRule);
  x.append("All");
  x.append(encodeParameterRule(lRule.getElementRule()));
}

}


Json::Value encodeParameterRule(const core::AbstractRule& aRule)
{
  using namespace swatch::core::rules;
  typedef std::function<void(const core::AbstractRule&, Json::Value&)> RuleEncoder;
  static const std::map<std::type_index, RuleEncoder> kRuleEncoderMap = {
    { std::type_index(typeid(FiniteNumber<uint32_t>)), rules::encodeFiniteNumber },
    { std::type_index(typeid(FiniteNumber<uint64_t>)), rules::encodeFiniteNumber },
    { std::type_index(typeid(FiniteNumber<int32_t>)), rules::encodeFiniteNumber },
    { std::type_index(typeid(FiniteNumber<int64_t>)), rules::encodeFiniteNumber },
    { std::type_index(typeid(FiniteNumber<float>)), rules::encodeFiniteNumber },
    { std::type_index(typeid(FiniteNumber<double>)), rules::encodeFiniteNumber },

    { std::type_index(typeid(FiniteVector<std::vector<uint32_t>>)), rules::encodeFiniteVector },
    { std::type_index(typeid(FiniteVector<std::vector<uint64_t>>)), rules::encodeFiniteVector },
    { std::type_index(typeid(FiniteVector<std::vector<int32_t>>)), rules::encodeFiniteVector },
    { std::type_index(typeid(FiniteVector<std::vector<int64_t>>)), rules::encodeFiniteVector },
    { std::type_index(typeid(FiniteVector<std::vector<float>>)), rules::encodeFiniteVector },
    { std::type_index(typeid(FiniteVector<std::vector<double>>)), rules::encodeFiniteVector },

    { std::type_index(typeid(GreaterThan<uint32_t>)), rules::encodeGreaterThan<uint32_t> },
    { std::type_index(typeid(GreaterThan<uint64_t>)), rules::encodeGreaterThan<uint64_t> },
    { std::type_index(typeid(GreaterThan<int32_t>)), rules::encodeGreaterThan<int32_t> },
    { std::type_index(typeid(GreaterThan<int64_t>)), rules::encodeGreaterThan<int64_t> },
    { std::type_index(typeid(GreaterThan<int>)), rules::encodeGreaterThan<int> },
    { std::type_index(typeid(GreaterThan<float>)), rules::encodeGreaterThan<float> },

    { std::type_index(typeid(InRange<uint32_t>)), rules::encodeInRange<uint32_t> },
    { std::type_index(typeid(InRange<uint64_t>)), rules::encodeInRange<uint64_t> },
    { std::type_index(typeid(InRange<int32_t>)), rules::encodeInRange<int32_t> },
    { std::type_index(typeid(InRange<int64_t>)), rules::encodeInRange<int64_t> },
    { std::type_index(typeid(InRange<float>)), rules::encodeInRange<float> },
    { std::type_index(typeid(InRange<double>)), rules::encodeInRange<double> },

    { std::type_index(typeid(IsAmong<uint32_t>)), rules::encodeIsAmong<uint32_t> },
    { std::type_index(typeid(IsAmong<uint64_t>)), rules::encodeIsAmong<uint64_t> },
    { std::type_index(typeid(IsAmong<int32_t>)), rules::encodeIsAmong<int32_t> },
    { std::type_index(typeid(IsAmong<int64_t>)), rules::encodeIsAmong<int64_t> },
    { std::type_index(typeid(IsAmong<float>)), rules::encodeIsAmong<float> },
    { std::type_index(typeid(IsAmong<double>)), rules::encodeIsAmong<double> },
    { std::type_index(typeid(IsAmong<std::string>)), rules::encodeIsAmong<std::string> },

    { std::type_index(typeid(LesserThan<uint32_t>)), rules::encodeLesserThan<uint32_t> },
    { std::type_index(typeid(LesserThan<uint64_t>)), rules::encodeLesserThan<uint64_t> },
    { std::type_index(typeid(LesserThan<int32_t>)), rules::encodeLesserThan<int32_t> },
    { std::type_index(typeid(LesserThan<int64_t>)), rules::encodeLesserThan<int64_t> },
    { std::type_index(typeid(LesserThan<float>)), rules::encodeLesserThan<float> },
    { std::type_index(typeid(LesserThan<double>)), rules::encodeLesserThan<double> },

    { std::type_index(typeid(NonEmptyString<std::string>)), [](const core::AbstractRule&, Json::Value& x) { x.append("NonEmptyString"); } },

    { std::type_index(typeid(None<bool>)), rules::encodeNone },
    { std::type_index(typeid(None<uint32_t>)), rules::encodeNone },
    { std::type_index(typeid(None<uint64_t>)), rules::encodeNone },
    { std::type_index(typeid(None<int32_t>)), rules::encodeNone },
    { std::type_index(typeid(None<int64_t>)), rules::encodeNone },
    { std::type_index(typeid(None<float>)), rules::encodeNone },
    { std::type_index(typeid(None<double>)), rules::encodeNone },
    { std::type_index(typeid(None<std::string>)), rules::encodeNone },
    { std::type_index(typeid(None<action::File>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<bool>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<uint32_t>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<uint64_t>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<int32_t>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<int64_t>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<float>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<double>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<std::string>>)), rules::encodeNone },
    { std::type_index(typeid(None<std::vector<action::File>>)), rules::encodeNone },

    { std::type_index(typeid(OfSize<std::vector<bool>>)), rules::encodeOfSize<bool> },
    { std::type_index(typeid(OfSize<std::vector<uint32_t>>)), rules::encodeOfSize<uint32_t> },
    { std::type_index(typeid(OfSize<std::vector<uint64_t>>)), rules::encodeOfSize<uint64_t> },
    { std::type_index(typeid(OfSize<std::vector<int32_t>>)), rules::encodeOfSize<int32_t> },
    { std::type_index(typeid(OfSize<std::vector<int64_t>>)), rules::encodeOfSize<int64_t> },
    { std::type_index(typeid(OfSize<std::vector<float>>)), rules::encodeOfSize<float> },
    { std::type_index(typeid(OfSize<std::vector<double>>)), rules::encodeOfSize<double> },
    { std::type_index(typeid(OfSize<std::vector<std::string>>)), rules::encodeOfSize<std::string> },
    { std::type_index(typeid(OfSize<std::vector<action::File>>)), rules::encodeOfSize<action::File> },

    { std::type_index(typeid(OutOfRange<uint32_t>)), rules::encodeOutOfRange<uint32_t> },
    { std::type_index(typeid(OutOfRange<uint64_t>)), rules::encodeOutOfRange<uint64_t> },
    { std::type_index(typeid(OutOfRange<int32_t>)), rules::encodeOutOfRange<int32_t> },
    { std::type_index(typeid(OutOfRange<int64_t>)), rules::encodeOutOfRange<int64_t> },
    { std::type_index(typeid(OutOfRange<float>)), rules::encodeOutOfRange<float> },
    { std::type_index(typeid(OutOfRange<double>)), rules::encodeOutOfRange<double> },

    { std::type_index(typeid(All<bool>)), rules::encodeAll<bool> },
    { std::type_index(typeid(All<uint32_t>)), rules::encodeAll<uint32_t> },
    { std::type_index(typeid(All<uint64_t>)), rules::encodeAll<uint64_t> },
    { std::type_index(typeid(All<int32_t>)), rules::encodeAll<int32_t> },
    { std::type_index(typeid(All<int64_t>)), rules::encodeAll<int64_t> },
    { std::type_index(typeid(All<float>)), rules::encodeAll<float> },
    { std::type_index(typeid(All<double>)), rules::encodeAll<double> },
    { std::type_index(typeid(All<std::string>)), rules::encodeAll<std::string> }
  };

  Json::Value lResult(Json::arrayValue);
  lResult.append(boost::lexical_cast<std::string>(aRule));

  const auto lEncoderIt = kRuleEncoderMap.find(std::type_index(typeid(aRule)));
  if (lEncoderIt != kRuleEncoderMap.end())
    lEncoderIt->second(aRule, lResult);

  return lResult;
}


Json::Value encodeParameterRule(const std::shared_ptr<const core::AbstractRule>& aRule)
{
  if (not aRule)
    return Json::Value();
  else
    return encodeParameterRule(*aRule);
}


Json::Value encodeFsmInfo(const action::ActionableObject& aDevice)
{
  Json::Value data(Json::objectValue);

  for (auto fsmId : aDevice.getStateMachines()) {
    const action::StateMachine& fsm = aDevice.getStateMachine(fsmId);
    data[fsmId] = Json::Value(Json::objectValue);
    data[fsmId]["alias"] = fsm.getAlias();
    data[fsmId]["description"] = "";
    data[fsmId]["initialState"] = fsm.getInitialState();
    data[fsmId]["errorState"] = fsm.getErrorState();
    data[fsmId]["states"] = Json::Value(Json::objectValue);

    for (auto stateId : fsm.getStates()) {
      data[fsmId]["states"][stateId] = Json::Value(Json::objectValue);
      for (auto transition : fsm.getTransitions(stateId)) {
        const auto& t = *transition.second;
        data[fsmId]["states"][stateId][transition.first] = Json::Value(Json::objectValue);
        data[fsmId]["states"][stateId][transition.first]["alias"] = t.getAlias();
        data[fsmId]["states"][stateId][transition.first]["description"] = t.getDescription();
        data[fsmId]["states"][stateId][transition.first]["endState"] = t.getEndState();
        data[fsmId]["states"][stateId][transition.first]["commands"] = Json::Value(Json::arrayValue);

        for (auto stepIt = t.begin(); stepIt != t.end(); stepIt++) {
          const int i = stepIt - t.begin();
          data[fsmId]["states"][stateId][transition.first]["commands"][i][0] = stepIt->get().getId();
          data[fsmId]["states"][stateId][transition.first]["commands"][i][1] = stepIt->getNamespace();
        }
      }
    }
  }

  return data;
}


std::shared_ptr<Json::Value> encodeLeaseInfo(const boost::optional<Server::Lease>& aLease)
{
  auto result = std::make_shared<Json::Value>(Json::objectValue);
  (*result)["supervisor"] = Json::Value();
  (*result)["duration"] = Json::Value();

  if (aLease and (not aLease->hasExpired())) {
    const float remainingTime(aLease->remainingTime());
    if (remainingTime > 0.0) {
      (*result)["supervisor"] = aLease->supervisor;
      (*result)["duration"] = remainingTime;
    }
  }

  return result;
}


std::shared_ptr<Json::Value> encodeLeaseToken(const Server::Lease& aLease)
{
  auto result = std::make_shared<Json::Value>(Json::objectValue);
  (*result)["lease"] = aLease.id;
  (*result)["duration"] = aLease.remainingTime();

  return result;
}


template <typename T>
Json::Value convertCommandResult(const boost::any& aValue)
{
  try {
    return Json::Value(boost::any_cast<const T&>(aValue));
  }
  catch (const boost::bad_any_cast&) {
    throw std::runtime_error("Error occurred when converting command result to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <typename T>
Json::Value convertVectorCommandResult(const boost::any& aValue)
{
  try {
    Json::Value lResult(Json::arrayValue);
    for (const T& x : boost::any_cast<const std::vector<T>&>(aValue))
      lResult.append(x);
    return lResult;
  }
  catch (const boost::bad_any_cast&) {
    throw std::runtime_error("Error occurred when converting command result to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <>
Json::Value convertCommandResult<void>(const boost::any& aValue)
{
  return Json::Value();
}


template <>
Json::Value convertCommandResult<swatch::action::File>(const boost::any& aValue)
{
  const swatch::action::File& lFile = boost::any_cast<const swatch::action::File&>(aValue);

  Json::Value lResult(Json::objectValue);
  lResult["name"] = lFile.getName();
  lResult["type"] = lFile.getContentType();
  lResult["format"] = lFile.getContentFormat();

  std::ifstream lStream(lFile.getPath().c_str(), std::ios::binary);
  if (not lStream.is_open())
    throw std::runtime_error("File '" + lFile.getPath() + "' does not exist");

  std::streampos lBegin = lStream.tellg();
  lStream.seekg(0, std::ios::end);
  std::streampos lEnd = lStream.tellg();
  size_t lSize = lEnd - lBegin;

  std::vector<char> lContents(lSize, 0);
  lStream.seekg(0, std::ios::beg);
  lStream.read(&lContents.front(), lSize);
  lStream.close();

  const std::string lEncodedData = encodeToBase64(std::string(lContents.begin(), lContents.end()));
  lResult["contents"] = lEncodedData;

  return lResult;
}


std::shared_ptr<Json::Value> encodeCommandSnapshot(const action::CommandSnapshot& aSnapshot)
{
  auto infoMap = std::make_shared<Json::Value>();

  (*infoMap)["path"] = aSnapshot.getActionPath();
  (*infoMap)["state"] = boost::lexical_cast<std::string>(aSnapshot.getState());
  (*infoMap)["startTime"] = std::chrono::duration<double>(aSnapshot.getStartTime().time_since_epoch()).count();
  (*infoMap)["duration"] = aSnapshot.getRunningTime();
  (*infoMap)["progress"] = aSnapshot.getProgress();
  (*infoMap)["messages"] = Json::Value(Json::arrayValue);
  for (const auto& x : aSnapshot.getMessages()) {
    Json::Value entry(Json::arrayValue);
    entry.append(Json::Value(x.first));
    entry.append(Json::Value(x.second));
    (*infoMap)["messages"].append(entry);
  }

  typedef std::function<Json::Value(const boost::any&)> ResultWriter;
  static const std::map<std::type_index, ResultWriter> kResultWriterMap = {
    { std::type_index(typeid(void)), convertCommandResult<void> },
    { std::type_index(typeid(bool)), convertCommandResult<bool> },
    { std::type_index(typeid(unsigned int)), convertCommandResult<unsigned int> },
    { std::type_index(typeid(int)), convertCommandResult<int> },
    { std::type_index(typeid(float)), convertCommandResult<float> },
    { std::type_index(typeid(std::string)), convertCommandResult<std::string> },
    { std::type_index(typeid(std::vector<bool>)), convertVectorCommandResult<bool> },
    { std::type_index(typeid(std::vector<unsigned int>)), convertVectorCommandResult<unsigned int> },
    { std::type_index(typeid(std::vector<int>)), convertVectorCommandResult<int> },
    { std::type_index(typeid(std::vector<float>)), convertVectorCommandResult<float> },
    { std::type_index(typeid(std::vector<std::string>)), convertVectorCommandResult<std::string> },
    { std::type_index(typeid(swatch::action::File)), convertCommandResult<swatch::action::File> },
  };

  const std::type_info& type = aSnapshot.getResult().type();

  const auto writerIt = kResultWriterMap.find(std::type_index(type));
  if (writerIt != kResultWriterMap.end())
    (*infoMap)["result"] = writerIt->second(aSnapshot.getResult());
  else
    throw std::runtime_error("Cannot extract result of type '" + core::demangleName(type.name()) + "'");

  (*infoMap)["errorDetails"] = encodeErrorInfo(aSnapshot.getErrorInfo());

  return infoMap;
}


std::shared_ptr<Json::Value> encodeTransitionSnapshot(const action::TransitionSnapshot& aSnapshot)
{
  auto data = std::make_shared<Json::Value>();

  const auto deviceId = aSnapshot.getActionableId();

  (*data)["path"] = aSnapshot.getActionPath();
  (*data)["state"] = boost::lexical_cast<std::string>(aSnapshot.getState());
  (*data)["startTime"] = std::chrono::duration<double>(aSnapshot.getStartTime().time_since_epoch()).count();
  (*data)["duration"] = aSnapshot.getRunningTime();
  (*data)["progress"] = aSnapshot.getProgress();
  (*data)["numCommandsDone"] = unsigned(aSnapshot.getNumberOfCompletedCommands());
  (*data)["numCommandsTotal"] = unsigned(aSnapshot.getTotalNumberOfCommands());

  (*data)["commands"] = Json::Value(Json::arrayValue);
  for (const auto& cmdSnapshot : aSnapshot)
    (*data)["commands"].append(*encodeCommandSnapshot(cmdSnapshot));

  return data;
}


Json::Value encodeErrorInfo(const std::shared_ptr<const swatch::core::ErrorInfo>& aErrorInfo)
{
  if (not aErrorInfo)
    return Json::Value(Json::nullValue);

  Json::Value lResult(Json::objectValue);
  lResult["type"] = core::demangleName(aErrorInfo->type.name());
  lResult["message"] = aErrorInfo->message;

  if (not aErrorInfo->location)
    lResult["location"] = Json::Value(Json::nullValue);
  else {
    lResult["location"] = Json::Value(Json::objectValue);
    lResult["location"]["function"] = aErrorInfo->location->function;
    lResult["location"]["file"] = aErrorInfo->location->file;
    lResult["location"]["line"] = unsigned(aErrorInfo->location->line);
  }

  lResult["nestedError"] = encodeErrorInfo(aErrorInfo->innerError);

  return lResult;
}


void encodeMonitorableObject(Json::Value& aJsonNode, const swatch::core::MonitorableObject& aMonObj, const boost::optional<size_t>& aDepth)
{
  aJsonNode = Json::Value(Json::objectValue);

  aJsonNode[fields::kMonObjAlias] = aMonObj.getAlias();
  // data["path"] = aMonObj.getPath();
  switch (aMonObj.getMonitoringStatus()) {
    case core::monitoring::kEnabled:
      aJsonNode[fields::kMonObjSetting] = fields::kMonEnabled;
      break;
    case core::monitoring::kNonCritical:
      aJsonNode[fields::kMonObjSetting] = fields::kMonNonCritical;
      break;
    case core::monitoring::kDisabled:
      aJsonNode[fields::kMonObjSetting] = fields::kMonDisabled;
      break;
  }

  switch (aMonObj.getStatusFlag()) {
    case core::kGood:
      aJsonNode[fields::kMonObjStatus] = fields::kMonGood;
      break;
    case core::kWarning:
      aJsonNode[fields::kMonObjStatus] = fields::kMonWarning;
      break;
    case core::kError:
      aJsonNode[fields::kMonObjStatus] = fields::kMonError;
      break;
    case core::kNoLimit:
      aJsonNode[fields::kMonObjStatus] = fields::kMonNoLimit;
      break;
    case core::kUnknown:
      aJsonNode[fields::kMonObjStatus] = fields::kMonUnknown;
      break;
  }

  // If depth = 0, then monitorables & metrics maps contain the same keys, but the values are 'null'
  aJsonNode[fields::kMonObjMonitorables] = Json::Value(Json::objectValue);
  aJsonNode[fields::kMonObjMetrics] = Json::Value(Json::objectValue);
  for (const auto& childId : aMonObj.getChildren()) {
    if (const core::MonitorableObject* monObjChild = aMonObj.getObjPtr<core::MonitorableObject>(childId)) {
      if (aDepth and (*aDepth == 0))
        aJsonNode[fields::kMonObjMonitorables][childId] = Json::Value();
      else if (aDepth)
        encodeMonitorableObject(aJsonNode[fields::kMonObjMonitorables][childId], *monObjChild, *aDepth - 1);
      else
        encodeMonitorableObject(aJsonNode[fields::kMonObjMonitorables][childId], *monObjChild, aDepth);
    }
    else if (const core::AbstractMetric* metricChild = aMonObj.getObjPtr<core::AbstractMetric>(childId)) {
      if (aDepth and (*aDepth == 0))
        aJsonNode[fields::kMonObjMetrics][childId] = Json::Value();
      else
        encodeMetric(aJsonNode[fields::kMonObjMetrics][childId], *metricChild);
    }
  }

  const auto lMonObjStatus = aMonObj.getStatus();
  aJsonNode[fields::kMonObjUpdateDuration] = Json::Value(std::chrono::duration<float>(lMonObjStatus.getUpdateDuration()).count());

  aJsonNode[fields::kMonObjUpdateError] = encodeErrorInfo(lMonObjStatus.getErrorInfo());
}


std::shared_ptr<Json::Value> encodeHistoryData(const std::vector<Server::HistoricalEvent>& historyData)
{
  typedef Server::HistoricalEvent Event_t;

  auto lResult = std::make_shared<Json::Value>(Json::objectValue);
  (*lResult)["data"] = Json::Value(Json::arrayValue);

  for (auto hData : historyData) {

    Json::Value lData(Json::objectValue);
    if (hData.type != Event_t::kLeaseRetire and hData.type != Event_t::kLeaseRenew and hData.type != Event_t::kLeaseObtain)
      lData["device"] = hData.deviceID;

    switch (hData.type) {
      case Event_t::kFSMEngage:
        lData["eventType"] = "fsm:engage";
        break;
      case Event_t::kFSMReset:
        lData["eventType"] = "fsm:reset";
        break;
      case Event_t::kFSMDisengage:
        lData["eventType"] = "fsm:disengage";
        break;
      case Event_t::kTransition:
        lData["eventType"] = "fsm:transition";
        break;
      case Event_t::kCommand:
        lData["eventType"] = "command";
        break;
      case Event_t::kLeaseObtain:
        lData["eventType"] = "lease:obtain";
        break;
      case Event_t::kLeaseRenew:
        lData["eventType"] = "lease:renew";
        break;
      case Event_t::kLeaseRetire:
        lData["eventType"] = "lease:retire";
        break;
    }

    if (hData.type == Event_t::kFSMEngage || hData.type == Event_t::kFSMReset || hData.type == Event_t::kFSMDisengage || hData.type == Event_t::kTransition)
      lData["fsm"] = hData.id;

    lData["time"] = std::chrono::duration<double>(hData.time.system.time_since_epoch()).count();

    if (hData.type == Event_t::kTransition || hData.type == Event_t::kCommand) {
      lData["parameters"] = hData.params;
      lData["status"] = hData.status;
    }

    if (hData.type == Event_t::kTransition) {
      lData["state"] = hData.stateID;
      lData["transition"] = hData.transID;
    }

    if (hData.type == Event_t::kCommand)
      lData["command"] = hData.id;

    if (hData.type == Event_t::kLeaseRetire || hData.type == Event_t::kLeaseRenew || hData.type == Event_t::kLeaseObtain)
      lData["lease"] = hData.lease;

    (*lResult)["data"].append(lData);
  }

  return lResult;
}


template <typename T>
Json::Value convertMetricValue(const core::MetricSnapshot& aSnapshot)
{
  try {
    return Json::Value(aSnapshot.getValue<T>());
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(T).name()) + "'");
  }
}


template <>
Json::Value convertMetricValue<uint64_t>(const core::MetricSnapshot& aSnapshot)
{
  try {
    return Json::Value(Json::Value::UInt64(aSnapshot.getValue<uint64_t>()));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(uint64_t).name()) + "'");
  }
}


template <>
Json::Value convertMetricValue<int64_t>(const core::MetricSnapshot& aSnapshot)
{
  try {
    return Json::Value(Json::Value::Int64(aSnapshot.getValue<int64_t>()));
  }
  catch (const std::exception&) {
    throw std::runtime_error("Error occurred when converting metric value to type '" + core::demangleName(typeid(int64_t).name()) + "'");
  }
}



template <typename T>
void addFloatingPointMetricAttributes(Json::Value& aJsonNode, const core::AbstractMetric& aMetric)
{
  if (const auto lMetric = dynamic_cast<const core::Metric<T>*>(&aMetric)) {
    aJsonNode[fields::kMetricUnit] = lMetric->getUnit().empty() ? Json::Value() : Json::Value(lMetric->getUnit());
    aJsonNode[fields::kMetricFormat] = Json::Value(Json::arrayValue);
    aJsonNode[fields::kMetricFormat].append(lMetric->getNotation() == core::format::kFixedPoint ? fields::kMetricFloatNotationFix : fields::kMetricFloatNotationSci);
    if (lMetric->getPrecision())
      aJsonNode[fields::kMetricFormat].append(Json::Value::UInt(*lMetric->getPrecision()));
  }
  else
    throw std::runtime_error("Could not convert property '" + aMetric.getPath() + "' to type '" + core::demangleName(typeid(action::Property<T>).name()) + "'");
}


template <typename T>
void addIntegralMetricAttributes(Json::Value& aJsonNode, const core::AbstractMetric& aMetric)
{
  if (const auto lMetric = dynamic_cast<const core::Metric<T>*>(&aMetric)) {
    aJsonNode[fields::kMetricUnit] = lMetric->getUnit().empty() ? Json::Value() : Json::Value(lMetric->getUnit());
    aJsonNode[fields::kMetricFormat] = Json::Value(Json::arrayValue);
    switch (lMetric->getBase()) {
      case core::format::kBinary:
        aJsonNode[fields::kMetricFormat].append(fields::kMetricBaseBin);
        break;
      case core::format::kOctal:
        aJsonNode[fields::kMetricFormat].append(fields::kMetricBaseOct);
        break;
      case core::format::kHeximal:
        aJsonNode[fields::kMetricFormat].append(fields::kMetricBaseHex);
        break;
      case core::format::kDecimal:
        aJsonNode[fields::kMetricFormat].append(fields::kMetricBaseDec);
        break;
    }
    if (lMetric->getWidth()) {
      aJsonNode[fields::kMetricFormat].append(Json::Value::UInt(*lMetric->getWidth()));
      aJsonNode[fields::kMetricFormat].append(lMetric->getFillChar());
    }
  }
  else
    throw std::runtime_error("Could not convert property '" + aMetric.getPath() + "' to type '" + core::demangleName(typeid(action::Property<T>).name()) + "'");
}


void encodeMetric(Json::Value& aJsonNode, const core::AbstractMetric& aMetric)
{
  typedef std::function<Json::Value(const core::MetricSnapshot&)> MetricValueConverter;
  static const std::map<std::type_index, MetricValueConverter> kMetricValueConverters {
    { std::type_index(typeid(bool)), convertMetricValue<bool> },
    { std::type_index(typeid(uint16_t)), convertMetricValue<uint16_t> },
    { std::type_index(typeid(uint32_t)), convertMetricValue<uint32_t> },
    { std::type_index(typeid(uint64_t)), convertMetricValue<uint64_t> },
    { std::type_index(typeid(int)), convertMetricValue<int> },
    { std::type_index(typeid(int64_t)), convertMetricValue<int64_t> },
    { std::type_index(typeid(float)), convertMetricValue<float> },
    { std::type_index(typeid(double)), convertMetricValue<double> },
    { std::type_index(typeid(std::string)), convertMetricValue<std::string> }
  };

  typedef std::function<void(Json::Value&, const core::AbstractMetric&)> MetricAttributeEncoder;
  static const std::map<std::type_index, MetricAttributeEncoder> kMetricAttributeEncoders {
    { std::type_index(typeid(uint16_t)), addIntegralMetricAttributes<uint16_t> },
    { std::type_index(typeid(uint32_t)), addIntegralMetricAttributes<uint32_t> },
    { std::type_index(typeid(uint64_t)), addIntegralMetricAttributes<uint64_t> },
    { std::type_index(typeid(int)), addIntegralMetricAttributes<int> },
    { std::type_index(typeid(int64_t)), addIntegralMetricAttributes<int64_t> },
    { std::type_index(typeid(float)), addFloatingPointMetricAttributes<float> },
    { std::type_index(typeid(double)), addFloatingPointMetricAttributes<double> }
  };

  const auto lSnapshot = aMetric.getSnapshot();
  aJsonNode[fields::kMetricAlias] = lSnapshot.getAlias();
  // aJsonNode["path"] = aMonObj.getPath();
  aJsonNode[fields::kMetricType] = kTypeNameMap.at(lSnapshot.getType());
  if (lSnapshot.isValueKnown())
    aJsonNode[fields::kMetricValue] = kMetricValueConverters.at(lSnapshot.getType())(lSnapshot);
  else
    aJsonNode[fields::kMetricValue] = Json::Value();

  aJsonNode[fields::kMetricUpdateTime] = std::chrono::duration<double>(lSnapshot.getUpdateTimestamp().system.time_since_epoch()).count();
  if (aMetric.getResponsibleObjIfUpdateDelegated() == boost::none)
    aJsonNode[fields::kMetricUpdaterIfDelegated] = Json::Value();
  else
    aJsonNode[fields::kMetricUpdaterIfDelegated] = *aMetric.getResponsibleObjIfUpdateDelegated();

  const auto it = kMetricAttributeEncoders.find(lSnapshot.getType());
  if (it != kMetricAttributeEncoders.end())
    it->second(aJsonNode, aMetric);

  switch (lSnapshot.getMonitoringStatus()) {
    case core::monitoring::kEnabled:
      aJsonNode[fields::kMetricSetting] = fields::kMonEnabled;
      break;
    case core::monitoring::kNonCritical:
      aJsonNode[fields::kMetricSetting] = fields::kMonNonCritical;
      break;
    case core::monitoring::kDisabled:
      aJsonNode[fields::kMetricSetting] = fields::kMonDisabled;
      break;
  }

  switch (lSnapshot.getStatusFlag()) {
    case core::kGood:
      aJsonNode[fields::kMetricStatus] = fields::kMonGood;
      break;
    case core::kWarning:
      aJsonNode[fields::kMetricStatus] = fields::kMonWarning;
      break;
    case core::kError:
      aJsonNode[fields::kMetricStatus] = fields::kMonError;
      break;
    case core::kNoLimit:
      aJsonNode[fields::kMetricStatus] = fields::kMonNoLimit;
      break;
    case core::kUnknown:
      aJsonNode[fields::kMetricStatus] = fields::kMonUnknown;
      break;
  }

  if (lSnapshot.getErrorCondition())
    aJsonNode[fields::kMetricErrorCondition] = Json::Value(boost::lexical_cast<std::string>(*lSnapshot.getErrorCondition()));
  else
    aJsonNode[fields::kMetricErrorCondition] = Json::Value();

  if (lSnapshot.getWarningCondition())
    aJsonNode[fields::kMetricWarnCondition] = Json::Value(boost::lexical_cast<std::string>(*lSnapshot.getWarningCondition()));
  else
    aJsonNode[fields::kMetricWarnCondition] = Json::Value();
}


std::shared_ptr<Json::Value> encodeMonitoringData(const swatch::phase2::Board& aBoard, const boost::optional<size_t>& aDepth)
{
  std::shared_ptr<Json::Value> data(new Json::Value());

  encodeMonitorableObject((*data)[aBoard.getServiceModule().getId()], aBoard.getServiceModule(), aDepth);

  for (const auto& id : aBoard.getProcessors())
    encodeMonitorableObject((*data)[id], aBoard.getProcessor(id), aDepth);

  for (const auto& id : aBoard.getOpticalModules())
    encodeMonitorableObject((*data)[id], aBoard.getOpticalModule(id), aDepth);

  return data;
}


template <typename T>
bool checkParameterType(const Json::Value&, std::string&);

template <>
bool checkParameterType<bool>(const Json::Value& aValue, std::string&)
{
  return aValue.isBool();
}

template <>
bool checkParameterType<uint32_t>(const Json::Value& aValue, std::string&)
{
  return aValue.isUInt();
}

template <>
bool checkParameterType<uint64_t>(const Json::Value& aValue, std::string&)
{
  return aValue.isUInt();
}

template <>
bool checkParameterType<int32_t>(const Json::Value& aValue, std::string&)
{
  return aValue.isIntegral();
}

template <>
bool checkParameterType<int64_t>(const Json::Value& aValue, std::string&)
{
  return aValue.isIntegral();
}

template <>
bool checkParameterType<float>(const Json::Value& aValue, std::string&)
{
  return aValue.isNumeric();
}

template <>
bool checkParameterType<double>(const Json::Value& aValue, std::string&)
{
  return aValue.isNumeric();
}

template <>
bool checkParameterType<std::string>(const Json::Value& aValue, std::string&)
{
  return aValue.isString();
}


template <>
bool checkParameterType<action::File>(const Json::Value& aValue, std::string& aMessage)
{
  // Internal format of JSON object
  //   1) Local file:
  //        {"path": PATH, "type": CONTENTS_TYPE, "format": CONTENTS_FORMAT}
  //   2) Transmitted file:
  //        {"name": NAME, "type": CONTENTS_TYPE, "format": CONTENTS_FORMAT, "contents": CONTENTS}

  if (not aValue.isObject())
    return false;

  if (aValue.size() == 3) {
    for (const auto& lPropertyName : std::vector<std::string> { "path", "type", "format" }) {
      if (not aValue.isMember(lPropertyName)) {
        aMessage = "'" + lPropertyName + "' child missing";
        return false;
      }
      if (not aValue[lPropertyName].isString()) {
        aMessage = "'" + lPropertyName + "' is of incorrect type (should be string)";
        return false;
      }
    }
  }
  else if (aValue.size() == 4) {
    for (const auto& lPropertyName : std::vector<std::string> { "name", "contents", "type", "format" }) {
      if (not aValue.isMember(lPropertyName)) {
        aMessage = "'" + lPropertyName + "' child missing";
        return false;
      }
      if (not aValue[lPropertyName].isString()) {
        aMessage = "'" + lPropertyName + "' is of incorrect type (should be string)";
        return false;
      }
    }
  }
  else if (aValue.size() < 3) {
    aMessage = "Too few child nodes";
    return false;
  }
  else {
    aMessage = "Too many child nodes";
    return false;
  }

  // If function has reached this far without returning, then the format is OK!
  return true;
}


template <typename T>
std::pair<T, CleanupTask> getParameter(const Json::Value&, const std::string&);

template <>
std::pair<bool, CleanupTask> getParameter<bool>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asBool(), CleanupTask());
}

template <>
std::pair<uint32_t, CleanupTask> getParameter<uint32_t>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asUInt(), CleanupTask());
}

template <>
std::pair<uint64_t, CleanupTask> getParameter<uint64_t>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asUInt64(), CleanupTask());
}

template <>
std::pair<int32_t, CleanupTask> getParameter<int32_t>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asInt(), CleanupTask());
}

template <>
std::pair<int64_t, CleanupTask> getParameter<int64_t>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asInt64(), CleanupTask());
}

template <>
std::pair<float, CleanupTask> getParameter<float>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asFloat(), CleanupTask());
}

template <>
std::pair<double, CleanupTask> getParameter<double>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asDouble(), CleanupTask());
}

template <>
std::pair<std::string, CleanupTask> getParameter<std::string>(const Json::Value& aValue, const std::string&)
{
  return std::make_pair(aValue.asString(), CleanupTask());
}


CleanupTask createDirDeleter(const boost::filesystem::path& aPath)
{
  std::function<void()> action = [aPath]() { boost::filesystem::remove_all(aPath); };
  return CleanupTask(action, "Deleting " + aPath.native());
}


template <>
std::pair<action::File, CleanupTask> getParameter<action::File>(const Json::Value& aValue, const std::string& aId)
{
  // Internal format of JSON object
  //   1) Local file:
  //        {"path": PATH, "type": CONTENTS_TYPE, "format": CONTENTS_FORMAT}
  //   2) Transmitted file:
  //        {"name": NAME, "type": CONTENTS_TYPE, "format": CONTENTS_FORMAT, "contents": CONTENTS}

  // NOTE: Can assume in this function that 'checkParameterType' has already been called

  namespace fs = boost::filesystem;

  if (aValue.size() == 3) {
    const std::string lPath(aValue["path"].asString());
    const std::string lType(aValue["type"].asString());
    const std::string lFormat(aValue["format"].asString());
    return std::make_pair(action::File(lPath, fs::path(lPath).filename().native(), lType, lFormat), CleanupTask());
  }
  else {
    // Create new temporary directory for file
    fs::path lDir(fs::temp_directory_path());
    lDir /= "herd";
    lDir /= fs::unique_path();
    fs::create_directories(lDir);
    fs::path lFilePath(lDir);
    lFilePath /= aValue["name"].asString();

    // Extract contents, and save to local file
    const std::string lContents = decodeFromBase64(aValue["contents"].asString());

    std::cout << "Creating file '" << lFilePath.native() << "' for parameter '" << aId << "'" << std::endl;
    std::fstream lFile(lFilePath.native(), std::ios::out | std::ios::binary);
    if (not lFile.is_open())
      throw std::runtime_error("Failed to open '" + lFilePath.native() + "' when saving file parameter '" + aId + "'");
    lFile.write(lContents.data(), lContents.size());
    lFile.close();

    const std::string lType(aValue["type"].asString());
    const std::string lFormat(aValue["format"].asString());
    return std::make_pair(action::File(lFilePath.native(), lType, lFormat), createDirDeleter(lDir));
  }
}


template <typename T>
std::vector<CleanupTask> extractParameter(core::ParameterSet& aSet, const std::string& aId, const Json::Value& aValue)
{
  std::string lMessage;
  if (not checkParameterType<T>(aValue, lMessage)) {
    const std::string lSuffix(lMessage.empty() ? "" : ": " + lMessage);
    throw std::runtime_error("Error occurred when extracting parameter '" + aId + "' from JSON - node has wrong type/format" + lSuffix);
  }

  const auto x = getParameter<T>(aValue, aId);
  aSet.add<T>(aId, x.first);

  std::vector<CleanupTask> lCleanupTasks;
  if (x.second)
    lCleanupTasks.push_back(x.second);
  return lCleanupTasks;
}


template <typename T>
std::vector<CleanupTask> extractVectorParameter(core::ParameterSet& aSet, const std::string& aId, const Json::Value& aValue)
{
  if (not aValue.isArray())
    throw std::runtime_error("Error occurred when extracting parameter '" + aId + "' from JSON - node is not an array");

  std::vector<T> lVector;
  std::vector<CleanupTask> lCleanupTasks;
  for (size_t i = 0; i < aValue.size(); i++) {
    const Json::Value& lChild = aValue[int(i)];
    std::string lMessage;
    if (not checkParameterType<T>(lChild, lMessage)) {
      const std::string lSuffix(lMessage.empty() ? "" : ": " + lMessage);
      throw std::runtime_error("Error occurred when extracting parameter '" + aId + "' from JSON - array child " + std::to_string(i) + " has wrong type/format" + lSuffix);
    }

    const auto x = getParameter<T>(lChild, aId);
    lVector.push_back(x.first);

    if (x.second)
      lCleanupTasks.push_back(x.second);
  }

  aSet.add<std::vector<T>>(aId, lVector);
  return lCleanupTasks;
}


std::pair<core::ParameterSet, Remnants> extractParameterSet(const action::Command& aCommand, const Json::Value& aObject)
{
  typedef std::function<std::vector<CleanupTask>(core::ParameterSet&, const std::string&, const Json::Value&)> ParameterReader;
  const std::map<std::type_index, ParameterReader> kParameterReaderMap = {
    { std::type_index(typeid(bool)), extractParameter<bool> },
    { std::type_index(typeid(uint32_t)), extractParameter<uint32_t> },
    { std::type_index(typeid(uint64_t)), extractParameter<uint64_t> },
    { std::type_index(typeid(int32_t)), extractParameter<int32_t> },
    { std::type_index(typeid(int64_t)), extractParameter<int64_t> },
    { std::type_index(typeid(float)), extractParameter<float> },
    { std::type_index(typeid(float)), extractParameter<double> },
    { std::type_index(typeid(std::string)), extractParameter<std::string> },
    { std::type_index(typeid(action::File)), extractParameter<action::File> },
    { std::type_index(typeid(std::vector<bool>)), extractVectorParameter<bool> },
    { std::type_index(typeid(std::vector<uint32_t>)), extractVectorParameter<uint32_t> },
    { std::type_index(typeid(std::vector<uint64_t>)), extractVectorParameter<uint64_t> },
    { std::type_index(typeid(std::vector<int32_t>)), extractVectorParameter<int32_t> },
    { std::type_index(typeid(std::vector<int64_t>)), extractVectorParameter<int64_t> },
    { std::type_index(typeid(std::vector<float>)), extractVectorParameter<float> },
    { std::type_index(typeid(std::vector<double>)), extractVectorParameter<double> },
    { std::type_index(typeid(std::vector<std::string>)), extractVectorParameter<std::string> },
    { std::type_index(typeid(std::vector<action::File>)), extractVectorParameter<action::File> },
  };

  const std::set<std::string> knownParameters = aCommand.getDefaultParams().keys();
  core::ParameterSet parameters;
  Remnants remnants;

  for (auto it = aObject.begin(); it != aObject.end(); it++) {
    if (knownParameters.count(it.name()) == 0)
      throw std::runtime_error("Unknown parameter '" + it.name() + "' specified for command '" + aCommand.getPath() + "'");

    const std::type_info& type = aCommand.getDefaultParams().getType(it.name());
    const auto readerIt = kParameterReaderMap.find(std::type_index(type));
    if (readerIt != kParameterReaderMap.end()) {
      const auto cleanupTasks = readerIt->second(parameters, it.name(), *it);
      for (const auto& task : cleanupTasks)
        remnants.add(aCommand.getId(), it.name(), task);
    }
    else
      throw std::runtime_error("Cannot extract parameter of type '" + core::demangleName(type.name()) + "' (id: '" + it.name() + "')");
  }

  return std::make_pair(parameters, remnants);
}


std::pair<std::vector<core::ParameterSet>, Remnants> extractParameterSets(const action::Transition& aTransition, const Json::Value& aObject)
{
  if (aObject.size() != aTransition.size())
    throw std::runtime_error("Vector of invalid length (" + boost::lexical_cast<std::string>(aObject.size()) + " != " + boost::lexical_cast<std::string>(aTransition.size()) + ") given to extractParameterSets");

  std::vector<core::ParameterSet> paramSets;
  Remnants remnants;
  for (unsigned i = 0; i < aTransition.size(); i++) {
    const auto x = extractParameterSet((aTransition.begin() + i)->get(), aObject[i]);
    paramSets.push_back(x.first);
    remnants.add(x.second);
  }

  return std::make_pair(paramSets, remnants);
}


void loadLibraries(const std::vector<std::string>& aPaths, const boost::filesystem::path& aBase)
{
  for (auto path : aPaths) {
    std::string libraryFile = path;
    if (libraryFile.find("/") != std::string::npos) {
      boost::filesystem::path filePath(path);

      if (filePath.is_relative())
        filePath = aBase / filePath;

      if ((not boost::filesystem::is_regular_file(filePath)) and (not boost::filesystem::is_symlink(filePath)))
        throw std::runtime_error("Library file path '" + filePath.native() + "' does not point to a file");

      libraryFile = filePath.native();
    }

    std::cout << "Loading library '" << libraryFile << "'" << std::endl;
    if (not dlopen(libraryFile.c_str(), RTLD_NOW | RTLD_GLOBAL | RTLD_DEEPBIND))
      throw std::runtime_error("Failed to load library '" + libraryFile + "': '" + dlerror() + "'");
  }
}


} // namespace app
} // namespace herd
