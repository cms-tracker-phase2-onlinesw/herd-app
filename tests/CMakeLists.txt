
#######################################################
# Library target: Declare - source files & dependencies
#######################################################

add_library(herd_app_tests SHARED)

target_sources(herd_app_tests
    PRIVATE
       src/common/TestUtilities.cpp
    )


target_include_directories(herd_app_tests
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
    )

target_link_libraries(herd_app_tests
    PUBLIC
        Boost::boost
    PRIVATE
        herd_app
        SWATCH::swatch_action_tests
        SWATCH::swatch_phase2_tests
        Boost::unit_test_framework
    )


#######################################################
# Runner target: Declare - source files & dependencies
#######################################################

add_executable(herd-app-test-runner src/common/herd-app-test-runner.cxx)

target_link_libraries(herd-app-test-runner
    PUBLIC
        herd_app_tests
        Boost::unit_test_framework
        log4cplus
    )


#######################################################
# Both targets: Build options
#######################################################

target_compile_features(herd_app_tests PUBLIC cxx_std_11)
target_compile_options(herd_app_tests PRIVATE -g -fPIC -O3 -Werror=return-type -Wall)

target_compile_features(herd-app-test-runner PUBLIC cxx_std_11)
target_compile_options(herd-app-test-runner PRIVATE -g -fPIC -O3 -Werror=return-type -Wall)


#######################################################
# Default rules (install, packaging, clang-format etc.)
#######################################################

include(${swatch_CMAKE_DIR}/../cactus-utilities.cmake)
cactus_add_component_rules(tests)