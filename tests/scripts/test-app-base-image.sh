#! /bin/bash

# Convenience script for basic test of herd-app-base images produced from this repository
# (Runs 'herd --help' to confirm that HERD libraries (and dependencies) loaded successfully)
# Tom Williams, July 2020

set -e


# Parse arguments
if [ $# -ne "2" ]
then
  echo "ERROR: Invalid usage!"
  echo "usage: $0 IMAGE BUILD_PLATFORMS"
  echo "  e.g: $0 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/herd-app/centos7/herd:master-abcd1234 linux/amd64,linux/arm64,linux/arm/v7"
  exit 1
fi

IMAGE=$1
BUILD_PLATFORMS=$2


# Perform the test
for IMAGE_PLATFORM in $(IFS=','; echo ${BUILD_PLATFORMS}); do
  echo "PLATFORM: ${IMAGE_PLATFORM}"
  echo "Running container with '--help'"
  docker run --rm --platform ${IMAGE_PLATFORM} -i ${IMAGE} --help
  echo "Running container with '--version'"
  docker run --rm --platform ${IMAGE_PLATFORM} -i ${IMAGE} --version
  echo "Cleaning up"
  docker rmi $IMAGE
  echo
  echo
done
