
#include <boost/test/unit_test.hpp>


#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <tuple>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <json/reader.h>

// SWATCH headers
#include "swatch/action/Command.hpp"
#include "swatch/action/File.hpp"
#include "swatch/action/StateMachine.hpp"
#include "swatch/action/test/DummyCommand.hpp"

#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/GreaterThan.hpp"
#include "swatch/core/rules/InRange.hpp"
#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/LesserThan.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/rules/None.hpp"
#include "swatch/core/rules/OfSize.hpp"
#include "swatch/core/rules/OutOfRange.hpp"

#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/DeviceStub.hpp"
#include "swatch/phase2/OpticalFibreConnectorStub.hpp"
#include "swatch/phase2/test/DummyProcessor.hpp"

#include "herd/app/utilities.hpp"


using namespace swatch;


namespace herd {
namespace app {
namespace test {

struct UtilitiesTestSetup {
  UtilitiesTestSetup() :
    board("__test__", kServiceStub, kProcessorStubs, kFibreConnectorStubs),
    processorA(dynamic_cast<phase2::test::DummyProcessor&>(board.getProcessor("procA"))),
    processorB(dynamic_cast<phase2::test::DummyProcessor&>(board.getProcessor("procB"))),
    commandA1(processorA.registerCommand<action::test::DummyCommand>("dummyCommand")),
    commandA2(processorA.registerCommand<action::test::DummyWarningCommand>("dummyCommand2"))
  {
    // Register 2 commands on device A, 1 on device B
    processorB.registerCommand<action::test::DummyCommand>("myCommand");

    // Create 1 FSM on device A
    action::StateMachine& lFSM = processorA.registerStateMachine("myFSM", "state0", "errorState");

    lFSM.addState("alpha");
    lFSM.addState("beta");
    lFSM.addTransition("hardReset", "state0", "state0").add(commandA1);
    lFSM.addTransition("null", "state0", "state0");
    lFSM.addTransition("configurePart1", "state0", "alpha").add(commandA1, "aNamespace").add(commandA1);
    lFSM.addTransition("configurePart2", "alpha", "beta").add(commandA2, "otherNamespace").add(commandA1);
  }

  ~UtilitiesTestSetup()
  {
  }

  static const phase2::DeviceStub kServiceStub;
  static const std::vector<phase2::DeviceStub> kProcessorStubs;
  static const std::vector<phase2::OpticalFibreConnectorStub> kFibreConnectorStubs;
  phase2::Board board;
  phase2::test::DummyProcessor& processorA;
  phase2::test::DummyProcessor& processorB;
  action::Command& commandA1;
  action::Command& commandA2;
};

const phase2::DeviceStub UtilitiesTestSetup::kServiceStub("myServiceFPGA", "swatch::phase2::test::DummyServiceModule", "aRole", "someURI", "myAddrTable.xml");

const std::vector<phase2::DeviceStub> UtilitiesTestSetup::kProcessorStubs = {
  { { "procA" }, "swatch::phase2::test::DummyProcessor", "aRole", "aUri", "addrTable.xml" },
  { { "procB" }, "swatch::phase2::test::DummyProcessor", "otherRole", "uri2", "addrTableB.xml" }
};

const std::vector<phase2::OpticalFibreConnectorStub> UtilitiesTestSetup::kFibreConnectorStubs;


BOOST_AUTO_TEST_SUITE(ServerTestSuite)


BOOST_AUTO_TEST_CASE(TestBase64)
{
  const std::vector<std::string> lSources = {
    "",
    "M",
    "Ma",
    "Man",
    // From RFC 4648
    "f",
    "fo",
    "foo",
    "foob",
    "fooba",
    "foobar"
  };
  const std::vector<std::string> lEncoded = {
    "",
    "TQ==",
    "TWE=",
    "TWFu",
    // From RFC 4648
    "Zg==",
    "Zm8=",
    "Zm9v",
    "Zm9vYg==",
    "Zm9vYmE=",
    "Zm9vYmFy"
  };

  for (size_t i = 0; i < lSources.size(); i++) {
    BOOST_TEST_MESSAGE("Testing with input " << i);

    BOOST_CHECK_EQUAL(encodeToBase64(lSources.at(i)), lEncoded.at(i));
    BOOST_CHECK_EQUAL(decodeFromBase64(lEncoded.at(i)), lSources.at(i));
  }
}


BOOST_FIXTURE_TEST_CASE(TestEncodeDeviceInfo, UtilitiesTestSetup)
{
  // 1) 'Empty' board object
  Json::Value lResult = *encodeDeviceInfo(phase2::Board("empty", kServiceStub, std::vector<phase2::DeviceStub>(), std::vector<phase2::OpticalFibreConnectorStub>()));

  BOOST_REQUIRE_EQUAL(lResult.type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult.size(), 4);

  const std::vector<std::string> lExpectedTopMembers = { "fibreConnectors", "optics", "processors", "serviceModule" };
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedTopMembers.begin(), lExpectedTopMembers.end());

  BOOST_CHECK_EQUAL(lResult["fibreConnectors"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["fibreConnectors"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["optics"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["optics"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["processors"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"].type(), Json::objectValue);

  const std::vector<std::string> lExpectedSrvcMembers = { "addressTable", "fsm", "id", "isPresent", "properties", "propertyUpdateDuration", "propertyUpdateError", "role", "runningActions", "state", "type", "uri" };
  BOOST_CHECK_EQUAL(lResult["serviceModule"].size(), lExpectedSrvcMembers.size());
  lMembers = lResult["serviceModule"].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedSrvcMembers.begin(), lExpectedSrvcMembers.end());

  BOOST_CHECK_EQUAL(lResult["serviceModule"]["id"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["id"].asString(), kServiceStub.id);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["role"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["role"].asString(), kServiceStub.role);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["type"].asString(), kServiceStub.creator);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["uri"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["uri"].asString(), kServiceStub.uri);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["addressTable"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["addressTable"].asString(), kServiceStub.addressTable);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["fsm"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["state"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["runningActions"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["runningActions"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["isPresent"].isBool(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["isPresent"].asBool(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["properties"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["properties"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["propertyUpdateDuration"].isDouble(), true);
  BOOST_CHECK_GE(lResult["serviceModule"]["propertyUpdateDuration"].asDouble(), 0.0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["propertyUpdateError"].isNull(), true);

  // 2) Board containing two devices
  lResult = *encodeDeviceInfo(phase2::Board("__test__", kServiceStub, kProcessorStubs, kFibreConnectorStubs));

  lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedTopMembers.begin(), lExpectedTopMembers.end());

  BOOST_CHECK_EQUAL(lResult["fibreConnectors"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["fibreConnectors"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["optics"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["optics"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["processors"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["serviceModule"].type(), Json::objectValue);
  BOOST_REQUIRE_EQUAL(lResult["serviceModule"].size(), 12);

  lMembers = lResult["serviceModule"].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedSrvcMembers.begin(), lExpectedSrvcMembers.end());

  BOOST_CHECK_EQUAL(lResult["serviceModule"]["id"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["id"].asString(), kServiceStub.id);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["role"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["role"].asString(), kServiceStub.role);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["type"].asString(), kServiceStub.creator);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["uri"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["uri"].asString(), kServiceStub.uri);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["addressTable"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["addressTable"].asString(), kServiceStub.addressTable);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["fsm"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["state"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["runningActions"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["runningActions"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["isPresent"].isBool(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["isPresent"].asBool(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["properties"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["properties"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["propertyUpdateDuration"].isDouble(), true);
  BOOST_CHECK_GE(lResult["serviceModule"]["propertyUpdateDuration"].asDouble(), 0.0);
  BOOST_CHECK_EQUAL(lResult["serviceModule"]["propertyUpdateError"].isNull(), true);

  BOOST_CHECK_EQUAL(lResult["processors"].isMember("procA"), true);
  BOOST_CHECK_EQUAL(lResult["processors"].isMember("procB"), true);

  const std::vector<std::string> lExpectedProcMembers = { "addressTable", "cspIndexOffset", "fsm", "inputs", "isPresent", "outputs", "properties", "propertyUpdateDuration", "propertyUpdateError", "role", "runningActions", "state", "tcds2Source", "type", "uri" };
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"].size(), lExpectedProcMembers.size());
  lMembers = lResult["processors"]["procA"].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedProcMembers.begin(), lExpectedProcMembers.end());

  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["role"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["role"].asString(), "aRole");
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["type"].asString(), "swatch::phase2::test::DummyProcessor");
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["uri"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["uri"].asString(), "aUri");
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["addressTable"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["addressTable"].asString(), "addrTable.xml");
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["fsm"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["state"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["runningActions"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["runningActions"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["isPresent"].isBool(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["isPresent"].asBool(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["cspIndexOffset"].isUInt(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["cspIndexOffset"].asUInt(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["tcds2Source"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["properties"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["properties"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["propertyUpdateDuration"].isDouble(), true);
  BOOST_CHECK_GE(lResult["processors"]["procA"]["propertyUpdateDuration"].asDouble(), 0.0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["propertyUpdateError"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["inputs"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["inputs"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["outputs"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procA"]["outputs"].size(), 0);

  BOOST_CHECK_EQUAL(lResult["processors"]["procB"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"].size(), lExpectedProcMembers.size());
  lMembers = lResult["processors"]["procB"].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_REQUIRE_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedProcMembers.begin(), lExpectedProcMembers.end());

  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["role"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["role"].asString(), "otherRole");
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["type"].asString(), "swatch::phase2::test::DummyProcessor");
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["uri"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["uri"].asString(), "uri2");
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["addressTable"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["addressTable"].asString(), "addrTableB.xml");
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["fsm"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["state"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["runningActions"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["runningActions"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["isPresent"].isBool(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["isPresent"].asBool(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["cspIndexOffset"].isUInt(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["cspIndexOffset"].asUInt(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["tcds2Source"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["properties"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["properties"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["propertyUpdateDuration"].isDouble(), true);
  BOOST_CHECK_GE(lResult["processors"]["procB"]["propertyUpdateDuration"].asDouble(), 0.0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["propertyUpdateError"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["inputs"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["inputs"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["outputs"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["processors"]["procB"]["outputs"].size(), 0);
}


BOOST_FIXTURE_TEST_CASE(TestEncodeCommandInfo, UtilitiesTestSetup)
{
  Json::Value lResult;
  encodeCommandInfo(lResult, board.getProcessor("procA"));

  // One entry for each command
  BOOST_CHECK_EQUAL(lResult.type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult.size(), 2);
  BOOST_CHECK_EQUAL(lResult.isMember("dummyCommand"), true);
  BOOST_CHECK_EQUAL(lResult.isMember("dummyCommand2"), true);

  // Check info given for procA.dummyCommand
  BOOST_CHECK_EQUAL(lResult["dummyCommand"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["description"].asString(), action::test::DummyCommand::kDescription);

  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"].isMember("x"), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["type"].asString(), "int32");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["description"].asString(), action::test::DummyCommand::kParamDescriptionX);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["rule"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["rule"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["rule"][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["rule"][0].asString(), "Dummy rule for unit tests");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["defaultValue"].isInt(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["x"]["defaultValue"].asInt(), 15);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"].isMember("todo"), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["type"].asString(), "string");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["description"].asString(), action::test::DummyCommand::kParamDescriptionToDo);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["rule"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["rule"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["rule"][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["rule"][0].asString(), "Dummy rule for unit tests");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["defaultValue"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["parameters"]["todo"]["defaultValue"].asString(), "");


  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"].isMember("constraintA"), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][0].size(), 1);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][0][0].asString(), "x");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintA"][1].asString(), "Dummy constraint for unit tests");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"].isMember("constraintB"), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0][0].asString(), "todo");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][0][1].asString(), "x");
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand"]["constraints"]["constraintB"][1].asString(), "Dummy constraint for unit tests");

  // Check info given for procA.dummyCommand2
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["description"].asString(), "");

  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["parameters"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["parameters"].size(), 0);

  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["constraints"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["dummyCommand2"]["constraints"].size(), 0);

  // Check info given for procB
  encodeCommandInfo(lResult, board.getProcessor("procB"));

  BOOST_CHECK_EQUAL(lResult.type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult.size(), 1);
  BOOST_CHECK_EQUAL(lResult.isMember("myCommand"), true);

  BOOST_CHECK_EQUAL(lResult["myCommand"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["description"].asString(), action::test::DummyCommand::kDescription);

  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"].isMember("x"), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["type"].asString(), "int32");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["description"].asString(), action::test::DummyCommand::kParamDescriptionX);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["rule"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["rule"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["rule"][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["rule"][0].asString(), "Dummy rule for unit tests");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["defaultValue"].isInt(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["x"]["defaultValue"].asInt(), 15);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"].isMember("todo"), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["type"].asString(), "string");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["description"].asString(), action::test::DummyCommand::kParamDescriptionToDo);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["rule"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["rule"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["rule"][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["rule"][0].asString(), "Dummy rule for unit tests");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["defaultValue"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["parameters"]["todo"]["defaultValue"].asString(), "");

  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"].type(), Json::objectValue);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"].isMember("constraintA"), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][0].size(), 1);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][0][0].asString(), "x");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintA"][1].asString(), "Dummy constraint for unit tests");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"].isMember("constraintB"), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"].size(), 2);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0][0].asString(), "todo");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][0][1].asString(), "x");
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["myCommand"]["constraints"]["constraintB"][1].asString(), "Dummy constraint for unit tests");
}


BOOST_FIXTURE_TEST_CASE(TestEncodeParameterRule, UtilitiesTestSetup)
{
  typedef std::shared_ptr<const core::AbstractRule> RulePtr_t;
  using namespace swatch::core::rules;

  // 1) No rule
  Json::Value lResult = encodeParameterRule(RulePtr_t());
  BOOST_CHECK_EQUAL(lResult.isNull(), true);

  // 2) FiniteNumber
  std::vector<RulePtr_t> lRules = { RulePtr_t(new FiniteNumber<unsigned int>()), RulePtr_t(new FiniteNumber<int>()), RulePtr_t(new FiniteNumber<float>()) };
  for (const auto& lRule : lRules) {
    lResult = encodeParameterRule(lRule);
    BOOST_CHECK_EQUAL(lResult.isArray(), true);
    BOOST_CHECK_EQUAL(lResult.size(), 2);
    BOOST_CHECK_EQUAL(lResult[1].isString(), true);
    BOOST_CHECK_EQUAL(lResult[1].asString(), "FiniteNumber");
  }

  // 3) FiniteVector
  lRules = { RulePtr_t(new FiniteVector<std::vector<unsigned int>>()), RulePtr_t(new FiniteVector<std::vector<int>>()), RulePtr_t(new FiniteVector<std::vector<float>>()) };
  for (const auto& lRule : lRules) {
    lResult = encodeParameterRule(lRule);
    BOOST_CHECK_EQUAL(lResult.isArray(), true);
    BOOST_CHECK_EQUAL(lResult.size(), 2);
    BOOST_CHECK_EQUAL(lResult[1].isString(), true);
    BOOST_CHECK_EQUAL(lResult[1].asString(), "FiniteVector");
  }

  // 4) GreaterThan
  lResult = encodeParameterRule(RulePtr_t(new GreaterThan<unsigned int>(42)));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 3);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "GreaterThan");
  BOOST_CHECK_EQUAL(lResult[2].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult[2].asInt(), 42);

  // 5) InRange
  lResult = encodeParameterRule(RulePtr_t(new InRange<int>(-2, +5)));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 4);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "InRange");
  BOOST_CHECK_EQUAL(lResult[2].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult[2].asInt(), -2);
  BOOST_CHECK_EQUAL(lResult[3].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult[3].asInt(), +5);

  // 6) IsAmong
  lResult = encodeParameterRule(RulePtr_t(new IsAmong<std::string>({ "Alice", "Bob" })));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 4);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "IsAmong");
  BOOST_CHECK_EQUAL(lResult[2].isString(), true);
  BOOST_CHECK_EQUAL(lResult[2].asString(), "Alice");
  BOOST_CHECK_EQUAL(lResult[3].isString(), true);
  BOOST_CHECK_EQUAL(lResult[3].asString(), "Bob");

  // 7) LesserThan
  lResult = encodeParameterRule(RulePtr_t(new LesserThan<float>(15.6)));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 3);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "LesserThan");
  BOOST_CHECK_EQUAL(lResult[2].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult[2].asFloat(), 15.6, 0.001);

  // 8) NonEmptyString
  lResult = encodeParameterRule(RulePtr_t(new NonEmptyString<std::string>()));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 2);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "NonEmptyString");

  // // 9) None
  BOOST_CHECK_EQUAL(encodeParameterRule(RulePtr_t(new None<bool>())).isNull(), true);
  BOOST_CHECK_EQUAL(encodeParameterRule(RulePtr_t(new None<std::string>())).isNull(), true);
  BOOST_CHECK_EQUAL(encodeParameterRule(RulePtr_t(new None<std::vector<bool>>())).isNull(), true);
  BOOST_CHECK_EQUAL(encodeParameterRule(RulePtr_t(new None<action::File>())).isNull(), true);

  // 10) OfSize
  lResult = encodeParameterRule(RulePtr_t(new OfSize<std::vector<bool>>(6)));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 3);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "OfSize");
  BOOST_CHECK_EQUAL(lResult[2].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult[2].asInt(), 6);

  // 11) OutOfRange
  lResult = encodeParameterRule(RulePtr_t(new OutOfRange<float>(34.2, 56.7)));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 4);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "OutOfRange");
  BOOST_CHECK_EQUAL(lResult[2].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult[2].asFloat(), 34.2, 0.001);
  BOOST_CHECK_EQUAL(lResult[3].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult[3].asFloat(), 56.7, 0.001);

  // 12) All
  lResult = encodeParameterRule(RulePtr_t(new All<std::string>(IsAmong<std::string>({ "Alice", "Bob" }))));
  BOOST_CHECK_EQUAL(lResult.isArray(), true);
  BOOST_CHECK_EQUAL(lResult.size(), 3);
  BOOST_CHECK_EQUAL(lResult[1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[1].asString(), "All");
  BOOST_CHECK_EQUAL(lResult[2].isArray(), true);
  BOOST_CHECK_EQUAL(lResult[2].size(), 4);
  BOOST_CHECK_EQUAL(lResult[2][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult[2][1].asString(), "IsAmong");
  BOOST_CHECK_EQUAL(lResult[2][2].isString(), true);
  BOOST_CHECK_EQUAL(lResult[2][2].asString(), "Alice");
  BOOST_CHECK_EQUAL(lResult[2][3].isString(), true);
  BOOST_CHECK_EQUAL(lResult[2][3].asString(), "Bob");
}


BOOST_FIXTURE_TEST_CASE(TestEncodeFSMs, UtilitiesTestSetup)
{
  Json::Value lResultA = encodeFsmInfo(board.getProcessor("procA"));
  Json::Value lResultB = encodeFsmInfo(board.getProcessor("procB"));

  // One entry for each FSM
  BOOST_CHECK_EQUAL(lResultA.isObject(), true);
  BOOST_CHECK_EQUAL(lResultA.size(), 4);
  BOOST_CHECK_EQUAL(lResultA.isMember("myFSM"), true);
  BOOST_CHECK_EQUAL(lResultB.isObject(), true);
  BOOST_CHECK_EQUAL(lResultB.size(), 3);
  // TODO: Add tests for standard Processor FSMs

  // Check info for the custom FSM on A
  BOOST_CHECK_EQUAL(lResultA["myFSM"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"].size(), 5);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["description"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["initialState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["initialState"].asString(), "state0");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["errorState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["errorState"].asString(), "errorState");

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].size(), 4 /* number of states */);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].isMember("state0"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].isMember("errorState"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].isMember("alpha"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"].isMember("beta"), true);

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"].size(), 3 /* transitions from state0 */);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"].isMember("hardReset"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"].size(), 4);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["description"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["endState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["endState"].asString(), "state0");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"].size(), 1);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0][0].asString(), "dummyCommand");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["hardReset"]["commands"][0][1].asString(), "");

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"].isMember("null"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"].size(), 4);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["description"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["endState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["endState"].asString(), "state0");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["null"]["commands"].size(), 0);

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"].isMember("configurePart1"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"].size(), 4);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["description"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["endState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["endState"].asString(), "alpha");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0][0].asString(), "dummyCommand");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][0][1].asString(), "aNamespace");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1][0].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1][0].asString(), "dummyCommand");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1][1].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["state0"]["configurePart1"]["commands"][1][1].asString(), "");

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"].size(), 1 /* transitions from alpha */);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"].isMember("configurePart2"), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"].size(), 4);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["alias"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["alias"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["description"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["description"].asString(), "");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["endState"].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["endState"].asString(), "beta");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0][0].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0][0].asString(), "dummyCommand2");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][0][1].asString(), "otherNamespace");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1].isArray(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1].size(), 2);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1][0].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1][0].asString(), "dummyCommand");
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1][1].isString(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["alpha"]["configurePart2"]["commands"][1][1].asString(), "");

  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["beta"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["beta"].size(), 0 /* transitions from beta */);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["errorState"].isObject(), true);
  BOOST_CHECK_EQUAL(lResultA["myFSM"]["states"]["errorState"].size(), 0 /* transitions from errorState */);
}


BOOST_AUTO_TEST_CASE(TestEncodeLeaseInfo)
{
  boost::optional<herd::app::Server::Lease> lLease;

  // Case 1: No lease
  auto lResult(*encodeLeaseInfo(lLease));

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "duration", "supervisor" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["supervisor"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["duration"].isNull(), true);

  // Case 2: Lease is defined
  const std::string lSupervisor("supervisor's name");
  const std::string lToken("some-long-random-token-klksdj");
  const float lDuration(123.4);
  lLease = herd::app::Server::Lease(lSupervisor, lToken, lDuration);
  lResult = *encodeLeaseInfo(lLease);

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["supervisor"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["supervisor"].asString(), lSupervisor);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asDouble(), lDuration, 0.001);
}


BOOST_AUTO_TEST_CASE(TestEncodeLeaseToken)
{
  const std::string lSupervisor("supervisor's name");
  const std::string lToken("some-long-random-token-klksdj");
  const float lDuration(987.6);
  herd::app::Server::Lease lLease(lSupervisor, lToken, lDuration);
  const auto lResult(*encodeLeaseToken(lLease));

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "duration", "lease" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["lease"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["lease"].asString(), lToken);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asDouble(), lDuration, 0.001);
}


BOOST_AUTO_TEST_CASE(TestEncodeCommandSimpleSnapshot)
{
  using action::Command;
  using action::CommandSnapshot;
  using core::ParameterSet;

  CommandSnapshot::IdAliasPair lCommandIdAlias("someDevice.aCommand", "command's alias");
  CommandSnapshot::IdAliasPair lDeviceIdAlias("someDevice", "my device");
  std::shared_ptr<const boost::any> lCmdResult(new boost::any(int(42)));

  const std::vector<std::pair<float, std::string>> lMessages { { 4.3, "Some informative status message" } };
  CommandSnapshot lSnapshot(lCommandIdAlias, lDeviceIdAlias, Command::kError, core::SystemTimePoint_t(), 42.5, 0.85, lMessages, ParameterSet(), ParameterSet(), lCmdResult, NULL);

  const Json::Value lResult = *encodeCommandSnapshot(lSnapshot);

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "duration", "errorDetails", "messages", "path", "progress", "result", "startTime", "state" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["path"].asString(), "someDevice.aCommand");
  BOOST_CHECK_EQUAL(lResult["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["state"].asString(), "Error");
  BOOST_CHECK_EQUAL(lResult["startTime"].isNumeric(), true);
  BOOST_CHECK_EQUAL(lResult["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asFloat(), 42.5, 0.00001);
  BOOST_CHECK_EQUAL(lResult["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["progress"].asFloat(), 0.85, 0.00001);
  BOOST_CHECK_EQUAL(lResult["messages"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["messages"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["messages"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["messages"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResult["messages"][0][0].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["messages"][0][0].asFloat(), lMessages.at(0).first, 0.00001);
  BOOST_CHECK_EQUAL(lResult["messages"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["messages"][0][1].asString(), lMessages.at(0).second);
  BOOST_CHECK_EQUAL(lResult["result"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["result"].asInt(), 42);
  BOOST_CHECK_EQUAL(lResult["errorDetails"].isNull(), true);

  // BOOST_CHECK_EQUAL(lResult[lDeviceIdAlias.idPath]["execution-details"].isObject(), true);
  // ...
}


BOOST_AUTO_TEST_CASE(TestEncodeCommandSnapshotWithFile)
{
  using action::Command;
  using action::CommandSnapshot;
  using core::ParameterSet;

  // Create dummy file in temporary directory, to use as command result
  namespace fs = boost::filesystem;
  fs::path lFilePath(fs::temp_directory_path());
  lFilePath /= fs::unique_path();
  fs::create_directories(lFilePath);
  lFilePath /= "testFile.txt";
  const std::vector<char> lFileContents { '1', '2', 'x', 'y' };
  const std::string lEncodedFileContents { 'M', 'T', 'J', '4', 'e', 'Q', '=', '=' };
  std::ofstream lFileStream(lFilePath.native(), std::ios::binary);
  lFileStream.write(lFileContents.data(), lFileContents.size());
  lFileStream.close();

  CommandSnapshot::IdAliasPair lCommandIdAlias("someDevice.aCommand", "command's alias");
  CommandSnapshot::IdAliasPair lDeviceIdAlias("someDevice", "my device");
  std::shared_ptr<const boost::any> lCmdResult(new boost::any(swatch::action::File(lFilePath.native(), "testFile.txt", "someType", "fmtX")));

  CommandSnapshot lSnapshot(lCommandIdAlias, lDeviceIdAlias, Command::kError, core::SystemTimePoint_t::clock::now(), 42.5, 0.85, {}, ParameterSet(), ParameterSet(), lCmdResult, NULL);

  const Json::Value lResult = *encodeCommandSnapshot(lSnapshot);

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "duration", "errorDetails", "messages", "path", "progress", "result", "startTime", "state" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["path"].asString(), "someDevice.aCommand");
  BOOST_CHECK_EQUAL(lResult["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["state"].asString(), "Error");
  BOOST_CHECK_EQUAL(lResult["startTime"].isNumeric(), true);
  BOOST_CHECK_GT(lResult["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asFloat(), 42.5, 0.00001);
  BOOST_CHECK_EQUAL(lResult["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["progress"].asFloat(), 0.85, 0.00001);
  BOOST_CHECK_EQUAL(lResult["messages"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["messages"].size(), 0);

  BOOST_CHECK_EQUAL(lResult["result"].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["result"].size(), 4);
  BOOST_CHECK_EQUAL(lResult["result"].isMember("name"), true);
  BOOST_CHECK_EQUAL(lResult["result"]["name"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["result"]["name"].asString(), "testFile.txt");
  BOOST_CHECK_EQUAL(lResult["result"].isMember("contents"), true);
  BOOST_CHECK_EQUAL(lResult["result"]["contents"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["result"]["contents"].asString(), lEncodedFileContents);
  BOOST_CHECK_EQUAL(lResult["result"].isMember("type"), true);
  BOOST_CHECK_EQUAL(lResult["result"]["type"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["result"]["type"].asString(), "someType");
  BOOST_CHECK_EQUAL(lResult["result"].isMember("format"), true);
  BOOST_CHECK_EQUAL(lResult["result"]["format"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["result"]["format"].asString(), "fmtX");
  BOOST_CHECK_EQUAL(lResult["errorDetails"].isNull(), true);
}


BOOST_AUTO_TEST_CASE(TestEncodeTransitionResultEmpty)
{
  using action::Transition;
  using action::TransitionSnapshot;
  using core::ParameterSet;

  TransitionSnapshot::IdAliasPair lActionIdAlias("someDevice.aFSM.someState.aTransition", "transition's alias");
  TransitionSnapshot::IdAliasPair lDeviceIdAlias("someDevice", "my device");

  TransitionSnapshot lSnapshot(lActionIdAlias, lDeviceIdAlias, Transition::kDone, core::SystemTimePoint_t(), 12.3, NULL, {}, 0);

  const Json::Value lResult = *encodeTransitionSnapshot(lSnapshot);

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "commands", "duration", "numCommandsDone", "numCommandsTotal", "path", "progress", "startTime", "state" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["path"].asString(), "someDevice.aFSM.someState.aTransition");
  BOOST_CHECK_EQUAL(lResult["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["state"].asString(), "Done");
  BOOST_CHECK_EQUAL(lResult["startTime"].isNumeric(), true);
  BOOST_CHECK_EQUAL(lResult["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asFloat(), 12.3, 0.00001);
  BOOST_CHECK_EQUAL(lResult["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["progress"].asFloat(), 1.0, 0.00001);
  BOOST_CHECK_EQUAL(lResult["numCommandsDone"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["numCommandsDone"].asInt(), 0);
  BOOST_CHECK_EQUAL(lResult["numCommandsTotal"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["numCommandsTotal"].asInt(), 0);
  BOOST_CHECK_EQUAL(lResult["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["commands"].size(), 0);
}


BOOST_AUTO_TEST_CASE(TestEncodeTransitionResult)
{
  using action::Command;
  using action::CommandSnapshot;
  using action::Transition;
  using action::TransitionSnapshot;
  using core::ParameterSet;

  TransitionSnapshot::IdAliasPair lActionIdAlias("someDevice.aFSM.someState.aTransition", "transition's alias");
  TransitionSnapshot::IdAliasPair lDeviceIdAlias("someDevice", "my device");

  const std::vector<std::pair<float, std::string>> lMessages { { 0.4, "A status msg" } };
  std::vector<CommandSnapshot> lCmdSnapshots;
  std::shared_ptr<const boost::any> lCmdResult(new boost::any(int(42)));
  lCmdSnapshots.push_back(CommandSnapshot({ "someDevice.aCommand", "cmd's alias" }, { "someDevice", "my device" }, Command::kDone, core::SystemTimePoint_t::clock::now(), 4.2, 1.0, lMessages, ParameterSet(), ParameterSet(), lCmdResult, NULL));
  lCmdResult.reset(new boost::any());
  lCmdSnapshots.push_back(CommandSnapshot({ "someDevice.commandB", "my alias" }, { "someDevice", "my device" }, Command::kError, core::SystemTimePoint_t::clock::now(), 5.6, 0.56, {}, ParameterSet(), ParameterSet(), lCmdResult, NULL));

  TransitionSnapshot lSnapshot(lActionIdAlias, lDeviceIdAlias, Transition::kError, core::SystemTimePoint_t::clock::now(), 10.1, NULL, lCmdSnapshots, 3);

  Json::Value lResult = *encodeTransitionSnapshot(lSnapshot);

  BOOST_CHECK_EQUAL(lResult.isObject(), true);
  const std::vector<std::string> lExpectedMembers = { "commands", "duration", "numCommandsDone", "numCommandsTotal", "path", "progress", "startTime", "state" };
  BOOST_CHECK_EQUAL(lResult.size(), lExpectedMembers.size());
  auto lMembers = lResult.getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers.begin(), lExpectedMembers.end());

  BOOST_CHECK_EQUAL(lResult["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["path"].asString(), "someDevice.aFSM.someState.aTransition");
  BOOST_CHECK_EQUAL(lResult["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["state"].asString(), "Error");
  BOOST_CHECK_EQUAL(lResult["startTime"].isNumeric(), true);
  BOOST_CHECK_GT(lResult["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["duration"].asFloat(), 10.1, 0.00001);
  BOOST_CHECK_EQUAL(lResult["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["progress"].asFloat(), 1.56 / 3, 0.00001);
  BOOST_CHECK_EQUAL(lResult["numCommandsDone"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["numCommandsDone"].asInt(), 2);
  BOOST_CHECK_EQUAL(lResult["numCommandsTotal"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["numCommandsTotal"].asInt(), 3);
  BOOST_CHECK_EQUAL(lResult["commands"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["commands"].size(), 2);

  BOOST_CHECK_EQUAL(lResult["commands"][0].isObject(), true);
  const std::vector<std::string> lExpectedMembers2 = { "duration", "errorDetails", "messages", "path", "progress", "result", "startTime", "state" };
  BOOST_CHECK_EQUAL(lResult["commands"][0].size(), lExpectedMembers2.size());
  lMembers = lResult["commands"][0].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers2.begin(), lExpectedMembers2.end());

  BOOST_CHECK_EQUAL(lResult["commands"][0]["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["path"].asString(), "someDevice.aCommand");
  BOOST_CHECK_EQUAL(lResult["commands"][0]["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["state"].asString(), "Done");
  BOOST_CHECK_EQUAL(lResult["commands"][0]["startTime"].isNumeric(), true);
  BOOST_CHECK_GT(lResult["commands"][0]["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["commands"][0]["duration"].asFloat(), 4.2, 0.00001);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["commands"][0]["progress"].asFloat(), 1.0, 0.00001);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"].size(), 1);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0].size(), 2);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0][0].isNumeric(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0][0].asFloat(), lMessages.at(0).first);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0][1].isString(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["messages"][0][1].asString(), lMessages.at(0).second);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["result"].isIntegral(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["result"].asInt(), 42);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["errorDetails"].isNull(), true);

  BOOST_CHECK_EQUAL(lResult["commands"][1].isObject(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][1].size(), lExpectedMembers2.size());
  lMembers = lResult["commands"][1].getMemberNames();
  std::sort(lMembers.begin(), lMembers.end());
  BOOST_CHECK_EQUAL_COLLECTIONS(lMembers.begin(), lMembers.end(), lExpectedMembers2.begin(), lExpectedMembers2.end());

  BOOST_CHECK_EQUAL(lResult["commands"][1]["path"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["path"].asString(), "someDevice.commandB");
  BOOST_CHECK_EQUAL(lResult["commands"][1]["state"].isString(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["state"].asString(), "Error");
  BOOST_CHECK_EQUAL(lResult["commands"][1]["startTime"].isNumeric(), true);
  BOOST_CHECK_GT(lResult["commands"][1]["startTime"].asFloat(), 0.0);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["duration"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["commands"][1]["duration"].asFloat(), 5.6, 0.00001);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["progress"].isNumeric(), true);
  BOOST_CHECK_CLOSE(lResult["commands"][1]["progress"].asFloat(), 0.56, 0.00001);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["messages"].isArray(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["messages"].size(), 0);
  BOOST_CHECK_EQUAL(lResult["commands"][1]["result"].isNull(), true);
  BOOST_CHECK_EQUAL(lResult["commands"][0]["errorDetails"].isNull(), true);
}


class ManyParameterDummyCommand : public action::test::DummyCommand {
public:
  ManyParameterDummyCommand(const std::string& aId, action::ActionableObject& aActionable) :
    DummyCommand(aId, aActionable)
  {
    registerParameter<bool>("b", true);
    registerParameter<unsigned int>("u", 42);
    registerParameter<float>("f", 3.14);

    registerParameter<std::vector<int>>("vec_i", { -42 });
    registerParameter<std::vector<std::string>>("vec_s", { "x" });
    registerParameter<std::vector<bool>>("vec_b", { true });
    registerParameter<std::vector<unsigned int>>("vec_u", { 42 });
    registerParameter<std::vector<float>>("vec_f", { 3.14 });

    registerParameter<action::File>("file1", { "/path/to/file.txt", "", "" });
    registerParameter<action::File>("file2", { "/path/to/file.txt", "", "" });
  }
};


std::vector<char> generateRandomVector(size_t aLength)
{
  std::random_device rd;
  std::mt19937 gen(rd());

  std::vector<char> values(aLength);
  std::uniform_int_distribution<> dis(0, 255);
  std::generate(values.begin(), values.end(), [&]() { return dis(gen); });
  return values;
}


BOOST_FIXTURE_TEST_CASE(TestExtractParameterSet, UtilitiesTestSetup)
{
  const std::vector<char> lFileContents = generateRandomVector(42);
  const std::string lJsonString = "{"
                                  "\"x\": 15, "
                                  "\"todo\": \"A string!\", "
                                  "\"b\": false, "
                                  "\"u\": 99, "
                                  "\"f\": 1.23, "
                                  "\"vec_i\": [99, -33, 66], "
                                  "\"vec_s\": [\"one\", \"two\"], "
                                  "\"vec_b\": [false, true, true, false], "
                                  "\"vec_u\": [42], "
                                  "\"vec_f\": [1.23], "
                                  // file1: A local file (just 'path' specified)
                                  "\"file1\": {\"path\": \"/path/to/some_file.txt\", \"type\": \"typeA\", \"format\": \"v1\"}, "
                                  // file2: Contents included in message
                                  "\"file2\": {\"name\": \"another_file.bin\", \"type\": \"typeY\", \"format\": \"myFmt\", \"contents\": \""
      + encodeToBase64({ lFileContents.begin(), lFileContents.end() }) + "\"}, "
                                                                         "}";
  BOOST_TEST_MESSAGE("lJsonString: " << lJsonString);

  const std::unique_ptr<Json::CharReader> lJsonReader(Json::CharReaderBuilder().newCharReader());
  Json::Value lParsedJson;
  std::string lError;
  lJsonReader->parse(lJsonString.data(), lJsonString.data() + lJsonString.size(), &lParsedJson, &lError);

  // Function should throw if object map contains a superfluous entry
  BOOST_CHECK_THROW(extractParameterSet(commandA2, lParsedJson), std::runtime_error);

  auto& command = processorA.registerCommand<ManyParameterDummyCommand>("manyParamCommand");
  core::ParameterSet lParameterSet = extractParameterSet(command, lParsedJson).first;

  BOOST_CHECK_EQUAL(lParameterSet.size(), 12);
  BOOST_CHECK(lParameterSet.has("x"));
  BOOST_CHECK_EQUAL(lParameterSet.get<int>("x"), 15);
  BOOST_CHECK(lParameterSet.has("todo"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::string>("todo"), "A string!");
  BOOST_CHECK(lParameterSet.has("b"));
  BOOST_CHECK_EQUAL(lParameterSet.get<bool>("b"), false);
  BOOST_CHECK(lParameterSet.has("u"));
  BOOST_CHECK_EQUAL(lParameterSet.get<unsigned>("u"), 99);
  BOOST_CHECK(lParameterSet.has("f"));
  BOOST_CHECK_CLOSE(lParameterSet.get<float>("f"), 1.23, 0.00001);

  BOOST_CHECK(lParameterSet.has("vec_i"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<int>>("vec_i").size(), 3);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<int>>("vec_i").at(0), 99);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<int>>("vec_i").at(1), -33);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<int>>("vec_i").at(2), 66);
  BOOST_CHECK(lParameterSet.has("vec_s"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<std::string>>("vec_s").size(), 2);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<std::string>>("vec_s").at(0), "one");
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<std::string>>("vec_s").at(1), "two");
  BOOST_CHECK(lParameterSet.has("vec_b"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<bool>>("vec_b").size(), 4);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<bool>>("vec_b").at(0), false);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<bool>>("vec_b").at(1), true);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<bool>>("vec_b").at(2), true);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<bool>>("vec_b").at(3), false);
  BOOST_CHECK(lParameterSet.has("vec_u"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<unsigned>>("vec_u").size(), 1);
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<unsigned>>("vec_u").at(0), 42);
  BOOST_CHECK(lParameterSet.has("vec_f"));
  BOOST_CHECK_EQUAL(lParameterSet.get<std::vector<float>>("vec_f").size(), 1);
  BOOST_CHECK_CLOSE(lParameterSet.get<std::vector<float>>("vec_f").at(0), 1.23, 0.00001);

  BOOST_CHECK(lParameterSet.has("file1"));
  const auto& lFile1 = lParameterSet.get<action::File>("file1");
  BOOST_CHECK_EQUAL(lFile1.getName(), "some_file.txt");
  BOOST_CHECK_EQUAL(lFile1.getPath(), "/path/to/some_file.txt");
  BOOST_CHECK_EQUAL(lFile1.getContentType(), "typeA");
  BOOST_CHECK_EQUAL(lFile1.getContentFormat(), "v1");

  namespace fs = boost::filesystem;
  BOOST_CHECK(lParameterSet.has("file2"));
  const auto& lFile2 = lParameterSet.get<action::File>("file2");
  BOOST_CHECK_EQUAL(lFile2.getName(), "another_file.bin");
  BOOST_CHECK_EQUAL(lFile2.getContentType(), "typeY");
  BOOST_CHECK_EQUAL(lFile2.getContentFormat(), "myFmt");

  BOOST_CHECK_EQUAL(fs::path(lFile2.getPath()).filename().native(), "another_file.bin");
  BOOST_CHECK(fs::exists(fs::path(lFile2.getPath())));
  BOOST_CHECK(fs::is_regular_file(fs::status(fs::path(lFile2.getPath()))));
  std::ifstream lFileStream(lFile2.getPath(), std::ios::binary);
  lFileStream.unsetf(std::ios::skipws);
  std::vector<char> lFileContents2;
  std::copy(std::istream_iterator<char>(lFileStream), std::istream_iterator<char>(), std::back_inserter(lFileContents2));
  BOOST_CHECK_EQUAL(lFileContents.size(), lFileContents2.size());
  BOOST_CHECK(lFileContents == lFileContents2);
}


BOOST_FIXTURE_TEST_CASE(TestExtractParameterSets, UtilitiesTestSetup)
{
  const action::Transition& lEmptyTransition = processorA.getStateMachine("myFSM").getTransition("state0", "null");
  std::vector<core::ParameterSet> lParamSets = extractParameterSets(lEmptyTransition, Json::Value(Json::objectValue)).first;
  BOOST_CHECK(lParamSets.empty());

  const action::Transition& lTransition = processorA.getStateMachine("myFSM").getTransition("alpha", "configurePart2");
  BOOST_CHECK_THROW(extractParameterSets(lTransition, Json::Value(Json::objectValue)), std::runtime_error);

  const std::string lJsonString("[{}, {\"x\": -42, \"todo\": \"Some string!\"}]");
  const std::unique_ptr<Json::CharReader> lJsonReader(Json::CharReaderBuilder().newCharReader());
  Json::Value lParsedJson;
  std::string lError;
  BOOST_CHECK(lJsonReader->parse(lJsonString.data(), lJsonString.data() + lJsonString.size(), &lParsedJson, &lError));

  lParamSets = extractParameterSets(lTransition, lParsedJson).first;
  BOOST_CHECK_EQUAL(lParamSets.size(), 2);
  BOOST_CHECK_EQUAL(lParamSets.at(0).size(), 0);
  BOOST_CHECK_EQUAL(lParamSets.at(1).size(), 2);
  BOOST_CHECK(lParamSets.at(1).has("x"));
  BOOST_CHECK_EQUAL(lParamSets.at(1).get<int>("x"), -42);
  BOOST_CHECK(lParamSets.at(1).has("todo"));
  BOOST_CHECK_EQUAL(lParamSets.at(1).get<std::string>("todo"), "Some string!");

  const std::string lJsonString2("[{\"x\": -42}, {\"x\": -42, \"todo\": \"Some string!\"}]");
  BOOST_CHECK(lJsonReader->parse(lJsonString2.data(), lJsonString2.data() + lJsonString2.size(), &lParsedJson, &lError));
  BOOST_CHECK_THROW(extractParameterSets(lTransition, lParsedJson), std::runtime_error);
}

BOOST_AUTO_TEST_SUITE_END() // ServerTestSuite

} /* namespace test */
} /* namespace app */
} /* namespace herd */
